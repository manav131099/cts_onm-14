rm(list = ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN026CHistorical.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/IN026CDigest/summaryFunctions_new.R')
RESETHISTORICAL=1
daysAlive = 0
reorderStnPaths = c(1,2,38,39,40,3,14,25,32,33,34,35,36,37,4,5,6,7,8,9,10,11,12,13,15,16,17,18,19,20,21,22,23,24,26,27,28,29,30,31)

if(RESETHISTORICAL)
{
system('rm -R /home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-026C]')
system('rm -R /home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-026C]')
system('rm -R /home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-026C]')
}

path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-026C]"
pathwrite2G = "/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-026C]"
pathwrite3G = "/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-026C]"
pathwrite4G = "/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-026C]"


checkdir(pathwrite2G)
checkdir(pathwrite3G)
checkdir(pathwrite4G)
years = dir(path)
for(x in 4 : length(years))
{
	pathyr = paste(path,years[x],sep="/")
	pathwriteyr = paste(pathwrite2G,years[x],sep="/")
	checkdir(pathwriteyr)
	months = dir(pathyr)
	if(!length(months))
		next
	for(y in 7 : length(months))
	{
		pathmon = paste(pathyr,months[y],sep="/")
		pathwritemon = paste(pathwriteyr,months[y],sep="/")
		checkdir(pathwritemon)
		stns = dir(pathmon)
		if(!length(stns))
			next
		stns = stns[reorderStnPaths]
		a=c('DC','GH','MC','NC','SS')
		for(z in 1 : length(stns))
		{
			type = 1
			pathstn = paste(pathmon,stns[z],sep="/")
			if(stns[z] %in% a)
				type = 0
			#if(stns[z] == "WMS")
			#	type = 2
			pathwritestn = paste(pathwritemon,stns[z],sep="/")
			checkdir(pathwritestn)
			days = dir(pathstn)
			if(!length(days))
				next
			for(t in 1 : length(days))
			{
				if(z == 1)
					daysAlive = daysAlive + 1
				pathread = paste(pathstn,days[t],sep="/")
				pathwritefile = paste(pathwritestn,days[t],sep="/")
				if(RESETHISTORICAL || !file.exists(pathwritefile))
				{
					secondGenData(pathread,pathwritefile,type)
					print(paste(days[t],"Done"))
				}
			}
		}
	}
}
sink()