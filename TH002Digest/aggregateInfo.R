source('/home/admin/CODE/common/aggregate.R')

registerMeterList("TH-002X",c("Check","Invoice","Load"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 4 #Column no for DA
aggColTemplate[3] = 9 #column for LastRead
aggColTemplate[4] = 8 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 6 #column for Yld-1
aggColTemplate[8] = 10 #column for Yld-2
aggColTemplate[9] = NA #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = NA #column for Irr
aggColTemplate[12] = NA # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("TH-002X","Check",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 4 #Column no for DA
aggColTemplate[3] = 9 #column for LastRead
aggColTemplate[4] = 8 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 6 #column for Yld-1
aggColTemplate[8] = 13 #column for Yld-2
aggColTemplate[9] = 11 #column for PR-1
aggColTemplate[10] = 14 #column for PR-2
aggColTemplate[11] = 10 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 12 #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("TH-002X","Invoice",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 3 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = NA #column for Eac-2
aggColTemplate[7] = 5 #column for Yld-1
aggColTemplate[8] = NA #column for Yld-2
aggColTemplate[9] = NA #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = NA #column for Irr
aggColTemplate[12] = NA # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("TH-002X","Load",aggNameTemplate,aggColTemplate)

