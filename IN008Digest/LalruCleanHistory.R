rm(list = ls())
errHandle = file('/home/admin/Logs/LogsLalruHistory.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/IN008Digest/FTPLalruDump.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
FIREERRATA = c(1,1)
LTCUTOFF = 0.001
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins)/5)+1))
}

checkErrata = function(row,ts,no)
{
  if(ts < 108 || ts > 192)
	{return()}
	if(FIREERRATA[no] == 0)
	{
	  print(paste('Errata mail already sent for meter',no,'so no more mails'))
		return()
	}
	if((!(is.finite(as.numeric(row[2])))) || (as.numeric(row[2]) < LTCUTOFF))
	{
		FIREERRATA[no] <<- 0
		subj = paste('IN-008X Meter',no,'down')
    body = 'Last recorded timestamp and reading\n'
		body = paste(body,'Timestamp:',as.character(row[1]))
		body = paste(body,'Real Power Tot kW reading:',as.character(row[2]))
		rec=getRecipients("IN-008X","a")
		send.mail(from = 'operations@cleantechsolar.com',
							to = rec,
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F)
		print(paste('Errata mail sent meter ',no))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
		                "\n Real Pow Tot kW reading:",as.character(row[2]))
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "IN-008X"',sep = "")
		system(command)
		print('Twilio message fired')
		recordTimeMaster("IN-008X","TwilioAlert",as.character(row[1]))
	}
}

stitchFile = function(path,days,pathwrite,erratacheck)
{
x = 1
t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
        {
                print(paste("Skipping",days[x],"Due to err in file"))
                return()
        }
  dataread = t
        if(nrow(dataread) < 1)
                return()
  dataread = dataread[,-c(1,2)]
  currcolnames = colnames(dataread)
  idxvals = match(currcolnames,colnames)
  temp = unlist(strsplit(days[x],"-"))
        temp = unlist(strsplit(temp[2],"_"))
  temp = as.numeric(temp[1])
  for(y in 1 : nrow(dataread))
  {
                idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
    rowtemp2 = rowtemp
    yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
    pathwritefinal = paste(pathwritemon,"/Meter ",temp,sep="")
                checkdir(pathwritefinal)
    mtno = temp
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname,temp,"] ",day,".txt",sep="")
    pathtowrite = paste(pathwritefinal,filename,sep="/")
    rowtemp2[idxvals] = dataread[y,]
		{
    if(!file.exists(pathtowrite))
    {
      df = data.frame(rowtemp2,stringsAsFactors = F)
      colnames(df) = colnames
      if(idxts != 1)
        {
          df[idxts,] = rowtemp2
          df[1,] = rowtemp
        }
			FIREERRATA <<- c(1,1)
			RESETDAY<<-1
    }
    else
    {
      df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
      df[idxts,] = rowtemp2
    }
  }
	if(erratacheck != 0)
	{
	pass = c(as.character(df[idxts,1]),as.character(df[idxts,9]))
	checkErrata(pass,idxts,mtno)
	}
	df = df[complete.cases(df[,1]),]
	tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
	df = df[order(tdx),]
  write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F)
	}
  recordTimeMaster("IN-008X","Gen1",as.character(df[nrow(df),1]))
}

path = '/home/admin/Data/Episcript-CEC/EGX IN-008 ALP Lalru'
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[IN-008X]'
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'Lalru.txt',sep="/")
DAYSGLOBAL = dir(path)
DAYSGLOBAL = DAYSGLOBAL[grepl("csv",DAYSGLOBAL)]
lastrecordeddate = c('','')
idxtostart = c(1,1)
{
	if(!file.exists(pathdatelastread))
	{
		print('Last date read file not found so cleansing system from start')
		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[IN-008X]"')
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		lastrecordeddate = c(lastrecordeddate[1],lastrecordeddate[2])
		idxtostart[1] = match(lastrecordeddate[1],DAYSGLOBAL)
		idxtostart[2] = match(lastrecordeddate[2],DAYSGLOBAL)
		print(paste('Date read is',lastrecordeddate[1],'and idxtostart is',idxtostart[1]))
		print(paste('Date read is',lastrecordeddate[2],'and idxtostart is',idxtostart[2]))
	}
}
checkdir(pathwrite)
colnames = colnames(read.csv(paste(path,DAYSGLOBAL[2],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames = colnames[-c(1,2)]
rowtemp = rep(NA,(length(colnames)))
x=1
stnname =  "[IN-008X-M"
days1 = DAYSGLOBAL[grepl('Meter-1',DAYSGLOBAL)]
days2 = DAYSGLOBAL[grepl('Meter-2',DAYSGLOBAL)]
{
	if(idxtostart[1] < length(days1))
	{
		print(paste('idxtostart[1] is',idxtostart[1],'while length of days1 is',length(days1)))
		for(x in idxtostart[1] : length(days1))
		{
 		 stitchFile(path,days1[x],pathwrite,0)
		}
		lastrecordeddate[1] = as.character(days1[x])
		print('Meter 1 DONE')
	}
	if(idxtostart[2] < length(days2))
	{
		print(paste('idxtostart[2] is',idxtostart[2],'while length of days2 is',length(days2)))
		for(x in idxtostart[2] : length(days2))
		{
 		 stitchFile(path,days2[x],pathwrite,0)
		}
		lastrecordeddate[2] = as.character(days2[x])
		print('Meter 2 Done')
	}
	
	else if( (!(idxtostart[1] < length(days1))) && (!(idxtostart[2] < length(days2))))
	{
		print('No historical backlog')
	}
}

print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
print(lastrecordeddate)
write(lastrecordeddate,pathdatelastread)
print('File Written')
#days1 = tail(days[grepl('Meter-1',days)],n=200)
#days2 = tail(days[grepl('Meter-2',days)],n=200)
#days = c(days1,days2)
while(1)
{
	days1 = tail(DAYSGLOBAL[grepl('Meter-1',DAYSGLOBAL)],n=NUMBACKLOG)
	days2 = tail(DAYSGLOBAL[grepl('Meter-2',DAYSGLOBAL)],n=NUMBACKLOG)
	DAYSGLOBAL = c(days1,days2)
  print('Checking FTP for new data')
  filefetch = dumpftp(path)
	if(filefetch == 0)
	{
		Sys.sleep(600)
		next
	}
	print('FTP Check complete')
	daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	days1a = tail(daysnew[grepl('Meter-1',daysnew)],n=NUMBACKLOG)
	days2a = tail(daysnew[grepl('Meter-2',daysnew)],n=NUMBACKLOG)
	daysnew = c(days1a,days2a)
	matchcommon = match(daysnew,DAYSGLOBAL)
	idxmtchs = which(matchcommon%in%NA)
	if(!length(idxmtchs))
	{
	  if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		Sys.sleep(600)
		next
	}
	daysnew = daysnew[idxmtchs]
	reprint = 1
	#match = match(days,daysnew)
	#daysnew = daysnew[-match]
	for(x in 1 : length(daysnew))
	{
	  print(paste('Processing',daysnew[x]))
		stitchFile(path,daysnew[x],pathwrite,1)
		print(paste('Done',daysnew[x]))
	}
	daysnew1 = daysnew[grepl('Meter-1',daysnew)]
	daysnew2 = daysnew[grepl('Meter-2',daysnew)]
	if(length(daysnew1) < 1)
		daysnew1 = lastrecordeddate[1]
	if(length(daysnew2) < 1)
		daysnew2 = lastrecordeddate[2]
	write(c(as.character(daysnew1[length(daysnew1)]),as.character(daysnew2[length(daysnew2)])),pathdatelastread)
	lastrecordeddate[1] = as.character(daysnew1[length(daysnew1)])
	lastrecordeddate[2] = as.character(daysnew2[length(daysnew2)])
	DAYSGLOBAL = dir(path)
gc()
}
print('Out of loop')
sink()
