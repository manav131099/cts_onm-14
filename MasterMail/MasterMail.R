rm(list = ls())
errHandle = file('/home/admin/Logs/LogsMasterMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
require('mailR')
source('/home/admin/CODE/Send_mail/sendmail.R')
source("/home/admin/CODE/MasterMail/HQDigest.R")
donefortheday = 0
time = as.character(format(Sys.time(),tz="Singapore"))
currday = as.character(unlist(strsplit(time," ")))[1]

listOfSubFiles = c("Bot.txt","FTPProbe.txt","FTPNewFiles.txt",
"Gen1.txt","Mail.txt","TwilioAlert.txt")
noUniqueFiles = length(listOfSubFiles)
bodyText = vector('list',noUniqueFiles)

for(x in 1 : noUniqueFiles)
{
	bodyText[[x]] = vector('character')
}
bodyText[[1]][1] = "Time bot last checked for new Gen-1 file (SGT) :"
bodyText[[2]][1] = "Time bot last probed FTP for raw data (SGT) :"
bodyText[[3]][1] = "Time FTP last provided new raw data (SGT) :"
bodyText[[3]][2] = "Last file downloaded from FTP-server :"
bodyText[[4]][1] = "Time bot last updated Gen-1 data (SGT) :"
bodyText[[4]][2] = "Last time appended to Gen-1 data (Local time) :"
bodyText[[5]][1] = "Time bot last fired mail (SGT) :"
bodyText[[5]][2] = "Digest last fired for date :"
bodyText[[6]][1] = "Time bot last sent SMS-alert (SGT) :"
bodyText[[6]][2] = "SMS alert sent for timestamp (Local time) :"

sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("Master","m")
recipientsNeg = getRecipients("Negative","m")
recipientsHQ = getRecipients("HQ","m")
recipients = c('andre.nobre@cleantechsolar.com','rupesh.baker@cleantechsolar.com',
'lucas.ferrand@cleantechsolar.com',
'Krishnamurthy.Ka@cleantechsolar.com',
'rahul.patil@cleantechsolar.com',
'rohit.jaswal@cleantechsolar.com',
'abhishek.puri@cleantechsolar.com',
'jeeva.govindarasu@cleantechsolar.com',
'shravan1994@gmail.com',
'achudhan.govind@cleantechsolar.com',
'divyaraj.chundawat@cleantechsolar.com', 'anusha.agarwal@cleantechsolar.com', 'sai.pranav@cleantechsolar.com')
pwd = 'CTS&*(789'
FORCEPUSH = 0
analyseFlags = function(flags,isGis)
{
	idxiterate = c(2,3,4,1)
	thresholds = c(300,300,300,300)
	if(isGis)
		thresholds[2] = 1400 # Increase FTP New file threshold for GIS since data is dumped once a day
	ErrorSummary = c("Error in bot probing FTP server (for EBX sites) / API data fetch (for Locus sites)  -- most likely Gen-1 bot is down",
	paste("Bot running but data backlogged -- Bot is working, but data isn't being transmitted from site to FTP server. Last file downloaded :",LASTMAILDATE),
	"Error appending Gen-1 Data -- most likely Gen-1 bot is down or data fetched from FTP is not valid to append Gen-1 data",
	"Error in Sending mail -- most likely Mail bot is down")
	idx = 1
	textReturn = "All systems working fine"
	for(x in idxiterate)
	{
		if(is.finite(flags[x]) &&	flags[x] > thresholds[idx])
		{
			textReturn = ErrorSummary[idx]
			break
		}
		idx = idx + 1
	}
	return(textReturn)
}
postProcessNeg = function(body)
{
  bodyret = ""
  addline = "\n--------------------\n"
  entries = unlist(strsplit(body,"\n"))
  prev = "Bot"
  for(x in 1:length(entries))
  {
    if(grepl("___",entries[x]))
      next;
    if(nchar(entries[x])<2)
    {
      bodyret = paste(bodyret,"\n")
      next
    }
    new = substr(trimws(entries[x]),1,3)
    if(new != prev)
    {
      bodyret = paste(bodyret,addline,paste("     ",new,"SITES\n",sep=""),sep="\n")
      prev = new
    }
    bodyret = paste(bodyret,entries[x],sep="\n")
  }
  bodyret = paste(bodyret, "\n\n****************************************")
  return (bodyret)
}
sendMail = function()
{
	print('Inside send-mail call')
	path = "/home/admin/Start/MasterMail"
	days = dir(path)
	days = as.character(days)
	stations = unlist(strsplit(days,"_"))
	seq1 = seq(from=1,to=length(stations),by=2)
	cities = stations[seq1]
	subfiles = stations[(seq1+1)]
	unique_cities = unique(cities)
	idxbreaks = match(unique_cities,cities)
	idxbreaks[(length(idxbreaks)+1)] = length(cities)+1
	body=""
	negBody = ""
	bodySummary = "Summary (refer mail body for mode details)\n"
	negBodySummary = "Bots not functioning correctly\n"
	prevcity = currcity =""
	flags = c(NA,NA,NA,NA)
	fireNegMail = 0
	LASTMAILDATE <<- ""
	for(x in 1 : (length(idxbreaks)-1))
	{
		isGis = 0
		printNam = unique_cities[x]
		flags = c(NA,NA,NA,NA)
		scratchBuffer = ""
		scratchBufferSum = ""
		LASTMAILDATE <<- ""
		if(printNam == "GIS")
		{
			printNam = "SolarGIS (Satellite Data)"
			isGis = 1
		}
		print(paste('x is',x))
		body = paste(body,"_____________________________________________\n\n",sep="")
		body = paste(body,printNam)
		scratchBuffer = "_____________________________________________\n\n"
		scratchBuffer = paste(scratchBuffer,printNam)
		currcity = substr(printNam,1,2)
		if(currcity != prevcity)
		{
			bodySummary = paste(bodySummary,"\n_____________________________________________\n") # Extra line for new country
			scratchBufferSum = "\n_____________________________________________\n"
			prevcity = currcity
		}
		body = paste(body,"\n\n_____________________________________________",sep="")
		scratchBuffer = paste(scratchBuffer,"\n\n_____________________________________________")
		bodySummary = paste(bodySummary,printNam,":",sep="")
		scratchBufferSum = paste(scratchBufferSum,printNam,":",sep="")
		start = idxbreaks[x]
		end = idxbreaks[x+1]-1
		print(paste('start',start))
		print(paste('end',end))
		for(y in start : end)
		{
			print(paste('y is',y))
			print(paste('subfile is',subfiles[y]))
			substationIdx = match(subfiles[y],listOfSubFiles)
			print(paste('Substationidx is',substationIdx))
			if(!is.finite(substationIdx))
			{
				next
			}
			fileData = readLines(paste(path,days[y],sep="/"))
			lenData = length(fileData)
			for(z in 1 : lenData)
			{
				print(paste('z is',z))
				textLine = paste(as.character(bodyText[[substationIdx]][z]),fileData[z])
				if(z == 2 && substationIdx == 3)
					LASTMAILDATE <<- fileData[z]
				if(z == 1)
				{
					t1 = strptime(fileData[z],format="%Y-%m-%d %H:%M:%S",tz="Singapore")
				  t2 = format(Sys.time(),tz="Singapore")
					t2 = strptime(t2,format="%Y-%m-%d %H:%M:%S",tz="Singapore")
					if(substationIdx > 0 && substationIdx < 5)
					{
						differenceHours = as.numeric(difftime(t2,t1,units="mins"))
						flags[substationIdx] = differenceHours
					}
					diff = t2 - t1
					diff = format(round(diff,2),nmsall=2)
					textLine = paste(textLine," (",diff," ago)",sep="")
				}
				print(paste('Textline is',textLine))
				body = paste(body,textLine,sep="\n\n")
				scratchBuffer = paste(scratchBuffer,textLine,sep="\n\n")
			}
		}
		flagAnalysis = analyseFlags(flags,isGis)
		bodySummary = paste(bodySummary,flagAnalysis,"\n")
		body = paste(body,"\n\n")
		if(flagAnalysis != "All systems working fine")
		{
			scratchBufferSum = paste(scratchBufferSum,flagAnalysis,"\n")
			negBody = paste(negBody,scratchBuffer,"\n\n")
			negBodySummary = paste(negBodySummary,scratchBufferSum)
			fireNegMail = 1
		}
	}
	print('Files probed, sending mail')
	body = paste(bodySummary,"\n\n",body,sep="")
	send.mail(from = sender,
            to = recipients,
            subject = paste("MASTER-MAIL",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            debug = F)
	print('Mail sent')
	{
	if(fireNegMail)
	{
		negBodySummary = postProcessNeg(negBodySummary)
		negBody = paste(negBodySummary,"\n\n",negBody,sep="")
	}
	else
	{
		negBody = paste(negBodySummary,"\n\n","All bots working fine!",sep="")
	}
	}
	send.mail(from = sender,
            to = recipientsNeg,
            subject = paste("Negative Mail Digest",currday),
            body = negBody,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            debug = F)
	print('Neg Mail sent')
	prevDay = as.Date(currday,"%Y-%m-%d")
	prevDay = prevDay-1
	prevDay = as.character(prevDay)
	sendAggregateInfo(prevDay,recipientsHQ)
}

LASTMAILDATE = ""
pathLastDate = '/home/admin/Start/StartMaster.txt'
if(file.exists(pathLastDate))
{
	dateLast = readLines(pathLastDate)
	tmnow = format(Sys.time(),tz="Singapore")
	tmnow = substr(as.character(tmnow),1,10)
	{
		if(as.character(dateLast[1]) == tmnow)
		{
			print('Fired already for today so ingore')
			donefortheday = 1
		}
		else
		print('Days dont match so will fire if above 8am')
	}
}

while(1)
{
	recipients = getRecipients("Master","m")
	recipientsNeg = getRecipients("Negative","m")
	recipientsHQ = getRecipients("HQ","m")
	time = as.character(format(Sys.time(),tz="Singapore"))
	tm = as.character(unlist(strsplit(time," ")))
	{
		if(!donefortheday)
		{
			hr = as.character(unlist(strsplit(tm[2],":")))
			{
				if(as.numeric(hr[1]) > 8 || FORCEPUSH)
				{
					sendMail()
					donefortheday = 1
					tmnow = format(Sys.time(),tz="Singapore")
					tmnow = substr(as.character(tmnow),1,10)
					write(tmnow,file=pathLastDate)
					FORCEPUSH = 0
				}
				else
				{
					print(paste('Not yet time as hr is',hr[1]))
				}
			}
		}
		else
		{
			{
				if(currday != tm[1])
				{
					currday = tm[1]
					donefortheday = 0
					print('date changed')
				}
				else
					print('Done for the day')
			}
		}
	}
	Sys.sleep(3600)
}
sink()
