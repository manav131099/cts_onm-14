library(mailR)
require('compiler')
require(Hmisc)

sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients =c('andre.nobre@cleantechsolar.com','anusha.agarwal@cleantechsolar.com','rupesh.baker@cleantechsolar.com','snehal.dakhore@cleantechsolar.com','sushil.disale@cleantechsolar.com','rohit.jaswal@cleantechsolar.com','shravan1994@gmail.com','pradip.chavan@cleantechsolar.com')
#recipients = c('anusha.agarwal@cleantechsolar.com')
pwd = 'CTS&*(789'
today = Sys.Date()-1
dtm = monthDays(as.Date(today)) #DAYS THIS MONTH
df2 = read.table('/home/admin/Dropbox/GIS_API/Historical.txt',header = TRUE)

day_passed = substring(today,9,10)
print(day_passed)
day_passed = as.numeric(day_passed)
day_passed = day_passed-1
path = "/home/admin/Dropbox/GIS_API"


#FILENAMES
filename1= paste("CHIPLUN_AGGREGATE.txt",sep="")
filename2= paste("MALUMBRA_AGGREGATE.txt",sep="")
filename3= paste("CHANGI_AGGREGATE.txt",sep="")
filename4= paste("WOODLANDS_AGGREGATE.txt",sep="")
filename5= paste("TUAS_AGGREGATE.txt",sep="")
filename6= paste("LALRU_AGGREGATE.txt",sep="")
filename7= paste("PAKA_AGGREGATE.txt",sep="")
filename8= paste("TAK_AGGREGATE.txt",sep="")
filename9= paste("KARAIKAL_AGGREGATE.txt",sep="")
filename10= paste("BAWAL_AGGREGATE.txt",sep="")
filename11= paste("NASIK_AGGREGATE.txt",sep="")
filename12= paste("COIMBATORE_AGGREGATE.txt",sep="")
filename13 = paste("BESCOM_AGGREGATE.txt",sep="")
filename14 = paste("TIRUNELVELI_AGGREGATE.txt",sep="")
filename15 = paste("SATNA_AGGREGATE.txt",sep="")
filename16 = paste("KURANGANWALI_AGGREGATE.txt",sep="")
filename17 = paste("BHILWARA_AGGREGATE.txt",sep="")
filename18 = paste("AURANGABAD_AGGREGATE.txt",sep="")
filename19 = paste("NEW_DELHI_AGGREGATE.txt",sep="")
filename20 = paste("ORAGADAM_AGGREGATE.txt",sep="")
filename21 = paste("PUNE_AGGREGATE.txt",sep="")
filename22 = paste("HYDERABAD_AGGREGATE.txt",sep="")
filename23 = paste("HOSUR_AGGREGATE.txt",sep="")
filename24 = paste("JHAJJHAR_AGGREGATE.txt",sep="")
filename25 = paste("CHENNAI_AGGREGATE.txt",sep="")

#PATHREADS
pathRead1 = paste(path,"CHIPLUN",filename1,sep="/") #Chiplun
pathRead2 = paste(path,"MALUMBRA",filename2,sep="/") #Malumbra
pathRead3 = paste(path,"CHANGI",filename3,sep="/") #Changi
pathRead4 = paste(path,"WOODLANDS",filename4,sep="/") #Woodlands
pathRead5 = paste(path,"TUAS",filename5,sep="/") #Tuas
pathRead6 = paste(path,"LALRU",filename6,sep="/") #Lalru
pathRead7 = paste(path,"PAKA",filename7,sep="/") #Paka
pathRead8 = paste(path,"TAK",filename8,sep="/") #Tak
pathRead9 = paste(path,"KARAIKAL",filename9,sep="/") #Karaikal
pathRead10 = paste(path,"BAWAL",filename10,sep="/") #Bawal
pathRead11 = paste(path,"NASIK",filename11,sep="/") #Nasik
pathRead12 = paste(path,"COIMBATORE",filename12,sep="/") #Coimbatore
pathRead13 = paste(path,"BESCOM",filename13,sep="/") #Bescom
pathRead14 = paste(path,"TIRUNELVELI",filename14,sep="/") #Tirunelveli
pathRead15 = paste(path,"SATNA",filename15,sep="/") #Satna
pathRead16 = paste(path,"KURANGANWALI",filename16,sep="/") #Kuranganwali
pathRead17 = paste(path,"BHILWARA",filename17,sep="/") #Bhilwara
pathRead18 = paste(path,"AURANGABAD",filename18,sep="/") #Aurangabad
pathRead19 = paste(path,"NEW_DELHI",filename19,sep="/") #New_Delhi
pathRead20 = paste(path,"ORAGADAM",filename20,sep="/") #Oragadam
pathRead21 = paste(path,"PUNE",filename21,sep="/") #Pune
pathRead22 = paste(path,"HYDERABAD",filename22,sep="/") #Hyderabad
pathRead23 = paste(path,"HOSUR",filename23,sep="/") #Hosur
pathRead24 = paste(path,"JHAJJHAR",filename24,sep="/") #Jhajjhar
pathRead25 = paste(path,"CHENNAI",filename25,sep="/") #Chennai

#CHIPLUN

dataread1 = read.table(pathRead1,header = T)
year_sum_Chiplun = round(sum(dataread1[2]),1)
Chiplun_ghi = tail(dataread1[2],1)
Chiplun_date = tail(dataread1[1],1)
Chiplun_ma = tail(dataread1[2],30)
Chiplun_ma = round(sum(Chiplun_ma)/30,2)
Chiplun_ghi <- as.matrix(Chiplun_ghi)
tmy_Chiplun = round(((year_sum_Chiplun/1888)-1)*-100,1)
#Extrapolated Value Calculation
Chiplun_ms = sum(tail(dataread1[2],day_passed))
exp_Chiplun = round((Chiplun_ms*dtm)/day_passed,2)
print("Exrapolated")
print(tail(dataread1[2],day_passed))
print(day_passed)
print(dtm)
print(exp_Chiplun)

#MALUMBRA

dataread2 = read.table(pathRead2,header = T)
year_sum_Malumbra = round(sum(dataread2[2]),1)
Malumbra_ma = tail(dataread2[2],30)
Malumbra_ma = round(sum(Malumbra_ma)/30,2)
Malumbra_ghi = tail(dataread2[2],1)
Malumbra_ghi <- as.matrix(Malumbra_ghi)
tmy_Malumbra = round(((year_sum_Malumbra/2031)-1)*-100,1)
Malumbra_ms = sum(tail(dataread2[2],day_passed))
exp_Malumbra = round((Malumbra_ms*dtm)/day_passed,2)

year_sum_Malumbra_gti = round(sum(dataread2[3]),1)
Malumbra_ma_gti = tail(dataread2[3],30)
Malumbra_ma_gti = round(sum(Malumbra_ma_gti)/30,2)
Malumbra_gti = tail(dataread2[3],1)
Malumbra_gti <- as.matrix(Malumbra_gti)
tmy_Malumbra_gti = round(((year_sum_Malumbra_gti/2031)-1)*-100,1)
Malumbra_ms_gti = sum(tail(dataread2[3],day_passed))
exp_Malumbra_gti = round((Malumbra_ms_gti*dtm)/day_passed,2)

#CHANGI

dataread3 = read.table(pathRead3,header = T)
year_sum_Changi = tail(dataread3[2],365)
year_sum_Changi = round(sum(year_sum_Changi),1)
Changi_ma = tail(dataread3[2],30)
Changi_ma = round(sum(Changi_ma)/30,2)
Changi_ghi = tail(dataread3[2],1)
Changi_ghi <- as.matrix(Changi_ghi)
tmy_Changi = round(((year_sum_Changi/1679)-1)*100,1)
Changi_ms = sum(tail(dataread3[2],day_passed))
exp_Changi = round((Changi_ms*dtm)/day_passed,2)

#WOODLANDS

dataread4 = read.table(pathRead4,header = T)
year_sum_Woodlands = tail(dataread4[2],365)
year_sum_Woodlands = round(sum(year_sum_Woodlands),1)
Woodlands_ma = tail(dataread4[2],30)
Woodlands_ma = round(sum(Woodlands_ma)/30,2)
Woodlands_ghi = tail(dataread4[2],1)
Woodlands_ghi <- as.matrix(Woodlands_ghi)
tmy_Woodlands = round(((year_sum_Woodlands/1617)-1)*100,1)
Woodlands_ms = sum(tail(dataread4[2],day_passed))
exp_Woodlands = round((Woodlands_ms*dtm)/day_passed,2)

#TUAS

dataread5 = read.table(pathRead5,header = T)
year_sum_Tuas = tail(dataread5[2],365)
year_sum_Tuas = round(sum(year_sum_Tuas),1)
Tuas_ma = tail(dataread5[2],30)
Tuas_ma = round(sum(Tuas_ma)/30,2)
Tuas_ghi = tail(dataread5[2],1)
Tuas_ghi <- as.matrix(Tuas_ghi)
tmy_Tuas = round(((year_sum_Tuas/1672)-1)*100,1)
Tuas_ms = sum(tail(dataread5[2],day_passed))
exp_Tuas = round((Tuas_ms*dtm)/day_passed,2)

#LALRU

dataread6 = read.table(pathRead6,header = T)
year_sum_Lalru = tail(dataread6[2])
year_sum_Lalru = round(sum(year_sum_Tuas),1)
Lalru_ma = tail(dataread6[2],30)
Lalru_ma = round(sum(Lalru_ma)/30,2)
Lalru_ghi = tail(dataread6[2],1)
Lalru_ghi <- as.matrix(Lalru_ghi)
tmy_Lalru = round(((year_sum_Lalru/1740)-1)*100,1)
Lalru_ms = sum(tail(dataread6[2],day_passed))
exp_Lalru = round((Lalru_ms*dtm)/day_passed,2)

#PAKA

dataread7 = read.table(pathRead7,header = T)
year_sum_Paka = tail(dataread7[2],365)
year_sum_Paka = round(sum(year_sum_Paka),1)
Paka_ma = tail(dataread7[2],30)
Paka_ma = round(sum(Paka_ma)/30,2)
Paka_ghi = tail(dataread7[2],1)
Paka_ghi <- as.matrix(Paka_ghi)
tmy_Paka = round(((year_sum_Paka/1713)-1)*100,1)
Paka_ms = sum(tail(dataread7[2],day_passed))
exp_Paka = round((Paka_ms*dtm)/day_passed,2)

#TAK

dataread8 = read.table(pathRead8,header = T)
year_sum_Tak = tail(dataread8[2],365)
year_sum_Tak = round(sum(year_sum_Tak),1)
Tak_ma = tail(dataread8[2],30)
Tak_ma = round(sum(Tak_ma)/30,2)
Tak_ghi = tail(dataread8[2],1)
Tak_ghi <- as.matrix(Tak_ghi)
tmy_Tak = round(((year_sum_Tak/1824)-1)*100,1)
Tak_ms = sum(tail(dataread8[2],day_passed))
exp_Tak = round((Tak_ms*dtm)/day_passed,2)

#BAWAL

dataread9 = read.table(pathRead9,header = T)
year_sum_Bawal = tail(dataread9[2],365)
year_sum_Bawal = round(sum(year_sum_Bawal),1)
Bawal_ma = tail(dataread9[2],30)
Bawal_ma = round(sum(Bawal_ma)/30,2)
Bawal_ghi = tail(dataread9[2],1)
Bawal_ghi <- as.matrix(Bawal_ghi)
tmy_Bawal = round(((year_sum_Bawal/1799)-1)*100,1)
Bawal_ms = sum(tail(dataread9[2],day_passed))
exp_Bawal = round((Bawal_ms*dtm)/day_passed,2)

#KARAIKAL

dataread10 = read.table(pathRead10,header = T)
year_sum_Karaikal = tail(dataread10[2],365)
year_sum_Karaikal = round(sum(year_sum_Karaikal),1)
Karaikal_ma = tail(dataread10[2],30)
Karaikal_ma = round(sum(Karaikal_ma)/30,2)
Karaikal_ghi = tail(dataread10[2],1)
Karaikal_ghi <- as.matrix(Karaikal_ghi)
tmy_Karaikal = round(((year_sum_Karaikal/1985)-1)*100,1)
Karaikal_ms = sum(tail(dataread10[2],day_passed))
exp_Karaikal = round((Karaikal_ms*dtm)/day_passed,2)

#NASIK

dataread11 = read.table(pathRead11,header = T)
year_sum_Nasik = tail(dataread11[2],365)
year_sum_Nasik = round(sum(year_sum_Nasik),1)
Nasik_ma = tail(dataread11[2],30)
Nasik_ma = round(sum(Nasik_ma)/30,2)
Nasik_ghi = tail(dataread11[2],1)
Nasik_ghi <- as.matrix(Nasik_ghi)
tmy_Nasik = round(((year_sum_Nasik/1953)-1)*100,1)
Nasik_ms = sum(tail(dataread11[2],day_passed))
exp_Nasik = round((Nasik_ms*dtm)/day_passed,2)

year_sum_Nasik_gti = tail(dataread11[3],365)
year_sum_Nasik_gti = round(sum(year_sum_Nasik_gti),1)
Nasik_ma_gti = tail(dataread11[3],30)
Nasik_ma_gti = round(sum(Nasik_ma_gti)/30,2)
Nasik_gti = tail(dataread11[3],1)
Nasik_gti <- as.matrix(Nasik_gti)
tmy_Nasik_gti = round(((year_sum_Nasik_gti/1953)-1)*100,1)
Nasik_ms_gti = sum(tail(dataread11[3],day_passed))
exp_Nasik_gti = round((Nasik_ms_gti*dtm)/day_passed,2)

#COIMBATORE

dataread12 = read.table(pathRead12,header = T)
year_sum_Coimbatore = tail(dataread12[2],365)
year_sum_Coimbatore = round(sum(year_sum_Coimbatore),1)
Coimbatore_ma = tail(dataread12[2],30)
Coimbatore_ma = round(sum(Coimbatore_ma)/30,2)
Coimbatore_ghi = tail(dataread12[2],1)
Coimbatore_ghi <- as.matrix(Coimbatore_ghi)
tmy_Coimbatore = round(((year_sum_Coimbatore/1952)-1)*100,1)
Coimbatore_ms = sum(tail(dataread12[2],day_passed))
exp_Coimbatore = round((Coimbatore_ms*dtm)/day_passed,2)

year_sum_Coimbatore_gti = tail(dataread12[3],365)
year_sum_Coimbatore_gti = round(sum(year_sum_Coimbatore_gti),1)
Coimbatore_ma_gti = tail(dataread12[3],30)
Coimbatore_ma_gti = round(sum(Coimbatore_ma_gti)/30,2)
Coimbatore_gti = tail(dataread12[3],1)
Coimbatore_gti <- as.matrix(Coimbatore_gti)
tmy_Coimbatore_gti = round(((year_sum_Coimbatore_gti/1952)-1)*100,1)
Coimbatore_ms_gti = sum(tail(dataread12[3],day_passed))
exp_Coimbatore_gti = round((Coimbatore_ms_gti*dtm)/day_passed,2)

#BESCOM

dataread13 = read.table(pathRead13,header = T)
year_sum_Bescom = tail(dataread13[2],365)
year_sum_Bescom = round(sum(year_sum_Bescom),1)
Bescom_ma = tail(dataread13[2],30)
Bescom_ma = round(sum(Bescom_ma)/30,2)
Bescom_ghi = tail(dataread13[2],1)
Bescom_ghi <- as.matrix(Bescom_ghi)
tmy_Bescom = round(((year_sum_Bescom/2059)-1)*100,1)
Bescom_ms = sum(tail(dataread13[2],day_passed))
exp_Bescom = round((Bescom_ms*dtm)/day_passed,2)

year_sum_Bescom_gti = tail(dataread13[3],365)
year_sum_Bescom_gti = round(sum(year_sum_Bescom_gti),1)
Bescom_ma_gti = tail(dataread13[3],30)
Bescom_ma_gti = round(sum(Bescom_ma_gti)/30,2)
Bescom_gti = tail(dataread13[3],1)
Bescom_gti <- as.matrix(Bescom_gti)
tmy_Bescom_gti = round(((year_sum_Bescom_gti/2059)-1)*100,1)
Bescom_ms_gti = sum(tail(dataread13[3],day_passed))
exp_Bescom_gti = round((Bescom_ms_gti*dtm)/day_passed,2)

#TIRUNELVELI

dataread14 = read.table(pathRead14,header = T)
year_sum_Tirunelveli = tail(dataread14[2],365)
year_sum_Tirunelveli = round(sum(year_sum_Tirunelveli),1)
Tirunelveli_ma = tail(dataread14[2],30)
Tirunelveli_ma = round(sum(Tirunelveli_ma)/30,2)
Tirunelveli_ghi = tail(dataread14[2],1)
Tirunelveli_ghi <- as.matrix(Tirunelveli_ghi)
tmy_Tirunelveli = round(((year_sum_Tirunelveli/1862)-1)*100,1)
Tirunelveli_ms = sum(tail(dataread14[2],day_passed))
exp_Tirunelveli = round((Tirunelveli_ms*dtm)/day_passed,2)

year_sum_Tirunelveli_gti = tail(dataread14[3],365)
year_sum_Tirunelveli_gti = round(sum(year_sum_Tirunelveli_gti),1)
Tirunelveli_ma_gti = tail(dataread14[3],30)
Tirunelveli_ma_gti = round(sum(Tirunelveli_ma_gti)/30,2)
Tirunelveli_gti = tail(dataread14[3],1)
Tirunelveli_gti <- as.matrix(Tirunelveli_gti)
tmy_Tirunelveli_gti = round(((year_sum_Tirunelveli_gti/1862)-1)*100,1)
Tirunelveli_ms_gti = sum(tail(dataread14[3],day_passed))
exp_Tirunelveli_gti = round((Tirunelveli_ms_gti*dtm)/day_passed,2)

#SATNA

dataread15 = read.table(pathRead15,header = T)
year_sum_Satna = tail(dataread15[2],365)
year_sum_Satna = round(sum(year_sum_Satna),1)

Satna_ma = tail(dataread15[2],30)
Satna_ma = round(sum(Satna_ma)/30,2)
Satna_ghi = tail(dataread15[2],1)
Satna_ghi <- as.matrix(Satna_ghi)
tmy_Satna = round(((year_sum_Satna/1874)-1)*100,1)
Satna_ms = sum(tail(dataread15[2],day_passed))
exp_Satna = round((Satna_ms*dtm)/day_passed,2)

year_sum_Satna_gti = tail(dataread15[3],365)
year_sum_Satna_gti = round(sum(year_sum_Satna_gti),1)
Satna_ma_gti = tail(dataread15[3],30)
Satna_ma_gti = round(sum(Satna_ma_gti)/30,2)
Satna_gti = tail(dataread15[3],1)
Satna_gti <- as.matrix(Satna_gti)
tmy_Satna_gti = round(((year_sum_Satna_gti/1874)-1)*100,1)
Satna_ms_gti = sum(tail(dataread15[3],day_passed))
exp_Satna_gti = round((Satna_ms_gti*dtm)/day_passed,2)

#Kuranganwali

dataread16 = read.table(pathRead16,header = T)
year_sum_Kuranganwali = tail(dataread16[2],365)
year_sum_Kuranganwali = round(sum(year_sum_Kuranganwali),1)

Kuranganwali_ma = tail(dataread16[2],30)
Kuranganwali_ma = round(sum(Kuranganwali_ma)/30,2)
Kuranganwali_ghi = tail(dataread16[2],1)
Kuranganwali_ghi <- as.matrix(Kuranganwali_ghi)
tmy_Kuranganwali = round(((year_sum_Kuranganwali/1791)-1)*100,1)
Kuranganwali_ms = sum(tail(dataread16[2],day_passed))
exp_Kuranganwali = round((Kuranganwali_ms*dtm)/day_passed,2)

year_sum_Kuranganwali_gti = tail(dataread16[3],365)
year_sum_Kuranganwali_gti = round(sum(year_sum_Kuranganwali_gti),1)
Kuranganwali_ma_gti = tail(dataread16[3],30)
Kuranganwali_ma_gti = round(sum(Kuranganwali_ma_gti)/30,2)
Kuranganwali_gti = tail(dataread16[3],1)
Kuranganwali_gti <- as.matrix(Kuranganwali_gti)
tmy_Kuranganwali_gti = round(((year_sum_Kuranganwali_gti/1791)-1)*100,1)
Kuranganwali_ms_gti = sum(tail(dataread16[3],day_passed))
exp_Kuranganwali_gti = round((Kuranganwali_ms_gti*dtm)/day_passed,2)

#BHILWARA

dataread17 = read.table(pathRead17,header = T, fill = T)
year_sum_Bhilwara = tail(dataread17[2],365)
year_sum_Bhilwara = round(sum(year_sum_Bhilwara),1)
Bhilwara_ma = tail(dataread17[2],30)
Bhilwara_ma = round(sum(Bhilwara_ma)/30,2)
Bhilwara_ghi = tail(dataread17[2],1)
Bhilwara_ghi <- as.matrix(Bhilwara_ghi)
tmy_Bhilwara = round(((year_sum_Bhilwara/2003)-1)*100,1)
Bhilwara_ms = sum(tail(dataread17[2],day_passed))
exp_Bhilwara = round((Bhilwara_ms*dtm)/day_passed,2)

year_sum_Bhilwara_gti = tail(dataread17[3],365)
year_sum_Bhilwara_gti = round(sum(year_sum_Bhilwara_gti),1)
Bhilwara_ma_gti = tail(dataread17[3],30)
Bhilwara_ma_gti = round(sum(Bhilwara_ma_gti)/30,2)
Bhilwara_gti = tail(dataread17[3],1)
Bhilwara_gti <- as.matrix(Bhilwara_gti)
tmy_Bhilwara_gti = round(((year_sum_Bhilwara_gti/2003)-1)*100,1)
Bhilwara_ms_gti = sum(tail(dataread17[3],day_passed))
exp_Bhilwara_gti = round((Bhilwara_ms_gti*dtm)/day_passed,2)

#AURANGABAD

dataread18 = read.table(pathRead18,header = T, fill = T)
year_sum_Aurangabad = tail(dataread18[2],365)
year_sum_Aurangabad = round(sum(year_sum_Aurangabad),1)
Aurangabad_ma = tail(dataread18[2],30)
Aurangabad_ma = round(sum(Aurangabad_ma)/30,2)
Aurangabad_ghi = tail(dataread18[2],1)
Aurangabad_ghi <- as.matrix(Aurangabad_ghi)
tmy_Aurangabad = round(((year_sum_Aurangabad/1931)-1)*100,1)
Aurangabad_ms = sum(tail(dataread18[2],day_passed))
exp_Aurangabad = round((Aurangabad_ms*dtm)/day_passed,2)



#NEW_DELHI

dataread19 = read.table(pathRead19,header = T, fill = T)
year_sum_ND = tail(dataread19[2],365)
year_sum_ND = round(sum(year_sum_ND),1)
ND_ma = tail(dataread19[2],30)
ND_ma = round(sum(ND_ma)/30,2)
ND_ghi = tail(dataread19[2],1)
ND_ghi <- as.matrix(ND_ghi)
tmy_ND = round(((year_sum_ND/1712)-1)*100,1)
ND_ms = sum(tail(dataread19[2],day_passed))
exp_ND = round((ND_ms*dtm)/day_passed,2)



#ORAGADAM

dataread20 = read.table(pathRead20,header = T, fill = T)
year_sum_Oragadam = tail(dataread20[2],365)
year_sum_Oragadam = round(sum(year_sum_Oragadam),1)
Oragadam_ma = tail(dataread20[2],30)
Oragadam_ma = round(sum(Oragadam_ma)/30,2)
Oragadam_ghi = tail(dataread20[2],1)
Oragadam_ghi <- as.matrix(Oragadam_ghi)
tmy_Oragadam = round(((year_sum_Oragadam/1943)-1)*100,1)
Oragadam_ms = sum(tail(dataread20[2],day_passed))
exp_Oragadam = round((Oragadam_ms*dtm)/day_passed,2)


#PUNE

dataread21 = read.table(pathRead21,header = T, fill = T)
year_sum_Pune = tail(dataread21[2],365)
year_sum_Pune = round(sum(year_sum_Pune),1)
Pune_ma = tail(dataread21[2],30)
Pune_ma = round(sum(Pune_ma)/30,2)
Pune_ghi = tail(dataread21[2],1)
Pune_ghi <- as.matrix(Pune_ghi)
tmy_Pune = round(((year_sum_Pune/1932)-1)*100,1)
Pune_ms = sum(tail(dataread21[2],day_passed))
exp_Pune = round((Pune_ms*dtm)/day_passed,2)


#CHENNAI

dataread25 = read.table(pathRead25,header = T, fill = T)
year_sum_Chennai = tail(dataread25[2],365)
year_sum_Chennai = round(sum(year_sum_Chennai),1)
Chennai_ma = tail(dataread25[2],30)
Chennai_ma = round(sum(Chennai_ma)/30,2)
Chennai_ghi = tail(dataread25[2],1)
Chennai_ghi <- as.matrix(Chennai_ghi)
tmy_Chennai = round(((year_sum_Chennai/1935)-1)*100,1)
Chennai_ms = sum(tail(dataread25[2],day_passed))
exp_Chennai = round((Chennai_ms*dtm)/day_passed,2)



#HYDERABAD

dataread22 = read.table(pathRead22,header = T, fill = T)
year_sum_Hyderabad = tail(dataread22[2],365)
year_sum_Hyderabad = round(sum(year_sum_Hyderabad),1)
Hyderabad_ma = tail(dataread22[2],30)
Hyderabad_ma = round(sum(Hyderabad_ma)/30,2)
Hyderabad_ghi = tail(dataread22[2],1)
Hyderabad_ghi <- as.matrix(Hyderabad_ghi)
tmy_Hyderabad = round(((year_sum_Hyderabad/1960)-1)*100,1)
Hyderabad_ms = sum(tail(dataread22[2],day_passed))
exp_Hyderabad = round((Hyderabad_ms*dtm)/day_passed,2)



#HOSUR

dataread23 = read.table(pathRead23,header = T, fill = T)
year_sum_Hosur = tail(dataread23[2],365)
year_sum_Hosur = round(sum(year_sum_Hosur),1)
Hosur_ma = tail(dataread23[2],30)
Hosur_ma = round(sum(Hosur_ma)/30,2)
Hosur_ghi = tail(dataread23[2],1)
Hosur_ghi <- as.matrix(Hosur_ghi)
tmy_Hosur = round(((year_sum_Hosur/1986)-1)*100,1)
Hosur_ms = sum(tail(dataread23[2],day_passed))
exp_Hosur = round((Hosur_ms*dtm)/day_passed,2)

#JHAJJHAR

dataread24 = read.table(pathRead24,header = T, fill = T)
year_sum_Jhajjhar = tail(dataread24[2],365)
year_sum_Jhajjhar = round(sum(year_sum_Jhajjhar),1)
Jhajjhar_ma = tail(dataread24[2],30)
Jhajjhar_ma = round(sum(Jhajjhar_ma)/30,2)
Jhajjhar_ghi = tail(dataread24[2],1)
Jhajjhar_ghi <- as.matrix(Jhajjhar_ghi)
tmy_Jhajjhar = round(((year_sum_Jhajjhar/1766)-1)*100,1)
Jhajjhar_ms = sum(tail(dataread24[2],day_passed))
exp_Jhajjhar = round((Jhajjhar_ms*dtm)/day_passed,2)



body = ""
body = paste(body,"Last date received for 25 stations and daily irradiation for the given day:")


#SENDING EMAIL

body = paste(body,"\n\nAurangabad (IN):",today,",",Aurangabad_ghi,"kWh/m2, 30-d MA:",Aurangabad_ma,", 365-d sum:",year_sum_Aurangabad,",",tmy_Aurangabad,"% vs TMY")
body = paste(body,"\n\nBawal (IN):",today,",",Bawal_ghi,"kWh/m2, 30-d MA:",Bawal_ma,", 365-d sum:",year_sum_Bawal,", -",tmy_Bawal,"% vs TMY")
body = paste(body,"\n\nBescom (IN):",today,",",Bescom_ghi,"kWh/m2, ",Bescom_gti,"kWh/m2, 30-d MA:",Bescom_ma,", 365-d sum:",year_sum_Bescom,",",tmy_Bescom,"% vs TMY")
body = paste(body,"\n\nBhilwara (IN):",today,",",Bhilwara_ghi,"kWh/m2, ",Bhilwara_gti,"kWh/m2, 30-d MA:",Bhilwara_ma,", 365-d sum:",year_sum_Bhilwara,",",tmy_Bhilwara,"% vs TMY")
body = paste(body,"\n\nChangi Airport (SG):",today,",",Changi_ghi,"kWh/m2, 30-d MA:",Changi_ma,", 365-d sum:",year_sum_Changi,", -",tmy_Changi,"% vs TMY")
body = paste(body,"\n\nChennai (IN):",today,",",Chennai_ghi,"kWh/m2, 30-d MA:",Chennai_ma,", 365-d sum:",year_sum_Chennai,",",tmy_Chennai,"% vs TMY")
body = paste(body,"\n\nChiplun (IN):",today,",",Chiplun_ghi,"kWh/m2, 30-d MA:",Chiplun_ma,", 365-d sum:",year_sum_Chiplun,",",tmy_Chiplun,"% vs TMY")
body = paste(body,"\n\nCoimbatore (IN):",today,",",Coimbatore_ghi,"kWh/m2, ",Coimbatore_gti,"kWh/m2, 30-d MA:",Coimbatore_ma,", 365-d sum:",year_sum_Coimbatore,",",tmy_Coimbatore,"% vs TMY")
body = paste(body,"\n\nHosur (IN):",today,",",Hosur_ghi,"kWh/m2, 30-d MA:",Hosur_ma,", 365-d sum:",year_sum_Hosur,",",tmy_Hosur,"% vs TMY")
body = paste(body,"\n\nHyderabad (IN):",today,",",Hyderabad_ghi,"kWh/m2, 30-d MA:",Hyderabad_ma,", 365-d sum:",year_sum_Hyderabad,",",tmy_Hyderabad,"% vs TMY")
body = paste(body,"\n\nJhajjar (IN):",today,",",Jhajjhar_ghi,"kWh/m2, 30-d MA:",Jhajjhar_ma,", 365-d sum:",year_sum_Jhajjhar,",",tmy_Jhajjhar,"% vs TMY")
body = paste(body,"\n\nKaraikal (IN):",today,",",Karaikal_ghi,"kWh/m2,30-d MA:",Karaikal_ma,", 365-d sum:",year_sum_Karaikal,",",tmy_Karaikal,"% vs TMY")
body = paste(body,"\n\nKuranganwali (IN):",today,",",Kuranganwali_ghi,"kWh/m2, ",Kuranganwali_gti,"kWh/m2, 30-d MA:",Kuranganwali_ma,", 365-d sum:",year_sum_Kuranganwali,",",tmy_Kuranganwali,"% vs TMY")
body = paste(body,"\n\nLalru (IN):",today,",",Lalru_ghi,"kWh/m2, 30-d MA:",Lalru_ma,", 365-d sum:",year_sum_Lalru,",",tmy_Lalru,"% vs TMY")
body = paste(body,"\n\nMalumbra (IN):",today,",",Malumbra_ghi,"kWh/m2, ",Malumbra_gti,"kWh/m2, 30-d MA:",Malumbra_ma,", 365-d sum:",year_sum_Malumbra,",",tmy_Malumbra,"% vs TMY")
body = paste(body,"\n\nNasik (IN):",today,",",Nasik_ghi,"kWh/m2, ",Nasik_gti,"kWh/m2, 30-d MA:",Nasik_ma,", 365-d sum:",year_sum_Nasik,",",tmy_Nasik,"% vs TMY")
body = paste(body,"\n\nNew Delhi (IN):",today,",",ND_ghi,"kWh/m2, 30-d MA:",ND_ma,", 365-d sum:",year_sum_ND,",",tmy_ND,"% vs TMY")
body = paste(body,"\n\nOragadam (IN):",today,",",Oragadam_ghi,"kWh/m2, 30-d MA:",Oragadam_ma,", 365-d sum:",year_sum_Oragadam,",",tmy_Oragadam,"% vs TMY")
body = paste(body,"\n\nPaka (MY):",today,",",Paka_ghi,"kWh/m2, 30-d MA:",Paka_ma,", 365-d sum:",year_sum_Paka,",",tmy_Paka,"% vs TMY")
body = paste(body,"\n\nPune (IN):",today,",",Pune_ghi,"kWh/m2, 30-d MA:",Pune_ma,", 365-d sum:",year_sum_Pune,",",tmy_Pune,"% vs TMY")
body = paste(body,"\n\nSatna (IN):",today,",",Satna_ghi,"kWh/m2, ",Satna_gti,"kWh/m2, 30-d MA:",Satna_ma,", 365-d sum:",year_sum_Satna,",",tmy_Satna,"% vs TMY")
body = paste(body,"\n\nTak (TH):",today,",",Tak_ghi,"kWh/m2, 30-d MA:",Tak_ma,", 365-d sum:",year_sum_Tak,",",tmy_Tak,"% vs TMY")
body = paste(body,"\n\nTirunelveli (IN):",today,",",Tirunelveli_ghi,"kWh/m2, ",Tirunelveli_gti,"kWh/m2, 30-d MA:",Tirunelveli_ma,", 365-d sum:",year_sum_Tirunelveli,",",tmy_Tirunelveli,"% vs TMY")
body = paste(body,"\n\nTuas (SG):",today,",",Tuas_ghi,"kWh/m2, 30-d MA:",Tuas_ma,", 365-d sum:",year_sum_Tuas,",",tmy_Tuas,"% vs TMY")
body = paste(body,"\n\nWoodlands (SG):",today,",",Woodlands_ghi,"kWh/m2, 30-d MA:",Woodlands_ma,", 365-d sum:",year_sum_Woodlands,",",tmy_Woodlands,"% vs TMY")



#W_FILE



City <- c("Bawal", "ChangiAirport", "Chiplun","Karaikal","Lalru","Malumbra","Malumbra","Paka","Woodlands","Tuas","Tak","Nasik","Nasik","Coimbatore","Coimbatore","Bescom","Bescom","Tirunelveli","Tirunelveli","Satna","Satna","Kuranganwali","Kuranganwali","Bhilwara","Bhilwara","Aurangabad","New Delhi","Oragadam","Pune","Hyderabad","Hosur", "Jhajjar","Chennai")
Country <- c("IN", "SG", "IN","IN","IN","IN","IN","MY","SG","SG","MY","IN","IN","IN","IN","IN","IN","IN","IN","IN","IN","IN","IN","IN","IN","IN","IN","IN","IN","IN","IN","IN","IN")
Lat <- c("28.1094", "1.3644", "17.5319","10.9254","30.4929","17.9300","17.9300","4.6266","30.1658","1.2949","16.8840","20.270197","20.270197","11.103033","11.103033","14.80685","14.80685","8.885125","8.885125","24.571884","24.571884","29.786753","29.786753","25.321367","25.321367","19.837299","28.669777","12.860411","18.516117","17.480219","12.836017","28.503888","13.039953")
Long <- c("76.6025", "103.9915", "73.5151","79.8380","76.8020","76.0080","76.0080","103.4400","95.4613","103.6305","99.1258","73.839717","73.839717","76.9851","76.9851","76.124664","76.124664","77.677131","77.677131","80.995512","80.995512","75.087425","75.087425","74.586903","74.586903","75.237695","77.083597","79.944721","73.705956","78.914536","77.796608","76.758162","80.166937")
LastDayRecord <- c(today,today, today,today,today,today,today,today,today,today,today,today,today, today, today, today, today, today, today, today, today, today, today, today, today, today, today, today, today, today, today, today, today)
LastDayValue <- c(Bawal_ghi, Changi_ghi, Chiplun_ghi, Karaikal_ghi, Lalru_ghi, Malumbra_ghi,Malumbra_gti, Paka_ghi, Woodlands_ghi, Tuas_ghi, Tak_ghi, Nasik_ghi, Nasik_gti,Coimbatore_ghi,Coimbatore_gti,Bescom_ghi, Bescom_gti, Tirunelveli_ghi, Tirunelveli_gti, Satna_ghi, Satna_gti, Kuranganwali_ghi, Kuranganwali_gti, Bhilwara_ghi, Bhilwara_gti, Aurangabad_ghi, ND_ghi,Oragadam_ghi, Pune_ghi, Hyderabad_ghi, Hosur_ghi, Jhajjhar_ghi, Chennai_ghi)
Last30MA <- c(Bawal_ma, Changi_ma, Chiplun_ma, Karaikal_ma, Lalru_ma, Malumbra_ma, Malumbra_ma_gti, Paka_ma, Woodlands_ma, Tuas_ma, Tak_ma, Nasik_ma, Nasik_ma_gti,Coimbatore_ma, Coimbatore_ma_gti, Bescom_ma, Bescom_ma_gti, Tirunelveli_ma, Tirunelveli_ma_gti, Satna_ma, Satna_ma_gti ,Kuranganwali_ma, Kuranganwali_ma_gti, Bhilwara_ma, Bhilwara_ma_gti, Aurangabad_ma,ND_ma, Oragadam_ma, Pune_ma, Hyderabad_ma, Hosur_ma, Jhajjhar_ma, Chennai_ma)
Last365DayTot <- c(year_sum_Bawal, year_sum_Changi, year_sum_Chiplun, year_sum_Karaikal, year_sum_Lalru, year_sum_Malumbra,year_sum_Malumbra_gti, year_sum_Paka, year_sum_Woodlands, year_sum_Tuas, year_sum_Tak, year_sum_Nasik, year_sum_Nasik_gti, year_sum_Coimbatore, year_sum_Coimbatore_gti, year_sum_Bescom, year_sum_Bescom_gti, year_sum_Tirunelveli, year_sum_Tirunelveli_gti, year_sum_Satna, year_sum_Satna_gti, year_sum_Karaikal, year_sum_Kuranganwali_gti,year_sum_Bhilwara, year_sum_Bhilwara_gti,
                   year_sum_Aurangabad, year_sum_ND, year_sum_Oragadam, year_sum_Paka, year_sum_Hyderabad , year_sum_Hosur, year_sum_Jhajjhar, year_sum_Chennai)
FirstMonth <- c("2018-01","2018-01","2020-01","2019-01","2017-01","2020-01","2020-01","2019-01","2019-01","2016-01","2019-12","2016-01","2016-01","2016-01","2016-01","2018-01","2018-01","2020-01","2020-01","2019-01","2019-01","2020-01","2020-01","2017-01","2017-01","2018-01","2016-04","2019-01","2016-01","2018-01","2019-01","2019-01","2016-01")
ExtrapolatedIrradiationMonth <- c(exp_Bawal, exp_Changi, exp_Chiplun, exp_Karaikal, exp_Lalru, exp_Malumbra,exp_Malumbra_gti, exp_Paka, exp_Woodlands, exp_Tuas, exp_Tak, exp_Nasik, exp_Nasik_gti, exp_Coimbatore, exp_Coimbatore_gti, exp_Bescom, exp_Bescom_gti, exp_Tirunelveli, exp_Tirunelveli_gti, exp_Satna, exp_Satna_gti, exp_Kuranganwali, exp_Kuranganwali_gti, exp_Bhilwara, exp_Bhilwara_gti, exp_Aurangabad, exp_ND, exp_Oragadam, exp_Pune, exp_Hyderabad, exp_Hosur, exp_Jhajjhar, exp_Chennai)
G_Type <- c("GHI", "GHI", "GHI","GHI","GHI","GHI","GTI","GHI","GHI","GHI","GHI","GHI","GTI","GHI","GTI","GHI","GTI","GHI","GTI","GHI","GTI", "GHI", "GTI","GHI","GTI","GHI","GHI","GHI","GHI","GHI","GHI","GHI","GHI")
Tilt <- c("0", "0", "0","0","0","0","15","0","0","0","0","0","15","0","12","0","12","0","8","0","14","0","17","0","20","0","0","0","0","0","0","0","0")
Azimuth <- c("0", "0", "0","0","0","0","180","0","0","0","0","0","180","0","180","0","180","0","180","0","180","0","180","0","180","0","180","0","0","0","0","0","0")
SummaryName <- c("BAWAL", "CHANGIAIRPORT", "CHIPLUN","KARAIKAL","LALRU","MALUMBRA","MALUMBRA","PAKA","WOODLANDS","TUA","TAK","NASIK","NASIK","COIMBATORE","COIMBATORE","BESCOM","BESCOM","TIRUNELVELI","TIRUNELVELI","SATNA","SATNA","KURANGANWALI","KURANGANWALI","BHILWARA","BHILWARA","AURANGABAD","NEWDELHI","ORAGADAM","PUNE","HYDERABAD","HOSUR","JHAJJAR","CHENNAI")




df <- data.frame(City, Country, Lat, Long,G_Type, Tilt, Azimuth ,LastDayRecord, LastDayValue, Last30MA, Last365DayTot,FirstMonth,ExtrapolatedIrradiationMonth, SummaryName, df2)
print(df)

write.table(df,"/home/anusha/GIS/W_AllSites_Master.txt",sep="\t",row.names=FALSE)

pathall = c('/home/admin/Dropbox/GIS_API/AURANGABAD/AURANGABAD_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/BAWAL/BAWAL_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/BESCOM/BESCOM_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/BHILWARA/BHILWARA_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/CHANGI/CHANGI_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/CHENNAI/CHENNAI_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/CHIPLUN/CHIPLUN_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/COIMBATORE/COIMBATORE_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/HOSUR/HOSUR_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/HYDERABAD/HYDERABAD_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/JHAJJHAR/JHAJJHAR_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/KARAIKAL/KARAIKAL_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/KURANGANWALI/KURANGANWALI_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/LALRU/LALRU_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/MALUMBRA/MALUMBRA_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/NASIK/NASIK_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/NEW_DELHI/NEW_DELHI_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/ORAGADAM/ORAGADAM_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/PUNE/PUNE_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/PAKA/PAKA_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/SATNA/SATNA_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/TAK/TAK_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/TIRUNELVELI/TIRUNELVELI_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/TUAS/TUAS_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/WOODLANDS/WOODLANDS_AGGREGATE.txt',
            '/home/admin/Dropbox/GIS_API/W_AllSites_Master.txt')

send.mail(from = sender,
          to = recipients,
          subject = paste("API GIS Files Update",today),
          body = body,
          smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
          authenticate = TRUE,
          send = TRUE,
          attach.files = pathall,
          # file.names = filenams, # optional paramete
          debug = F)




