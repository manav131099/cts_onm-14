library(ggplot2)
library(zoo)

args<-commandArgs(TRUE)
date=args[1]

#Using 3rd gen data; not available in sharepoint

system(paste("python /home/pranav/ctsfork/IN301LDigest/Temp_Corrected_PR_Extract.py",date,sep=' '))
pathRead <-  paste("/home/admin/Graphs/Graph_Extract/IN-301/[IN-301] Graph ", date, " - Temperature Corrected PR Evolution.txt", sep="")
graph <- paste("/home/admin/Graphs/Graph_Output/IN-301/[IN-301] Graph ", date, " - Temperature Corrected PR Evolution.pdf", sep="")


dff = read.table(pathRead, header = T,sep='\t') 

for(x in 1 :nrow(dff))
{
	if(is.finite(dff[x,3]) && (as.numeric(dff[x,3]) < 5) || (as.numeric(dff[x,4]) < 50))
		dff[x,3] = NA
}

dff = dff[complete.cases(as.numeric(dff[,3])),]
dff = dff[(dff[,3] <= 100),]
dff = dff[(dff[,3] >= 0),]
dff = dff[(as.Date(dff$Date)<= date),]
dff <- unique(dff)
row.names(dff) <- NULL

#moving average 30d
zoo.PR <- zoo(dff[,3], as.Date(dff$Date))
if(nrow(dff)<120){
ma1 <- rollapplyr(zoo.PR, list(-(6:1)), mean, fill = NA, na.rm = T)
}else{
ma1 <- rollapplyr(zoo.PR, list(-(29:1)), mean, fill = NA, na.rm = T)
}
dff$ambPR.av = coredata(ma1)
row.names(dff) <- NULL

firstdate <- as.Date(dff[1,1])
lastdate <- as.Date(dff[length(dff[,1]),1])

PRUse = as.numeric(dff$PR)
PRUse = PRUse[complete.cases(PRUse)]
last7 = round(mean(tail(PRUse,7)),1)
last30 = round(mean(tail(PRUse,30)),1)
last60 = round(mean(tail(PRUse,60)),1)
last90 = round(mean(tail(PRUse,90)),1)
last365 = round(mean(tail(PRUse,365)),1)
lastlt = round(mean(PRUse),1)
rightIdx = nrow(dff) * 0.75
x1idx = round(nrow(dff) * 0.20, 0)
x2idx = round(nrow(dff) * 0.4, 0)
x3idx = round(nrow(dff) * 0.45, 0)

#data sorting for visualisation
dff$colour[dff[,2] < 2] <- '< 2'
dff$colour[dff[,2] >= 2 & dff[,2] <= 4] <- '2 ~ 4'
dff$colour[dff[,2] >= 4 & dff[,2] <= 6] <- '4 ~ 6'
dff$colour[dff[,2] > 6] <- '> 6'
dff$colour = factor(dff$colour, levels = c("< 2", "2 ~ 4", "4 ~ 6", "> 6"), labels =c('< 2', '2 ~ 4', '4 ~ 6', '> 6')) #to fix legend arrangement
titlesettings <- theme(plot.title = element_text(face = "bold", size = 12, lineheight = 0.7, hjust = 0.5, margin = margin(0,0,7,0)), plot.subtitle = element_text(face = "bold", size = 12, lineheight = 0.9, hjust = 0.5))

p <- ggplot() + theme_bw()
p <- p + geom_point(data=dff, aes(x=as.Date(Date), y = PR, shape = as.factor(18), colour = colour), size = 2) + guides(shape = FALSE)
p <- p + ylab("Performance Ratio [%]") + xlab("") + coord_cartesian(ylim = c(0,100))
if(nrow(dff)<120){
    p <- p + scale_x_date(expand=c(0,0),date_breaks = "7 days", date_labels = "%d/%m/%y", limits = c(firstdate, NA)) + scale_y_continuous(breaks=seq(0, 100, 10))
    p <- p + annotate("segment", x = as.Date(dff[nrow(dff)* 0.35,1]), xend = as.Date(dff[nrow(dff)* 0.38,1]), y=45, yend=45, colour="red", size=1.5)
    p <- p + annotate('text', label = "7-d moving average of PR", y = 45, x = as.Date(dff[x2idx,1]), colour = "red",fontface =2,hjust = 0)
}else{
    p <- p + scale_x_date(expand=c(0,0),date_breaks = "3 months", date_labels = "%b/%y", limits = c(firstdate, NA)) + scale_y_continuous(breaks=seq(0, 100, 10))
    p <- p + annotate("segment", x = as.Date(dff[nrow(dff)* 0.35,1]), xend = as.Date(dff[nrow(dff)* 0.38,1]), y=45, yend=45, colour="red", size=1.5)
    p <- p + annotate('text', label = "30-d moving average of PR", y = 45, x = as.Date(dff[x2idx,1]), colour = "red",fontface =2,hjust = 0)
}
p <- p + theme(axis.title.x = element_text(size=13), axis.title.y = element_text(size=13), axis.text = element_text(size=13), panel.grid.minor = element_blank(),                 panel.border = element_blank(), axis.line = element_line(colour = "black"), legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97))                     #LEGNED AT RHS
p <- p + titlesettings + ggtitle('Temperature Corrected Performance Ratio Evolution - IN-301', subtitle = paste0('From ', firstdate, ' to ', lastdate))
p <- p + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))
p <- p + scale_colour_manual('Daily Irradiation [kWh/m2]', values = c("blue", "deepskyblue1", "orange","darkorange3"), labels = c('< 2','2 ~ 4','4 ~ 6','> 6'),                                 guide = guide_legend(title.position = "left", ncol = 4, nrow=1)) 
p <- p + scale_shape_manual(values = 18)
p <- p + theme(legend.box = "horizontal", legend.direction="horizontal") #legend.position = "bottom"
p <- p + geom_hline(yintercept = 76.3, colour="darkgreen", size=1) 

p <- p + guides(colour = guide_legend(override.aes = list(shape = 18)))
p <- p + geom_line(data=dff, aes(x=as.Date(Date), y=ambPR.av), color="red", size=1.5)
dff$Date=as.Date(dff$Date)
p <- p + annotate("segment", x = as.Date(dff[nrow(dff)* 0.35,1]), xend = as.Date(dff[nrow(dff)* 0.38,1]), y=50, yend=50, colour="darkgreen", size=1.5)
p <- p + annotate('text', label = paste("Target Budget Yield Performance Ratio [", format(76.3,nsmall=1), "%]", sep=""), y = 50, x = as.Date(dff[x2idx,1]), colour = "darkgreen",fontface =2,hjust = 0)
p <- p + annotate('text', label = paste("Points above Target Budget PR = ",sum(dff$PR > 76.3),'/',nrow(dff),' = ',round((sum(dff$PR > 76.3)/nrow(dff))*100,1),'%',sep=''), y = 40, x = as.Date(dff[x2idx,1]), colour = "black",fontface =2,hjust = 0)

p <- p + annotate('text', label = paste("Average PR last 7-d:", last7, "%"), y=30, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 30-d:", last30, "%"), y=25, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 60-d:", last60, "%"), y=20, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 90-d:", last90, "%"), y=15, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 365-d:", last365,"%"), y=10, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR Lifetime:", lastlt,"%"), y=5, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0,fontface=2)
p <- p + annotate("segment", x = as.Date(dff[nrow(dff),1]), xend = as.Date(dff[nrow(dff),1]), y=0, yend=100, colour="deeppink4", size=1.5, alpha=0.5)

ggsave(paste0(graph), p, width = 11, height=8)
