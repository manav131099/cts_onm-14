source('/home/admin/CODE/common/aggregate.R')

registerMeterList("PH-006S",c(""))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 28 #Column no for DA
aggColTemplate[3] = 24 #column for LastRead
aggColTemplate[4] = 25 #column for LastTime
aggColTemplate[5] = 18 #column for Eac-1
aggColTemplate[6] = 19 #column for Eac-2
aggColTemplate[7] = 22 #column for Yld-1
aggColTemplate[8] = 23 #column for Yld-2
aggColTemplate[9] = 20 #column for PR-1
aggColTemplate[10] = 21 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 5 #column for Tamb
aggColTemplate[14] = 17 #column for Tmod
aggColTemplate[15] = 11 #column for Hamb

registerColumnList("PH-006S","",aggNameTemplate,aggColTemplate)
