rm(list=ls())
library(stringr)
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN070LMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/IN070LDigest/summaryFunctions.R')
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
RESETHISTORICAL=0
daysAlive = 0
reorderStnPaths = c(7,9,13,1,2,3,4,5)

source('/home/admin/CODE/MasterMail/timestamp.R')
require('mailR')
source('/home/admin/CODE/IN070LDigest/aggregateInfo.R')
Gmod = NA
TOTALGENCALC = 0
METERNICKNAMES = c("ESS-03","ESS-02","ESS-10","INV-1","INV-2","INV-3","INV-4","INV-5")
METERACNAMES = c("MFM_1_ESS-03 Main_MFM","MFM_3_ESS-02 Main_MFM","MFM_7_ESS-10 Main_MFM","INVERTER_1","INVERTER_2","INVERTER_3","INVERTER_4","INVERTER_5")
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

DOB2 = "2019-05-03"
DOB2=as.Date(DOB2)
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
# recipients =getRecipients("IN-070L","m")
recipients = c('operationsSouthIN@cleantechsolar.com','om-it-digest@cleantechsolar.com','oms@avisolar.com')
pwd = 'CTS&*(789'
referenceDays = NA
extractDaysOnly = function(days)
{
  seq = seq(from=1,to=length(days)*6,by=6)
  days = unlist(strsplit(days,"-"))
  days = paste(days[seq+3],days[seq+4],days[seq+5],sep="-")
  return(days)
}
analyseDays = function(days,ref)
{
  if(length(days) == length(ref))
    return(days)
  daysret = unlist(rep(NA,length(ref)))
  if(length(days) == 0)
    return(daysret)
  days2 = extractDaysOnly(days)
  idxmtch = match(days2,ref)
  idxmtch2 = idxmtch[complete.cases(idxmtch)]
  if(length(idxmtch) != length(idxmtch2))
  {
    print(".................Missmatch..................")
    print(days2)
    print("#########")
    print(ref)
    print("............................................")
    return(days)
  }
  if(is.finite(idxmtch))
  {
    daysret[idxmtch] = as.character(days)
    if(!(is.na(daysret[length(daysret)])))
      return(daysret)
  }	
  return(days)
}
performBackWalkCheck = function(day){
  path = "/home/admin/Dropbox/Gen 1 Data/[IN-070L]"
  retval = 0
  yr = substr(day,1,4)
  yrmon = substr(day,1,7)
  pathprobe = paste(path,yr,yrmon,sep="/")
  stns = dir(pathprobe)
  print(stns)
  idxday = c()
  for(t in 1 : length(stns)){
    pathdays = paste(pathprobe,stns[t],sep="/")
    print(pathdays)
    days = dir(pathdays)
    dayMtch = days[grepl(day,days)]
    if(length(dayMtch)){
      idxday[t] = match(dayMtch,days)
    }
    print(paste("Match for",day,"is",idxday[t]))
  }
  idxmtch = match(NA,idxday[t])
  if(length(unique(idxday))==1 || length(idxmtch) < 5){
    print('All days present ')
    retval = 1
  }
  return(retval)
}
sendMail= function(pathall){
  path = pathall[1]
  dataread = read.table(path,header = T,sep="\t")
  
  len = str_length(path)
  wms_date = substr(path,len-13,len-4)
  wms_yr = substr(path, len-13, len-10)
  wms_yr_mon = substr(path, len-13, len-7)
  dataread = read.table(path,header = T,sep="\t")
  wms_printed = 0

  currday = as.character(dataread[1,1])
  Cur=as.Date(currday)
  filenams = c()
  body = ""
  body = paste(body,"Site Name: Apollo Tyres Phase 2",sep="")
  body = paste(body,"\n\nLocation: Chennai, India ")
  body = paste(body,"\n\nO&M Code: IN-070")
  body = paste(body,"\n\nSystem Size [kWp]:",INSTCAP)
  body = paste(body, "\n\nSystem Size Facing East: 4178.1 kWp (45.1% of total)")
  body = paste(body, "\n\nSystem Size Facing West: 4707.0 kWp (50.8% of total)")
  body = paste(body, "\n\nSystem Size Facing South: 99 kWp (1.1% of total)")
  body = paste(body, "\n\nSystem Size Facing North: 283.8 kWp (3.1% of total)")
  body = paste(body,"\n\nNumber of Energy Meters: 3")
  body = paste(body,"\n\nModule Brand / Model / Nos: JA Solar JAP72S01-(325-330W), Trina TSM-PE14A (325-330W)")
  body = paste(body,"\n\nInverter Brand / Model / Nos: ABB PV800-57B-1732 kW- 5 Nos")
  body = paste(body,"\n\nSite COD: 2019-05-03")
  body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(Cur-DOB2))))
  body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(Cur-DOB2))/365,1)))
  
  bodyac = body
  body = ""
  TOTALGENCALC = 0
  filename = paste("[IN-015S] ",Cur,".txt",sep="")
    path = "/home/admin/Dropbox/Second Gen/[IN-015S]"
    pathRead = paste(path,wms_yr,wms_yr_mon,filename,sep="/")
    GTIGreater20=NA
    GTI = DNI = NA
    if(file.exists(pathRead))
    {
      dataread = read.table(pathRead,sep="\t",header = T)

      if(nrow(dataread) > 0)
      {
        GTI1 = as.numeric(dataread[1,3])
        GTI2 = as.numeric(dataread[1,4])
        GTI3 = as.numeric(dataread[1,5])
        GTI4 = as.numeric(dataread[1,44])
        GTI5 = as.numeric(dataread[1,45])
        body = paste(body,"\n\n_____________________________________________\n",sep="")
        body = paste(body,"WMS")
        body  = paste(body,"\n_____________________________________________\n",sep="")
        body = paste(body,"\nDaily Irradiation Silicon Sensor [kWh/m2]: ",as.character(dataread[1,3]),sep="")
        body = paste(body,"\n\nDaily Irradiation Silicon Sensor, North [kWh/m2]: ",as.character(dataread[1,4]),sep="")
        body = paste(body,"\n\nDaily Irradiation Silicon Sensor, South [kWh/m2]: ",as.character(dataread[1,5]),sep="")
        body = paste(body,"\n\nDaily Irradiation Silicon Sensor, East [kWh/m2]: ",as.character(dataread[1,44]),sep="")
        body = paste(body,"\n\nDaily Irradiation Silicon Sensor, West [kWh/m2]: ",as.character(dataread[1,45]),sep="")
        body = paste(body,"\n\nWeighted Gmod for Site [kWh/m2]: ",round((dataread[1,44]*0.451)+(dataread[1,45]*0.508)+(dataread[1,4]*0.011)+(dataread[1,5]*0.031) ,2),sep="")
        body = paste(body,"\n")
        Gmod = round((dataread[1,44]*0.451)+(dataread[1,45]*0.508)+(dataread[1,4]*0.011)+(dataread[1,5]*0.031) ,2)
        
  
      }
    }
  MYLD = c()
  
  acname = METERNICKNAMES
  noInverters = NOINVS
  globMtYld=c()
  idxglobMtYld=1
  InvReadings = unlist(rep(NA,noInverters))
  InvAvail = unlist(rep(NA,noInverters))
  for(t in 1 : length(pathall)){
    type = 0
    meteridx = t
    if(grepl("WMS",pathall[t])) 
      type = 1
    else if(grepl("ESS",pathall[t]))
      type = 1
    else
      type = 0
    if( t > (NOMETERS)){
      splitpath = unlist(strsplit(pathall[t],"INVERTER_"))
      if(length(splitpath)>1){
        meteridx = unlist(strsplit(splitpath[2],"/"))
        meteridx = as.numeric(meteridx[1])
      }
    }
    path = pathall[t]
    dataread = read.table(path,header = T,sep="\t")
    currday = as.character(dataread[1,1])
    if(type==0){
      filenams[t] = paste(currday,"-",METERNICKNAMES[meteridx+1],".txt",sep="")
    }else if(type==1 && grepl("WMS",pathall[t])){
      filenams[t] = paste(currday,"-",'WMS',".txt",sep="")
    }else{
      if(grepl("ESS-03",pathall[t])){
        filenams[t] = paste(currday,"-",'MFM1',".txt",sep="") 
      }else if(grepl("ESS-02",pathall[t])){
        filenams[t] = paste(currday,"-",'MFM3',".txt",sep="") 
      }else{
        filenams[t] = paste(currday,"-",'MFM7',".txt",sep="")
      }
      
    }
    print("---------------------")
    print(path)
    print(filenams[t])
    print("---------------------")
    
    if(type == 1)
    {
      body = paste(body,"\n________________________________________________\n\n")
      body = paste(body,currday,acname[t])
      body = paste(body,"\n________________________________________________\n\n")
      body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
      {
        if(grepl("ESS",pathall[t])){
          if(grepl("ESS-03",pathall[t])){
          body = paste(body,"System Size [kWp]:",INSTCAPM[1],"\n\n")
          body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(format(round(dataread[1,3],1), nsmall = 1)),"\n\n")
          body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(format(round(dataread[1,4],1), nsmall = 1)),"\n\n")
          TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
          yld1 = round(dataread[1,3]/INSTCAPM[1],2)
          yld2 = round(dataread[1,4]/INSTCAPM[1],2)
          body = paste(body,"Yield-1 [kWh/kWp]:",as.character(format(round(yld1,2), nsmall = 2)),"\n\n")
          body = paste(body,"Yield-2 [kWh/kWp]:",as.character(format(round(yld2,2), nsmall = 2)),"\n\n")
          body = paste(body,"PR-1 (GHI) [%]:",as.character(format(round((yld1*100)/Gmod,1), nsmall = 1)),"\n\n")
          body = paste(body,"PR-2 (GHI) [%]:",as.character(format(round((yld2*100)/Gmod,1), nsmall = 1)),"\n\n")
          globMtYld[idxglobMtYld] = yld2
          idxglobMtYld = idxglobMtYld + 1
          }else if(grepl("ESS-02", pathall[t])){
          body = paste(body,"System Size [kWp]:",INSTCAPM[2],"\n\n")
          body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(format(round(dataread[1,3],1), nsmall = 1)),"\n\n")
          body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(format(round(dataread[1,4],1), nsmall = 1)),"\n\n")
    
          TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
          yld1 = round(dataread[1,3]/INSTCAPM[2],2)
          yld2 = round(dataread[1,4]/INSTCAPM[2],2)
          body = paste(body,"Yield-1 [kWh/kWp]:",as.character(format(round(yld1,2), nsmall = 2)),"\n\n")
          body = paste(body,"Yield-2 [kWh/kWp]:",as.character(format(round(yld2,2), nsmall = 2)),"\n\n")
          body = paste(body,"PR-1 (GHI) [%]:",as.character(format(round((yld1*100)/Gmod,1), nsmall = 1)),"\n\n")
          body = paste(body,"PR-2 (GHI) [%]:",as.character(format(round((yld2*100)/Gmod,1), nsmall = 1)),"\n\n")
          globMtYld[idxglobMtYld] = yld2
          idxglobMtYld = idxglobMtYld + 1
          }else if(grepl("ESS-10", pathall[t])){
          body = paste(body,"System Size [kWp]:",INSTCAPM[3],"\n\n")
          body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(format(round(dataread[1,3],1), nsmall = 1)),"\n\n")
          body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(format(round(dataread[1,4],1), nsmall = 1)),"\n\n")
          TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
          yld1 = round(dataread[1,3]/INSTCAPM[3],2)
          yld2 = round(dataread[1,4]/INSTCAPM[3],2)
          body = paste(body,"Yield-1 [kWh/kWp]:",as.character(format(round(yld1,2), nsmall = 2)),"\n\n")
          body = paste(body,"Yield-2 [kWh/kWp]:",as.character(format(round(yld2,2), nsmall = 2)),"\n\n")
          body = paste(body,"PR-1 (GHI) [%]:",as.character(format(round((yld1*100)/Gmod,1), nsmall = 1)),"\n\n")
          body = paste(body,"PR-2 (GHI) [%]:",as.character(format(round((yld2*100)/Gmod,1), nsmall = 1)),"\n\n")
          globMtYld[idxglobMtYld] = yld2
          idxglobMtYld = idxglobMtYld + 1
          
          }
          
         
          
          path_inv = "/home/admin/Dropbox/Gen 1 Data/[IN-070L]"

          if(grepl("ESS-03",pathall[t])){
            filename_mfm = paste("[IN-070L]-MFM1-",Cur,".txt",sep = "")
            pathread_mfm = paste(path_inv, wms_yr, wms_yr_mon,"MFM_1_ESS-03 Main_MFM",filename_mfm, sep = "/")
            dataread_mfm = read.table(pathread_mfm, sep = "\t", header = T)
          }else if (grepl("ESS-02",pathall[t])) {
            filename_mfm = paste("[IN-070L]-MFM3-",Cur,".txt",sep = "")
            pathread_mfm = paste(path_inv, wms_yr, wms_yr_mon,"MFM_3_ESS-02 Main_MFM",filename_mfm, sep = "/")
            dataread_mfm = read.table(pathread_mfm, sep = "\t", header = T)
          }else{
            filename_mfm = paste("[IN-070L]-MFM7-",Cur,".txt",sep = "")
            pathread_mfm = paste(path_inv, wms_yr, wms_yr_mon,"MFM_7_ESS-10 Main_MFM",filename_mfm, sep = "/")
            dataread_mfm = read.table(pathread_mfm, sep = "\t", header = T)
          }
          imp_en1 = dataread_mfm[61]
          imp_en1 = imp_en1[complete.cases(imp_en1), ]
          imp_en_value1 = imp_en1[1]
          imp_en_last1 = tail(imp_en1, n=1)
          imp_en_last1 = imp_en_last1/1000
          body = paste(body,"Import Energy Reading [kWh]:",round(imp_en_last1,2),"\n\n")
          body = paste(body,"Last recorded value [kWh]:",as.character(format(round(dataread[1,9],1), nsmall = 1)),"\n\n")
          body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n")
          
          # body = paste(body,"\n\nGrid Availability [%]:",as.character(dataread[1,13]),"\n\n")
          # body = paste(body,"Plant Availability [%]:",as.character(dataread[1,14]))
        }
      }
      next
    }
    InvReadings[meteridx] = as.numeric(dataread[1,7])
    InvAvail[meteridx]= as.numeric(dataread[1,10])
    MYLD[(meteridx)] = as.numeric(dataread[1,7])
  }
  body = paste(body,"\n________________________________________________\n\n")
  body = paste(body,currday,"Inverters")
  body = paste(body,"\n________________________________________________")
  MYLDCPY = MYLD
  if(length(MYLD)){
    MYLD = MYLD[complete.cases(MYLD)]
  }
  addwarn = 0
  sddev = covar = NA
  if(length(MYLD)){
    addwarn = 1
    sddev = round(sdp(MYLD)/10,2)
    meanyld = mean(MYLD)
    covar = round((sdp(MYLD)*100/meanyld),1)
  }
  MYLD = MYLDCPY
  for(t in 1 : noInverters){
    body = paste(body,"\n\n Yield ",acname[(t+NOMETERS)],": ",as.character(round(InvReadings[t]/10,2)),sep="")

  }
  body = paste(body,"\n\nStdev/COV Yields: ",sddev," / ",covar,"%",sep="")
  # for(t in 1 : noInverters){
  #   body = paste(body,"\n\n Inverter Availability ",acname[(t+NOMETERS)]," [%]: ",as.character(InvAvail[t]),sep="")
  # }
  bodyac = paste(bodyac,"\n\nSystem Full Generation [kWh]:",TOTALGENCALC)	
  bodyac = paste(bodyac,"\n\nSystem Full Yield [kWh/kWp]:", (format(round(TOTALGENCALC/INSTCAP, 2))))	
  bodyac = paste(bodyac,"\n\nSystem Full PR [%]:", format(round(TOTALGENCALC/INSTCAP/Gmod*100, 1)), "\n\n")
	for(t in 1 : NOMETERS)	
{	
bodyac = paste(bodyac,"Yield",acname[(t)],"[kWh/kWp]:",as.character(format(round(globMtYld[t],2), nsmall = 2,)),"\n\n")	
}
  sdm =round(sdp(globMtYld),3)
  covarm = round(sdm*100/mean(globMtYld),1)
  bodyac = paste(bodyac,"Stdev/COV Yields:",as.character(format(round(sdm,2), nsmall = 1)),"/",as.character(format(round(covarm,1), nsmall = 1)),"[%]","\n")
  
  body = gsub("\n ","\n",body)
  body = paste(bodyac,body,sep="")
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-070L] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = pathall,
            file.names = filenams, # optional paramete
            debug = F)
  recordTimeMaster("IN-070L","Mail",currday)
}


path = "/home/admin/Dropbox/Gen 1 Data/[IN-070L]"
path2G = '/home/admin/Dropbox/Second Gen/[IN-070L]'
path3G = '/home/admin/Dropbox/Third Gen/[IN-070L]'
path4G = '/home/admin/Dropbox/Fourth_Gen/[IN-070L]'


if(RESETHISTORICAL != 0)
{
  print('abcdefxyz')
  command = paste("rm -rf",path2G)
  system(command)
  command = paste("rm -rf",path3G)
  system(command)
  command = paste("rm -rf",path4G)
  system(command)
}

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "IN-070L"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"]-I5-",lastdatemail,".txt",sep="")
print(paste('StopDate is',stopDate))
ENDCALL=0
for(x in 1 : length(years))
{
  path2Gyr = paste(path2G,years[x],sep = "/")
  pathyr = paste(path,years[x],sep="/")
  checkdir(path2Gyr)
  months = dir(pathyr)
  for(y in 1 : length(months))
  {
    path2Gmon = paste(path2Gyr,months[y],sep = "/")
    pathmon = paste(pathyr,months[y],sep="/")
    checkdir(path2Gmon)
    stns = dir(pathmon)
    stns = stns[reorderStnPaths]
    dunmun = 0
    for(t in 1 : length(stns))
    {
      type = 1
      if(grepl("ESS", stns[t]))
      {
        type = 0
      }
      if(grepl("WMS", stns[t]))
        type = 2
      pathmon1 = paste(pathmon,stns[t],sep="/")
      days = dir(pathmon1)
      path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
      if(!file.exists(path2Gmon1))
      {	
        dir.create(path2Gmon1)
      }
      
      
      if(length(days)>0)
      {
        for(z in 1 : length(days))
        {
          if(ENDCALL == 1)
            break
          if((z==length(days)) && (y == length(months)) && (x ==length(years)))
            next
          print(days[z])
          pathfinal = paste(pathmon1,days[z],sep = "/")
          path2Gfinal = paste(path2Gmon1,days[z],sep="/")
          if(RESETHISTORICAL)
          {
            secondGenData(pathfinal,path2Gfinal,type)
          }
          if(!dunmun){
            daysAlive = daysAlive+1
          }
          if(days[z] == stopDate)
          {
            ENDCALL = 1
            print('Hit end call')
            next
          }
        }
      }
      dunmun = 1
      if(ENDCALL == 1)
        break
    }
    if(ENDCALL == 1)
      break
  }
  if(ENDCALL == 1)
    break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
newlen=1
while(1)
{
  # recipients = getRecipients("IN-070L","m")
  recipients = c('operationsSouthIN@cleantechsolar.com','om-it-digest@cleantechsolar.com','oms@avisolar.com')
  recordTimeMaster("IN-070L","Bot")
  years = dir(path)
  noyrs = length(years)
  path2Gfinalall = vector('list')
  for(x in prevx : noyrs)
  {
    pathyr = paste(path,years[x],sep="/")
    path2Gyr = paste(path2G,years[x],sep="/")
    checkdir(path2Gyr)
    mons = dir(pathyr)
    nomons = length(mons)
    startmn = prevy
    endmn = nomons
    if(startmn>endmn)
    {
      startmn = 1
      prevx = x-1
      prevz = 1
    }
    for(y in startmn:endmn)
    {
      pathmon = paste(pathyr,mons[y],sep="/")
      path2Gmon = paste(path2Gyr,mons[y],sep="/")
      checkdir(path2Gmon)
      stns = dir(pathmon)
      if(length(stns) < 2)
      {
        print('Station sync issue.. sleeping for an hour')
        Sys.sleep(3600) # Sync issue, meter data comes after MFM and WMS
        stns = dir(pathmon)
      }
      stns = stns[reorderStnPaths]
      for(t in 1 : length(stns))
      {
        newlen = (y-startmn+1) + (x-prevx)
        print(paste('Reset newlen to',newlen))
        type = 1
        if(grepl("ESS", stns[t]))
        {
          type = 0
        }
        if(grepl("WMS", stns[t]))
          type = 2
        pathmon1 = paste(pathmon,stns[t],sep="/")
        days = dir(pathmon1)
        path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
        chkcopydays = days[grepl('Copy',days)]
        if(!file.exists(path2Gmon1))
        {
          dir.create(path2Gmon1)
        }
        if(length(chkcopydays) > 0)
        {
          print('Copy file found they are')
          print(chkcopydays)
          idxflse = match(chkcopydays,days)
          print(paste('idx matches are'),idxflse)
          for(innerin in 1 : length(idxflse))
          {
            command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
            print(paste('Calling command',command))
            system(command)
          }
          days = days[-idxflse]
        }
        days = days[complete.cases(days)]
        if(t==1)
          referenceDays <<- extractDaysOnly(days)
        else
          days = analyseDays(days,referenceDays)
        nodays = length(days)
        if(y > startmn)
        {
          z = prevz = 1
        }
        if(nodays > 0)
        {
          startz = prevz
          if(startz > nodays)
            startz = nodays
          for(z in startz : nodays)
          {
            if(t == 1)
            {
              path2Gfinalall[[newlen]] = vector('list')
            }
            if(is.na(days[z]))
              next
            pathdays = paste(pathmon1,days[z],sep = "/")
            path2Gfinal = paste(path2Gmon1,days[z],sep="/")
            secondGenData(pathdays,path2Gfinal,type)
            if((z == nodays) && (y == endmn) && (x == noyrs))
            {
              if(!repeats)
              {
                print('No new data')
                repeats = 1
              }
              next
            }
            if(is.na(days[z]))
            {
              print('Day was NA, do next in loop')
              next
            }
            SENDMAILTRIGGER <<- 1
            if(t == 1)
            {
              splitstr =unlist(strsplit(days[z],"-"))
              daysSend = paste(splitstr[4],splitstr[5],splitstr[6],sep="-")
              print(paste("Sending",daysSend))
              if(performBackWalkCheck(daysSend) == 0)
              {
                print("Sync issue, sleeping for an 1 hour")
                Sys.sleep(3600) # Sync issue -- meter data comes a little later than
                # MFM and WMS
              }
            }
            repeats = 0
            print(paste('New data, calculating digests',days[z]))
            path2Gfinalall[[newlen]][t] = paste(path2Gmon1,days[z],sep="/")
            newlen = newlen + 1
            print(paste('Incremented newlen by 1 to',newlen))
          }
        }
      }
    }
  }
  if(SENDMAILTRIGGER)
  {
    print('Sending mail')
    print(paste('newlen is ',newlen))
    for(lenPaths in 1 : (newlen - 1))
    {
      if(lenPaths < 1)
        next
      print(unlist(path2Gfinalall[[lenPaths]]))
      pathsend = unlist(path2Gfinalall[[lenPaths]])
      daysAlive = daysAlive+1
      sendMail(pathsend)
    }
    SENDMAILTRIGGER <<- 0
    newlen = 1
    print(paste('Reset newlen to',newlen))
    
  }
  
  prevx = x
  prevy = y
  prevz = z
  Sys.sleep(300)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
sink()