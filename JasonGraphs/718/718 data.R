rm(list=ls(all =TRUE))

###     IN-713 DATA EXTRACTION       ###
pathRead <- "/home/admin/Dropbox/Cleantechsolar/1min/[718]"
pathWrite <- "/home/admin/Jason/cec intern/results/718/[718]_da_summary.txt"
pathWrite2 <- "/home/admin/Jason/cec intern/results/718/[718]_da_summary.csv"
setwd(pathRead)
filelist <- dir(pattern = ".txt", recursive= TRUE)
timemin <- format(as.POSIXct("2017-01-10 05:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-11-06 20:00:00"), format="%H:%M:%S")

nameofStation <- "718"

col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col7 <- c()
col8 <- c()
col9 <- c()
col10 <- c()
col11 <- c()
col0[10^5] = col1[10^5] = col2[10^5] = col3[10^5] = col4[10^5] =col5[10^5]=col6[10^5]=col7[10^5] =col8[10^5]=col9[10^5]=col10[10^5]=col11[10^5]=0
index <- 1

for (i in filelist[-1]){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  condition <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin &
    format(as.POSIXct(temp[,1]), format="%H:%M:%S") < timemax
  date <- substr(paste(temp[1,1]),1,10)
  
  col0[index] <- nameofStation
  col1[index] <- date
  
  #col5[index] <- length(temp[,3][temp[,3]>5])       #no.of points > 5w/m2
  col4[index] <- length(temp[,3][temp[,3] == "NA"])      #no. of NA pts at col 3
  
  #data availability
  pts <- length(temp[,1])            # no. of rows in txt
  col3[index] <- pts                  #length of column of txt 
  
  sp_temp <-  temp[condition,]        #melt data to time
  
  col5[index] <- length(sp_temp[,3])      #pts at sunhours
  
  col6[index] <- (length(sp_temp[,3])/840)*100
  col2[index] <- ((col3[index]-col4[index])/1440)*100
  
  #gsi 
  gsi <- as.numeric(abs(temp[,3]))   #spm10
  col7[index] <- round(sum(gsi)/60000,2)    
  
  gsi2 <- as.numeric(abs(temp[,4]))  #avggsi00
  col8[index] <- round(sum(gsi2)/60000,2)
  
  gsi3 <- as.numeric(abs(temp[,5]))  #avggti_nw
  col9[index] <- round(sum(gsi3)/60000,2)
  
  gsi4 <- as.numeric(abs(temp[,6]))   #avggti_ctc
  col10[index] <- round(sum(gsi4)/60000,2)
  
  gsi5 <- as.numeric(abs(temp[,8]))   #avggti_ctd
  col11[index] <- round(sum(gsi5)/60000,2)
  
  print(paste(i, "done"))
  
  index <- index + 1
}

col0 <- col0[1:(index)]
col1 <- col1[1:(index)]
col2 <- col2[1:(index)]
col3 <- col3[1:(index)]
col4 <- col4[1:(index)]
col5 <- col5[1:(index)]
col6 <- col6[1:(index)]
col7 <- col7[1:(index)]
col8 <- col8[1:(index)]
col9 <- col9[1:(index)]
col10 <- col10[1:(index)]
col11 <- col11[1:(index)]

#col2 <- ((col3-col5)/1440)*100
col2[is.na(col2)] <- NA              #states T/F
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col5[is.na(col5)] <- NA
col6[is.na(col6)] <- NA
col7[is.na(col7)] <- NA


print("Starting to save..")

result <- cbind(col0,col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11)
colnames(result) <- c("Meter Reference","Date","DA%_Fullday",
                      "No. of Points", "Pts_with_NA","Pts_6to8",'DA%_6to8',
                      "SMP10","GSI00","GTI_NW","GTI_CTC","GTI_CTD")

for(x in c(3,6,7,8,9,10,11,12)){
  result[,x] <- round(as.numeric(result[,x]),3)
  #print(result[,x])
}


rownames(result) <- NULL
result <- data.frame(result)    
result <- result[-length(result[,1]),]   #removing last row 
dataWrite <- result
write.table(result,na = "",pathWrite,row.names = FALSE,sep ="\t")
write.csv(result,pathWrite2, na= "", row.names = FALSE)



