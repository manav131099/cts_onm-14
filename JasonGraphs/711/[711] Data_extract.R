rm(list=ls(all =TRUE))

###     IN-711A DATA EXTRACTION       ###
pathRead <- "/home/admin/Dropbox/Cleantechsolar/1min/[711]"
pathWrite <- "/home/admin/Jason/cec intern/results/711/[IN-711]_summary.txt"
pathWrite2 <- "/home/admin/Jason/cec intern/results/711/[IN-711]_summary.csv"
setwd(pathRead)                                     #set working directory
print("Extracting data..")
filelist <- dir(pattern = ".txt", recursive= TRUE)  #contains only files of '.txt' format

nameofStation <- "IN-711A"

col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col7 <- c()
col8 <- c()
col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = col5[10^6] = col6[10^6] = col7[10^6]= col8[10^6]=0

index <- 1

for (i in filelist[-1]){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")  #reading text file at location filelist[i]
  date <- substr(paste(temp[1,1]),1,10)         #manually obtain the date from the text filename
  col0[index] <- nameofStation
  col1[index] <- date
  
  #data availability
  pts <- temp[,2]
  col3[index] <- pts
  
  #gsi - global solar irradiance
  gsi <- as.numeric(temp[,5])     #5
  col4[index] <- sum(gsi)
  
  #hamb&tamb - ambient humidity and temperature
  tamb <- mean(as.numeric(temp[,10]))    #10 
  col5[index] <- tamb
  hamb <- mean(as.numeric(temp[,6]))
  col6[index] <- hamb
  
  #gsi/pyr ratio 
  gsi1 <- sum(as.numeric(temp[,3]))/60000
  gsi2 <- sum(as.numeric(temp[,4]))/60000
  pyr <- sum(as.numeric(temp[,5]))/60000
  col7[index] <- pyr/gsi2 * 100
  col8[index] <- gsi1/gsi2 * 100
  print(paste(i, "done"))
  index <- index + 1
}
  
col0 <- col0[1:(index-1)]   #removes last row for all columns
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col5 <- col5[1:(index-1)]
col6 <- col6[1:(index-1)]
col7 <- col7[1:(index-1)]
col8 <- col8[1:(index-1)]
col2 <- col3/max(col3)*100          #calculate DA with no. of points
col2[is.na(col2)] <- NA              #states T/F
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col5[is.na(col5)] <- NA
col6[is.na(col6)] <- NA
col7[is.na(col7)] <- NA
col8[is.na(col8)] <- NA

print("Starting to save..")

result <- cbind(col0,col1,col2,col3,col4,col5,col6,col7,col8)
colnames(result) <- c("Meter Reference","Date","Data Availability [%]",
                      "No. of Points","GSI [J/m^2]","Tamb [C]","Hamb [%]",
                      "pyr/gsi2 ratio [%]","Gsi1/gsi2 ratio [%]")         #columns names

for(x in c(3,5,6,7,8,9)){       
  result[,x] <- round(as.numeric(result[,x]),1)
  #print(result[,x])
}
  

rownames(result) <- NULL
result <- data.frame(result)    
#result <- result[-length(result[,1]),]   #removing last row 
#use head(xxx,5) to view first 5 of the dataframe
dataWrite <- result
write.table(result,na = "",pathWrite,row.names = FALSE,sep ="\t")   #saving into txt and csv file
write.csv(result,pathWrite2, na= "", row.names = FALSE)



  
