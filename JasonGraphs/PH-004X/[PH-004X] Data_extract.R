rm(list=ls(all =TRUE))

###     PH-004X DATA EXTRACTION       ###

pathRead <- "~/intern/data/[PH-004X]"
pathwritetxt <- "C:/Users/talki/Desktop/cec intern/results/PH-004X/[PH-004X]_summary.txt"
pathwritecsv <- "C:/Users/talki/Desktop/cec intern/results/PH-004X/[PH-004X]_summary.csv"

setwd(pathRead)
filelist <- dir(pattern = ".txt", recursive= TRUE)

nameofStation <- "PH-004A"

col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = col5[10^6] = col6[10^6] = 0
index <- 1

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(paste(temp[1,1]),1,10)
  col0[index] <- nameofStation
  col1[index] <- date
  
  #data availability
  pts <- length(temp[,1])
  col3[index] <- pts
  
  #gsi
  gsi <- as.numeric(temp[,48][!is.na(temp[,48])])
  col4[index] <- sum(gsi)/60000
  
  #tamb
  tamb <- mean(as.numeric(temp[,49][!is.na(temp[,49])]))
  col5[index] <- tamb
  #index <- index + 1
  
  print(paste(i, "done"))
  
  index <- index + 1
}

col0 <- col0[1:(index-1)]
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col5 <- col5[1:(index-1)]
col6 <- col6[1:(index-1)]
col3[col3>1440] = 1440
col2 <- col3/max(col3)*100
col2[is.na(col2)] <- NA
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col5[is.na(col5)] <- NA
col6[is.na(col6)] <- NA

col4[col4<0] <- NA
col5[col5<0] <- NA
col5[col5>45] <- NA


result <- cbind(col0,col1,col2,col3,col4,col5,col6)
colnames(result) <- c("Meter Reference","Date","Data Availability [%]",
                      "No. of Points","GSI [J/m^2]","Tamb [C]","Hamb [%]")
result[,3] <- round(as.numeric(result[,3]),1)
result[,5] <- round(as.numeric(result[,5]),1)
result[,6] <- round(as.numeric(result[,6]),1)
result[,7] <- round(as.numeric(result[,7]),1)

rownames(result) <- NULL
result <- data.frame(result)
result <- result[-length(result[,1]),]
dataWrite <- result
write.table(result,na = "",pathwritetxt,row.names = FALSE,sep ="\t")
write.csv(result,pathwritecsv, na= "", row.names = FALSE)

