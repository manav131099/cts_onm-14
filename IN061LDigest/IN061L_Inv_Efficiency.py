import os
import glob
import pandas as pd
import numpy as np
from numpy import *
from datetime import timedelta 
import matplotlib.pyplot as plt
from matplotlib.offsetbox import AnchoredText
import statistics


from datetime import date
today = str(date.today())
print(today)
year = today[0:4]
content = []
count_pr = 0
count_energy = 0
month = today[0:7]
inv_eff_list = []

system_size = [74100, 64000, 74100, 62400, 62400]
 
def Inv_eff(path,i):
  df11 = pd.read_csv(path,sep='\t')
  df11.dropna(how='any') 
  df11=(df11[['ts','W_avg','DCW_avg']].dropna())
  df11=df11.loc[((df11['W_avg']>0) & (df11['DCW_avg']>df11['W_avg'])) ,] #Filters
  df11['Loading'] = df11['W_avg']/system_size[i]
  df11['Eff'] = df11['W_avg']/df11['DCW_avg']
  df11['sumpro1'] = df11['Loading']*df11['Eff']
  sp = df11['sumpro1'].sum()
  sl = df11['Loading'].sum()
  inv_eff11 = round((sp/sl)*100,2)
  inv_eff_list.append(inv_eff11)
  
Inv_eff('/home/admin/Dropbox/Gen 1 Data/[IN-061L]/'+year+'/'+month+'/INVERTER_1/[IN-061L]-I1-'+today+'.txt' ,0)
Inv_eff('/home/admin/Dropbox/Gen 1 Data/[IN-061L]/'+year+'/'+month+'/INVERTER_2/[IN-061L]-I2-'+today+'.txt' ,1)
Inv_eff('/home/admin/Dropbox/Gen 1 Data/[IN-061L]/'+year+'/'+month+'/INVERTER_3/[IN-061L]-I3-'+today+'.txt' ,2)
Inv_eff('/home/admin/Dropbox/Gen 1 Data/[IN-061L]/'+year+'/'+month+'/INVERTER_4/[IN-061L]-I4-'+today+'.txt' ,3)
Inv_eff('/home/admin/Dropbox/Gen 1 Data/[IN-061L]/'+year+'/'+month+'/INVERTER_5/[IN-061L]-I5-'+today+'.txt' ,4)

x_axis = [1, 2, 3, 4, 5]
LABELS = ["INV-1", "INV-2", "INV-3","INV-4","INV-5"]
plot = plt.bar(x_axis,inv_eff_list,width=0.4,align='center',color='lightsteelblue',  edgecolor='black')
for rect in plot:
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width()/2.0, height, '%.2f' % float(height), ha='center', va='bottom')
plt.xticks(x_axis,LABELS)       
plt.title("Inverter Efficiency - "+str(today))
plt.ylabel("Inverter Efficiency [%]")

plt.rcParams.update({'font.size': 12})
axes = plt.gca()
mean = statistics.mean(inv_eff_list) 
mean = round(mean,2)
plt.axhline(y=mean, xmin=-10, xmax=100, linewidth=2, linestyle='--',color='black')
axes.set_ylim([0,105])

plt.text(4.8, 89.82, 'Mean ='+str(mean), va="bottom")
plt.savefig('/home/admin/Jason/cec intern/results/IN-032L/[IN-061L] Graph '+str(today)+' Inverter_Efficiency.pdf')
print(inv_eff_list)


