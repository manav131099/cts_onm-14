import requests, json
import requests.auth
import pandas as pd
import datetime as dt
import os
import re 
import time
import shutil
import pytz
import sys
import matplotlib
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.backends.backend_pdf
import matplotlib.dates as mdates
import logging
import pyodbc

stn=sys.argv[1]
end_date=sys.argv[2]
path='/home/admin/Dropbox/Gen 1 Data/['+stn+'L]/'
path_write='/home/admin/Graphs/'
types={'Grid Availability':['GA','red'],'Plant Availability':['PA','green'],'Data Availability':['DA','blue']}
df_daily=pd.DataFrame()

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
SQL_Query = pd.read_sql_query('''SELECT TOP (1000) [Station_Name],[Station_Columns],[Station_Irradiation_Center],[Station_No_Meters],[Provider],[Alarm_Status] FROM [dbo].[stations] ''', connStr)
dflifetime = pd.DataFrame(SQL_Query, columns=['Station_Name','Station_Columns','Station_Irradiation_Center','Station_No_Meters','Provider','Alarm_Status'])
connStr.close()

dflifetime=dflifetime.loc[(dflifetime['Station_Name'].str.strip()==stn ),:]

n=dflifetime['Station_No_Meters'].values[0]
cols=dflifetime['Station_Columns'].values[0]

temp=[]
for index,j in enumerate(cols.strip().split(',')):
    if(index==1):
        temp2=(j.replace('"','').split('.')[:-1])
        temp.append(' '.join(temp2))
    if(index>1 and index<(2+n)): #Skip date and include meters only
        temp2=(j.replace('"','').split('.')[:-1])
        temp.append(' '.join(temp2))

wms_name=temp[0]
mfm_name=temp[1]

#Daily GA,PA,DA Calculation
for i in sorted(os.listdir(path)):
  for k in sorted(os.listdir(path+i)):
    for l in sorted(os.listdir(path+i+'/'+k)):
        if(l==mfm_name):
          for m in sorted(os.listdir(path+i+'/'+k+'/'+l)):
            try:
              a=m.split('-')
              mfm_df=pd.read_csv(path+i+'/'+k+'/'+l+'/'+m,sep='\t')
              m=m[0:10]+wms_name.split('_')[0]+wms_name.split('_')[1]+m[14:]
              if(os.path.exists(path+i+'/'+k+'/'+wms_name+'/'+m)):
                  wms_df=pd.read_csv(path+i+'/'+k+'/'+wms_name+'/'+m,sep='\t')
                  irr_df=wms_df[['ts','POAI_avg']]
                  irr_df=irr_df[irr_df['POAI_avg'].notnull()]
                  if(len(irr_df)>0):
                      mfm_df=mfm_df[['ts','Hz_avg','W_avg']]
                      irr_timestamps=irr_df.loc[irr_df['POAI_avg']>20,'ts']
                      freq_timestamps=mfm_df.loc[mfm_df['Hz_avg']>40,'ts']
                      power_timestamps=mfm_df.loc[mfm_df['W_avg']>2,'ts']
                      ga_common=list(set(freq_timestamps) & set(irr_timestamps))
                      pa_common=list(set(freq_timestamps) & set(irr_timestamps)  & set(power_timestamps))
                      if((len(irr_timestamps)>0) and (len(freq_timestamps)>0)):
                          GA=(float(len(ga_common))/float(len(irr_timestamps)))*100
                          PA=(float(len(pa_common))/float(len(ga_common)))*100
                          DA=(float(len(mfm_df[mfm_df['W_avg'].notnull()]))/288)*100
                          if(DA>100):
                            DA=100
                          df_ga=pd.DataFrame({"Date":[m[15:-4]],'GA':[GA],'PA':[PA],'DA':[DA]},columns=['Date', 'GA', 'PA', 'DA'])
                          df_daily=df_daily.append(df_ga)
                  else:
                      mfm_df=mfm_df[['ts','Hz_avg','W_avg']]
                      mfm_df['ts']=pd.to_datetime(mfm_df['ts'])
                      mask = (mfm_df['ts'].dt.hour > 7) & (mfm_df['ts'].dt.hour < 18)
                      mfm_df=mfm_df[mask]
                      freq_timestamps=mfm_df.loc[mfm_df['Hz_avg']>40,'ts']
                      power_timestamps=mfm_df.loc[mfm_df['W_avg']>2,'ts']
                      pa_common=list(set(freq_timestamps) & set(power_timestamps))
                      if(len(freq_timestamps)>0):
                        GA=(float(len(freq_timestamps))/float(len(mfm_df['ts'])))*100
                        PA=(float(len(pa_common))/float(len(freq_timestamps)))*100
                        DA=(float(len(mfm_df[mfm_df['W_avg'].notnull()]))/120)*100
                        if(DA>100):
                          DA=100
                        df_ga=pd.DataFrame({"Date":[m[15:-4]],'GA':[GA],'PA':[PA],'DA':[DA]},columns=['Date', 'GA', 'PA', 'DA'])
                        df_daily=df_daily.append(df_ga)      
              else:
                mfm_df=mfm_df[['ts','Hz_avg','W_avg']]
                mfm_df['ts']=pd.to_datetime(mfm_df['ts'])
                mask = (mfm_df['ts'].dt.hour > 7) & (mfm_df['ts'].dt.hour < 18)
                mfm_df=mfm_df[mask]
                freq_timestamps=mfm_df.loc[mfm_df['Hz_avg']>40,'ts']
                power_timestamps=mfm_df.loc[mfm_df['W_avg']>2,'ts']
                pa_common=list(set(freq_timestamps) & set(power_timestamps))
                if(len(freq_timestamps)>0):
                  GA=(float(len(freq_timestamps))/float(len(mfm_df['ts'])))*100
                  PA=(float(len(pa_common))/float(len(freq_timestamps)))*100
                  DA=(float(len(mfm_df[mfm_df['W_avg'].notnull()]))/120)*100
                  if(DA>100):
                    DA=100
                  df_ga=pd.DataFrame({"Date":[m[15:-4]],'GA':[GA],'PA':[PA],'DA':[DA]},columns=['Date', 'GA', 'PA', 'DA'])
                  df_daily=df_daily.append(df_ga)         
            except:
              pass
df_daily=df_daily[df_daily['Date']<=end_date]
#Monthly DA
df_monthly=df_daily
df_monthly.index=pd.to_datetime(df_monthly['Date'])
df_monthly=df_monthly.resample("1m").mean()
df_monthly['Date'] = df_monthly.index.to_series().apply(lambda x: dt.datetime.strftime(x, '%b-%Y'))
df_monthly=df_monthly.round(2)
cols = list(df_monthly.columns)
cols = [cols[-1]] + cols[:-1]
df_monthly = df_monthly[cols]

#Graph Creation
font = {'size'   : 11}
plt.rcParams['axes.facecolor'] = 'white'
matplotlib.rc('font', **font)
df_daily['Date']=pd.to_datetime(df_daily['Date'])
for i in types: 
    fig, ax = plt.subplots(figsize=(20, 10))
    plt.plot(df_daily['Date'],df_daily[types[i][0]],linewidth=1.5,color=types[i][1])
    ax.set_xlabel('')
    ax.set_ylabel(i+' [%]')
    ax.set_ylim([0,105])
    ax.set_xlim([df_daily['Date'].head(1).values[0],df_daily['Date'].tail(1).values[0]])
    ttl = ax.set_title('From '+str(df_daily['Date'].head(1).values[0])[0:10]+' to '+str(df_daily['Date'].tail(1).values[0])[0:10], fontdict={'fontsize': 15, 'fontweight': 'medium'})
    ttl.set_position([.5, 1.02])
    ttl_main=fig.suptitle(stn+' '+i,fontsize=18,x=.512)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b-%y')) 
    ax.xaxis.set_major_locator(mdates.MonthLocator(bymonth=[3,6,9,12]))
    fig.savefig(path_write+"Graph_Output/"+stn+"/["+stn+"] Graph "+str(df_daily['Date'].tail(1).values[0])[0:10]+" - "+i+".pdf", bbox_inches='tight') #Graph

df_daily.to_csv(path_write+"Graph_Extract/"+stn+'/['+stn+'] - Master Extract Daily.txt',sep='\t',mode='w',header=True,index=False) #Daily Extract
df_monthly.to_csv(path_write+"Graph_Extract/"+stn+'/['+stn+'] - Master Extract Monthly.txt',sep='\t',index=False) #Monthly Extract