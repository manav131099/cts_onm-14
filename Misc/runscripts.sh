#!/bin/bash
RUNALL=0
x=""
if [ $# == 0 ] 
then
	RUNALL=1
	echo "Running all scripts"
fi

RUNPYTHONONLY=0
RUNRONLY=0

if [ $# == 1 ]
	then
	if [ "$1" == "python-only" ]
	then
		RUNPYTHONONLY=1
		RUNALL=1
		echo "Running only python scripts"
	elif [ "$1" == "R-only" ]
	then
		RUNALL=1
		RUNRONLY=1
		echo "Running only R scripts"
	fi
fi

echo "Value of RUNALL $RUNALL"
echo "Value of RUNPYTHONONLY $RUNPYTHONONLY"
echo "Value of RUNRONLY $RUNRONLY"

if [ $RUNPYTHONONLY -eq 0 ] 
then
if [ "$1" == "GIS_Live" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/GIS/FTPCapture.R &
	x=`echo $!`
	x="$x --GIS_Live"
	if [ "$1" == "GIS_Live" ] 
	then
		x="\n$x"
	fi
	sleep 10
fi

if [ "$1" == "IN-711S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/711Digest/MailDigest711.R &
	y=`echo $!`
	x="$x\n$y --IN-711S_Mail"
	sleep 10
fi

if [ "$1" == "KH-714S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/714Digest/MailDigest714.R &
	y=`echo $!`
	x="$x\n$y --KH-714S_Mail"
	sleep 10
fi

if [ "$1" == "KH-003S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH003NIDigest/MailDigestKH003.R &
	y=`echo $!`
	x="$x\n$y --KH-003S_Mail"
	sleep 10
fi

if [ "$1" == "KH-009L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH009LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --KH-009L_Mail"
	sleep 10
fi

if [ "$1" == "IN-015S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN015Digest/MailDigestIN015.R &
	y=`echo $!`
	x="$x\n$y --IN-015S_Mail"
	sleep 10
fi

if [ "$1" == "IN-713S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/713Digest/MailDigest713.R &
	y=`echo $!`
	x="$x\n$y --IN-713S_Mail"
	sleep 10
fi

if [ "$1" == "SG-003S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/716Digest/MailDigest716.R &
	y=`echo $!`
	x="$x\n$y --SG-003S_Mail"
	sleep 10
fi

if [ "$1" == "SG-007L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SG007LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --SG-007L_Mail"
	sleep 10
fi

if [ "$1" == "SG-008L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SG008LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --SG-008L_Mail"
	sleep 10
fi


if [ "$1" == "PH-719S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/719Digest/MailDigest719.R &
	y=`echo $!`
	x="$x\n$y --PH-719S_Mail"
	sleep 10
fi

if [ "$1" == "SG-004S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG004SDigest/MailDigest717.R &
	y=`echo $!`
	x="$x\n$y --SG-004S_Mail"
	sleep 10
fi

if [ "$1" == "PH-006S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/PH006SDigest/MailDigest719.R &
	y=`echo $!`
	x="$x\n$y --PH-006S_Mail"
	sleep 10
fi

if [ "$1" == "IN-036S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN036Digest/MailDigestIN036.R &
	y=`echo $!`
	x="$x\n$y --IN-036S_Mail"
	sleep 10
fi

if [ "$1" == "IN-003W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN003WDigest/Live.py  > /home/admin/Logs/LogsIN003WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-003W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-005W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN005WDigest/Live.py  > /home/admin/Logs/LogsIN005WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-005W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-012L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3864177" "[IN-012L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN012LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-012L_History"
	sleep 30
fi

if [ "$1" == "IN-013W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN013WDigest/Live.py  > /home/admin/Logs/LogsIN013WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-013W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-014L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3850755" "[IN-014L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-10"> /home/admin/Logs/LogsIN014LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-014L_History"
	sleep 30
fi

if [ "$1" == "IN-017A_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN017ADigest/Live.py  > /home/admin/Logs/LogsIN017AHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-017A_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-018L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3818604" "[IN-018L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN018LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-018L_History"
	sleep 30
fi

if [ "$1" == "IN-019L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3878389" "[IN-019L]" "Asia/Kolkata" "Asia/Calcutta" "2020-09-26"> /home/admin/Logs/LogsIN019LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-019L_History"
	sleep 30
fi

if [ "$1" == "IN-021L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835525" "[IN-021L]" "Asia/Kolkata" "Asia/Calcutta" "2019-11-01"> /home/admin/Logs/LogsIN021LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-021L_History"
	sleep 30
fi

if [ "$1" == "IN-022L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3858321" "[IN-022L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-10"> /home/admin/Logs/LogsIN022LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-022L_History"
	sleep 30
fi

if [ "$1" == "IN-023L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3863937" "[IN-023L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-15"> /home/admin/Logs/LogsIN023LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-023L_History"
	sleep 30
fi

if [ "$1" == "IN-025L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3864693" "[IN-025L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-25"> /home/admin/Logs/LogsIN025LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-025L_History"
	sleep 30
fi


if [ "$1" == "IN-027L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3866025" "[IN-027L]" "Asia/Kolkata" "Asia/Calcutta" "2020-08-22"> /home/admin/Logs/LogsIN027LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-027L_History"
	sleep 30
fi


if [ "$1" == "IN-028L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3883745" "[IN-028L]" "Asia/Kolkata" "Asia/Calcutta" "2020-09-25"> /home/admin/Logs/LogsIN028LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-028L_History"
	sleep 30
fi

if [ "$1" == "IN-030L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3878378" "[IN-030L]" "Asia/Kolkata" "Asia/Calcutta" "2020-08-22"> /home/admin/Logs/LogsIN030LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-030L_History"
	sleep 30
fi


if [ "$1" == "IN-031L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3802193" "[IN-031L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN031LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-031L_History"
	sleep 30
fi

if [ "$1" == "IN-032W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN032WDigest/Live.py  > /home/admin/Logs/LogsIN032WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-032W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-033L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3869286" "[IN-033L]" "Asia/Kolkata" "Asia/Calcutta" "2020-09-08"> /home/admin/Logs/LogsIN033LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-033L_History"
	sleep 30
fi


if [ "$1" == "IN-038L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3765443" "[IN-038L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN038LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-038L_History"
	sleep 30
fi

if [ "$1" == "IN-037L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3860495" "[IN-037L]" "Asia/Kolkata" "Asia/Calcutta" "2020-05-28"> /home/admin/Logs/LogsIN037LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-037L_History"
	sleep 30
fi

if [ "$1" == "IN-040R_History" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/IN040RDigest/Live.py &
	y=`echo $!`
	x="$x\n$y --IN-040R_History"
	sleep 30
fi

if [ "$1" == "IN-039L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3860862" "[IN-039L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-01"> /home/admin/Logs/LogsIN039LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-039L_History"
	sleep 30
fi


if [ "$1" == "IN-041L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835524" "[IN-041L]" "Asia/Kolkata" "Asia/Calcutta" "2019-11-01"> /home/admin/Logs/LogsIN041LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-041L_History"
	sleep 30
fi

if [ "$1" == "IN-042L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3777348" "[IN-042L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN042LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-042L_History"
	sleep 30
fi

if [ "$1" == "IN-043L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3777349" "[IN-043L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN043LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-043L_History"
	sleep 30
fi

if [ "$1" == "IN-045L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3794812" "[IN-045L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN045LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-045L_History"
	sleep 30
fi

if [ "$1" == "IN-047L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3794653" "[IN-047L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN047LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-047L_History"
	sleep 30
fi

if [ "$1" == "IN-048L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN048LDigest/Live.py  > /home/admin/Logs/LogsIN048LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-048L_History"
	sleep 30
fi

if [ "$1" == "IN-049L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3794253" "[IN-049L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN049LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-049L_History"
	sleep 30
fi

if [ "$1" == "IN-050L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1IN050.py "3864840" "[IN-050L]" "Asia/Kolkata" "Asia/Calcutta" "2020-08-04"> /home/admin/Logs/LogsIN050LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-050L_History"
	sleep 30
fi

if [ "$1" == "IN-051L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3787245" "[IN-051L]" "Asia/Kolkata" "Asia/Calcutta" "2019-07-01"> /home/admin/Logs/LogsIN051LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-051L_History"
	sleep 30
fi

if [ "$1" == "IN-052L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN052LDigest/Live.py > /home/admin/Logs/LogsIN052LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-052L_History"
	sleep 30
fi

if [ "$1" == "IN-053L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN053LDigest/Live.py > /home/admin/Logs/LogsIN053LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-053L_History"
	sleep 30
fi

if [ "$1" == "IN-054L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3864876" "[IN-054L]" "Asia/Kolkata" "Asia/Calcutta" "2020-08-04"> /home/admin/Logs/LogsIN054LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-054L_History"
	sleep 30
fi

if [ "$1" == "IN-055L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3795317" "[IN-055L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN055LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-055L_History"
	sleep 30
fi


if [ "$1" == "IN-056L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN056LDigest/Live.py > /home/admin/Logs/LogsIN056LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-056L_History"
	sleep 30
fi


if [ "$1" == "IN-057L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3869097" "[IN-057L]" "Asia/Kolkata" "Asia/Calcutta" "2020-09-08"> /home/admin/Logs/LogsIN057LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-057L_History"
	sleep 30
fi


if [ "$1" == "IN-058L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN058LDigest/Live.py > /home/admin/Logs/LogsIN058LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-058L_History"
	sleep 30
fi


if [ "$1" == "IN-058L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN058LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-058L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-059L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3800132" "[IN-059L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN059LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-059L_History"
	sleep 30
fi

if [ "$1" == "IN-060L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN060LDigest/Live.py > /home/admin/Logs/LogsIN060LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-060L_History"
	sleep 30
fi

if [ "$1" == "IN-061L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3800671" "[IN-061L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN061LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-061L_History"
	sleep 30
fi

if [ "$1" == "IN-062L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN062LDigest/Live.py > /home/admin/Logs/LogsIN062LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-062L_History"
	sleep 30
fi

if [ "$1" == "IN-063L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3802488" "[IN-063L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN063LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-063L_History"
	sleep 30
fi

if [ "$1" == "IN-064L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN064LDigest/Live.py > /home/admin/Logs/LogsIN064LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-064L_History"
	sleep 30
fi

if [ "$1" == "IN-065L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3814767" "[IN-065L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN065LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-065L_History"
	sleep 30
fi

if [ "$1" == "IN-066L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN066LDigest/Live.py > /home/admin/Logs/LogsIN066LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-066L_History"
	sleep 30
fi

if [ "$1" == "IN-067L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3804923" "[IN-067L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN067LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-067L_History"
	sleep 30
fi

if [ "$1" == "IN-068L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN068LDigest/Live.py > /home/admin/Logs/LogsIN068LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-068L_History"
	sleep 30
fi

if [ "$1" == "IN-069L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN069LDigest/Live.py > /home/admin/Logs/LogsIN069LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-069L_History"
	sleep 30
fi

if [ "$1" == "IN-070L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3864775" "[IN-070L]" "Asia/Kolkata" "Asia/Calcutta" "2020-08-01"> /home/admin/Logs/LogsIN070LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-070L_History"
	sleep 10
fi

if [ "$1" == "IN-071L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3804130" "[IN-071L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN071LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-071L_History"
	sleep 30
fi

if [ "$1" == "IN-072L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3805118" "[IN-072L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN072LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-072L_History"
	sleep 30
fi

if [ "$1" == "IN-073L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3805120" "[IN-073L]" "Asia/Kolkata" "Asia/Calcutta" "2019-09-01" > /home/admin/Logs/LogsIN073LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-073L_History"
	sleep 30
fi

if [ "$1" == "IN-074L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3820177" "[IN-074L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN074LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-074L_History"
	sleep 30
fi

if [ "$1" == "IN-075L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN075LDigest/Live.py > /home/admin/Logs/LogsIN075LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-075L_History"
	sleep 30
fi
 
if [ "$1" == "IN-076L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3822707" "[IN-076L]" "Asia/Kolkata" "Asia/Calcutta" "2020-02-10"> /home/admin/Logs/LogsIN076LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-076L_History"
	sleep 30
fi

if [ "$1" == "IN-077L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3823815" "[IN-077L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN077LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-077L_History"
	sleep 30
fi

if [ "$1" == "IN-078L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3821964" "[IN-078L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN078LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-078L_History"
	sleep 30
fi

if [ "$1" == "IN-079L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3829586" "[IN-079L]" "Asia/Kolkata" "Asia/Calcutta" "2019-09-01"> /home/admin/Logs/LogsIN079LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-079L_History"
	sleep 30
fi

if [ "$1" == "IN-080L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN080LDigest/Live.py  > /home/admin/Logs/LogsIN080LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-080L_History"
	sleep 30
fi

if [ "$1" == "IN-081L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3836445" "[IN-081L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN081LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-081L_History"
	sleep 30
fi

if [ "$1" == "IN-082L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3835833" "[IN-082L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN082LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-082L_History"
	sleep 30
fi

if [ "$1" == "IN-083L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3839350" "[IN-083L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN083LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-083L_History"
	sleep 30
fi

if [ "$1" == "IN-085L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3849158" "[IN-085L]" "Asia/Kolkata" "Asia/Calcutta" "2020-02-10"> /home/admin/Logs/LogsIN085LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-085L_History"
	sleep 30
fi

if [ "$1" == "IN-086L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3850328" "[IN-086L]" "Asia/Kolkata" "Asia/Calcutta" "2020-02-10"> /home/admin/Logs/LogsIN086LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-086L_History"
	sleep 30
fi

if [ "$1" == "IN-087L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3850906" "[IN-087L]" "Asia/Kolkata" "Asia/Calcutta" "2020-03-01"> /home/admin/Logs/LogsIN087LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-087L_History"
	sleep 30
fi

if [ "$1" == "IN-088L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3862554" "[IN-088L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-10"> /home/admin/Logs/LogsIN088LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-088L_History"
	sleep 30
fi

if [ "$1" == "IN-089L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3851158" "[IN-089L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-01"> /home/admin/Logs/LogsIN089LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-089L_History"
	sleep 30
fi

if [ "$1" == "IN-090L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3864358" "[IN-090L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-21"> /home/admin/Logs/LogsIN090LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-090L_History"
	sleep 30
fi

if [ "$1" == "IN-301L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live1min.py "3862993" "[IN-301L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-01"> /home/admin/Logs/LogsIN301LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-301L_History"
	sleep 30
fi

if [ "$1" == "IN-302L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV6.py "3866833" "[IN-302L]" "Asia/Kolkata" "Asia/Calcutta" "2020-09-01"> /home/admin/Logs/LogsIN302LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-302L_History"
	sleep 30
fi

if [ "$1" == "KH-009L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3848700" "[KH-009L]" "Asia/Phnom_Penh" "Asia/Phnom_Penh" "2020-02-01"> /home/admin/Logs/LogsKH009LHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-009L_History"
	sleep 30
fi

if [ "$1" == "MY-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3862535" "[MY-002L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-15"> /home/admin/Logs/LogsMY002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-002L_History"
	sleep 30
fi

if [ "$1" == "MY-005L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3819590" "[MY-005L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2019-06-01"> /home/admin/Logs/LogsMY005LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-005L_History"
	sleep 30
fi	

if [ "$1" == "MY-007L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3827749" "[MY-007L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2019-12-01"> /home/admin/Logs/LogsMY007LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-007L_History"
	sleep 30
fi

if [ "$1" == "MY-008L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3824826" "[MY-008L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2019-12-01"> /home/admin/Logs/LogsMY008LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-008L_History"
	sleep 30
fi

if [ "$1" == "MY-009L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3836443" "[MY-009L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2019-12-01"> /home/admin/Logs/LogsMY009LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-009L_History"
	sleep 30
fi

if [ "$1" == "MY-010L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3849442" "[MY-010L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-02-01"> /home/admin/Logs/LogsMY010LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-010L_History"
	sleep 30
fi

if [ "$1" == "MY-011L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3871876" "[MY-011L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-20"> /home/admin/Logs/LogsMY011LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-011L_History"
	sleep 30
fi

if [ "$1" == "MY-012L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3878380" "[MY-012L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-22"> /home/admin/Logs/LogsMY012LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-012L_History"
	sleep 30
fi


if [ "$1" == "MY-401L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3845432" "[MY-401L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-01-01"> /home/admin/Logs/LogsMY401LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-401L_History"
	sleep 30
fi

if [ "$1" == "MY-402L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3845631" "[MY-402L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-01-01"> /home/admin/Logs/LogsMY402LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-402L_History"
	sleep 30
fi

if [ "$1" == "MY-403L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3845864" "[MY-403L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-01-01"> /home/admin/Logs/LogsMY403LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-403L_History"
	sleep 30
fi

if [ "$1" == "MY-404L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3846888" "[MY-404L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-02-01"> /home/admin/Logs/LogsMY404LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-404L_History"
	sleep 30
fi

if [ "$1" == "MY-405L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3847585" "[MY-405L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-01-01"> /home/admin/Logs/LogsMY405LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-405L_History"
	sleep 30
fi

if [ "$1" == "MY-406L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3848698" "[MY-406L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-02-01"> /home/admin/Logs/LogsMY406LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-406L_History"
	sleep 30
fi

if [ "$1" == "MY-407L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865271" "[MY-407L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY407LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-407L_History"
	sleep 30
fi

if [ "$1" == "MY-408L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865052" "[MY-408L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY408LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-408L_History"
	sleep 30
fi

if [ "$1" == "MY-409L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865316" "[MY-409L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY409LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-409L_History"
	sleep 30
fi

if [ "$1" == "MY-410L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865451" "[MY-410L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY410LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-410L_History"
	sleep 30
fi

if [ "$1" == "MY-411L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865502" "[MY-411L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY411LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-411L_History"
	sleep 30
fi

if [ "$1" == "MY-412L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865515" "[MY-412L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY412LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-412L_History"
	sleep 30
fi


if [ "$1" == "MY-413L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865526" "[MY-413L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY413LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-413L_History"
	sleep 30
fi

if [ "$1" == "MY-414L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865896" "[MY-414L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY414LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-414L_History"
	sleep 30
fi

if [ "$1" == "MY-415L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3866594" "[MY-415L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-25"> /home/admin/Logs/LogsMY415LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-415L_History"
	sleep 30
fi

if [ "$1" == "MY-416L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3866705" "[MY-416L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-25"> /home/admin/Logs/LogsMY416LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-416L_History"
	sleep 30
fi

if [ "$1" == "MY-417L_History" ] || [ $RUNALL == 1 ]  
then 
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3866777" "[MY-417L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-27"> /home/admin/Logs/LogsMY417LHistory.txt & 
	y=`echo $!` 
	x="$x\n$y --MY-417L_History" 
	sleep 30 
fi

if [ "$1" == "MY-418L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3866866" "[MY-418L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-05"> /home/admin/Logs/LogsMY418LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-418L_History"
	sleep 30
fi


if [ "$1" == "MY-419L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869062" "[MY-419L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-05"> /home/admin/Logs/LogsMY419LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-419L_History"
	sleep 30
fi

if [ "$1" == "MY-420L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869136" "[MY-420L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY420LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-420L_History"
	sleep 30
fi

if [ "$1" == "MY-421L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869677" "[MY-421L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY421LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-421L_History"
	sleep 30
fi

if [ "$1" == "MY-422L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869173" "[MY-422L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY422LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-422L_History"
	sleep 30
fi

if [ "$1" == "MY-423L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869288" "[MY-423L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY423LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-423L_History"
	sleep 30
fi

if [ "$1" == "MY-424L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869291" "[MY-424L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY424LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-424L_History"
	sleep 30
fi

if [ "$1" == "MY-425L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869096" "[MY-425L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-20"> /home/admin/Logs/LogsMY425LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-425L_History"
	sleep 30
fi
	
if [ "$1" == "MY-426L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3870203" "[MY-426L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY426LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-426L_History"
	sleep 30
fi

if [ "$1" == "MY-427L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3871874" "[MY-427L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-16"> /home/admin/Logs/LogsMY427LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-427L_History"
	sleep 30
fi

if [ "$1" == "MY-428L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3873918" "[MY-428L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY428LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-428L_History"
	sleep 30
fi

if [ "$1" == "MY-429L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3878377" "[MY-429L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-20"> /home/admin/Logs/LogsMY429LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-429L_History"
	sleep 30
fi

if [ "$1" == "MY-430L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3878379" "[MY-430L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-20"> /home/admin/Logs/LogsMY430LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-430L_History"
	sleep 30
fi

if [ "$1" == "MY-431L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3878388" "[MY-431L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-22"> /home/admin/Logs/LogsMY431LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-431L_History"
	sleep 30
fi

if [ "$1" == "MY-432L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3883755" "[MY-432L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-25"> /home/admin/Logs/LogsMY432LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-432L_History"
	sleep 30
fi

if [ "$1" == "MY-433L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3883756" "[MY-433L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-25"> /home/admin/Logs/LogsMY433LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-433L_History"
	sleep 30
fi

if [ "$1" == "PH-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3830382" "[PH-002L]" "Asia/Manila" "Asia/Manila" "2019-07-01"> /home/admin/Logs/LogsPH002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-002L_History"
	sleep 30
fi

if [ "$1" == "SG-007L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3795604" "[SG-007L]" "Asia/Singapore" "Asia/Singapore" > /home/admin/Logs/LogsSG007LHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-007L_History"
	sleep 30
fi

if [ "$1" == "SG-008L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3836631" "[SG-008L]" "Asia/Singapore" "Asia/Singapore" "2019-12-01" > /home/admin/Logs/LogsSG008LHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-008L_History"
	sleep 30
fi


if [ "$1" == "TH-001L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3865479" "[TH-001L]" "Asia/Bangkok" "Asia/Bangkok" "2020-08-17"> /home/admin/Logs/LogsTH001LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-001L_History"
	sleep 30
fi


if [ "$1" == "TH-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3862423" "[TH-002L]" "Asia/Bangkok" "Asia/Bangkok" "2020-06-15"> /home/admin/Logs/LogsTH002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-002L_History"
	sleep 30
fi

if [ "$1" == "TH-004L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835526" "[TH-004L]" "Asia/Bangkok" "Asia/Bangkok" "2019-11-01" > /home/admin/Logs/LogsTH004LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-004L_History"
	sleep 30
fi

if [ "$1" == "TH-005L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835213" "[TH-005L]" "Asia/Bangkok" "Asia/Bangkok" "2019-11-01"> /home/admin/Logs/LogsTH005LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-005L_History"
	sleep 30
fi

if [ "$1" == "TH-006L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3864365" "[TH-006L]" "Asia/Bangkok" "Asia/Bangkok" "2020-07-25"> /home/admin/Logs/LogsTH006LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-006L_History"
	sleep 30
fi

if [ "$1" == "TH-007L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3846845" "[TH-007L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-01"> /home/admin/Logs/LogsTH007LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-007L_History"
	sleep 30
fi

if [ "$1" == "TH-008L_History" ] || [ $RUNALL == 1 ]  
then 
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3871877" "[TH-008L]" "Asia/Bangkok" "Asia/Bangkok" "2020-09-14"> /home/admin/Logs/LogsTH008LHistory.txt & 
	y=`echo $!` 
	x="$x\n$y --TH-008L_History"
	sleep 30
fi

if [ "$1" == "TH-010L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3839347" "[TH-010L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH010LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-010L_History"
	sleep 30
fi	

if [ "$1" == "TH-011L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3839348" "[TH-011L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH011LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-011L_History"
	sleep 30
fi

if [ "$1" == "TH-012L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840208" "[TH-012L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH012LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-012L_History"
	sleep 30
fi

if [ "$1" == "TH-013L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3839597" "[TH-013L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH013LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-013L_History"
	sleep 30
fi

if [ "$1" == "TH-014L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3844885" "[TH-014L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH014LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-014L_History"
	sleep 30
fi

if [ "$1" == "TH-015L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840207" "[TH-015L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH015LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-015L_History"
	sleep 30
fi	

if [ "$1" == "TH-016L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840369" "[TH-016L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH016LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-016L_History"
	sleep 30
fi

if [ "$1" == "TH-017L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3839502" "[TH-017L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH017LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-017L_History"
	sleep 30
fi	

if [ "$1" == "TH-018L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3841478" "[TH-018L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH018LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-018L_History"
	sleep 30
fi	

if [ "$1" == "TH-019L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3841479" "[TH-019L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH019LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-019L_History"
	sleep 30
fi	

if [ "$1" == "TH-020L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3841476" "[TH-020L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH020LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-020L_History"
	sleep 30
fi	

if [ "$1" == "TH-021L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3839596" "[TH-021L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH021LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-021L_History"
	sleep 30
fi

if [ "$1" == "TH-022L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840166" "[TH-022L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH022LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-022L_History"
	sleep 30
fi		

if [ "$1" == "TH-023L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840169" "[TH-023L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH023LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-023L_History"
	sleep 30
fi	

if [ "$1" == "TH-024L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3845630" "[TH-024L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH024LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-024L_History"
	sleep 30
fi

if [ "$1" == "TH-025L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840406" "[TH-025L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH025LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-025L_History"
	sleep 30
fi

if [ "$1" == "TH-026L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840480" "[TH-026L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH026LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-026L_History"
	sleep 30
fi

if [ "$1" == "TH-027L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3840205" "[TH-027L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH027LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-027L_History"
	sleep 30
fi	

if [ "$1" == "TH-028L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveTescoAzure.py "3844886" "[TH-028L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH028LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-028L_History"
	sleep 30
fi

if [ "$1" == "TH-046L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3857794" "[TH-046L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-25"> /home/admin/Logs/LogsTH046LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-046L_History"
	sleep 30
fi

if [ "$1" == "TH-047L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3850754" "[TH-047L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-25"> /home/admin/Logs/LogsTH047LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-047L_History"
	sleep 30
fi

if [ "$1" == "TH-048L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3850298" "[TH-048L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-25"> /home/admin/Logs/LogsTH048LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-048L_History"
	sleep 30
fi

if [ "$1" == "TH-049L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3849157" "[TH-049L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-25"> /home/admin/Logs/LogsTH049LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-049L_History"
	sleep 30
fi	

if [ "$1" == "VN-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3818352" "[VN-002L]" "Asia/Saigon" "Asia/Saigon" > /home/admin/Logs/LogsVN002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --VN-002L_History"
	sleep 30
fi

if [ "$1" == "VN-003L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3822066" "[VN-003L]" "Asia/Saigon" "Asia/Saigon" > /home/admin/Logs/LogsVN003LHistory.txt &
	y=`echo $!`
	x="$x\n$y --VN-003L_History"
	sleep 30
fi

if [ "$1" == "SG-003S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/716Live/Live.py > /home/admin/Logs/Logs716History.txt &
	y=`echo $!`
	x="$x\n$y --SG-003S_Live"
	sleep 10
fi

if [ "$1" == "SG-003S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/716Live/Live_Azure_4g.py > /home/admin/Logs/Logs716AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-003S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "SG-004S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/717Live/Live.py > /home/admin/Logs/Logs717History.txt &
	y=`echo $!`
	x="$x\n$y --SG-004S_Live"
	sleep 10
fi

if [ "$1" == "SG-004S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/717Live/Live_Azure_4g.py > /home/admin/Logs/Logs717AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-004S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "SG-005S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/725Live/Live.py > /home/admin/Logs/Logs725History.txt &
	y=`echo $!`
	x="$x\n$y --SG-005S_Live"
	sleep 10
fi

if [ "$1" == "SG-005S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/725Live/Live_Azure_4g.py > /home/admin/Logs/Logs725AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-005S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "KH-001S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/714Live/Live.py > /home/admin/Logs/Logs714History.txt &
	y=`echo $!`
	x="$x\n$y --KH-001S_Live"
	sleep 10
fi

if [ "$1" == "KH-008S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/728Live/Live.py > /home/admin/Logs/Logs728History.txt &
	y=`echo $!`
	x="$x\n$y --KH-008S_Live"
	sleep 10
fi

if [ "$1" == "KH-008S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/728Live/Live_Azure_4g.py > /home/admin/Logs/Logs728AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-008S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "KH-003S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/715Live/Live.py > /home/admin/Logs/Logs715History.txt &
	y=`echo $!`
	x="$x\n$y --KH-003S_Live"
	sleep 10
fi

if [ "$1" == "KH-003S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/715Live/Live_Azure_4g.py > /home/admin/Logs/Logs715AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-003S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "MY-004S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/720Live/Live.py > /home/admin/Logs/Logs720History.txt &
	y=`echo $!`
	x="$x\n$y --MY-004S_Live"
	sleep 10
fi
if [ "$1" == "MY-006S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/729Live/Live.py > /home/admin/Logs/Logs729History.txt &
	y=`echo $!`
	x="$x\n$y --MY-006S_Live"
	sleep 10
fi

if [ "$1" == "MY-006S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/729Live/Live_Azure_4g.py > /home/admin/Logs/Logs729AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-006S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "IN-015S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/712Live/Live.py > /home/admin/Logs/Logs712History.txt &
	y=`echo $!`
	x="$x\n$y --IN-015S_Live"
	sleep 10
fi

if [ "$1" == "IN-015S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/712Live/Live_Azure_4g.py > /home/admin/Logs/Logs712AzureHistory.txt &	
	y=`echo $!`
	x="$x\n$y --IN-015S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "IN-036S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/718Live/Live.py > /home/admin/Logs/Logs718History.txt &
	y=`echo $!`
	x="$x\n$y --IN-036S_Live"
	sleep 10
fi

if [ "$1" == "IN-036S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/718Live/Live_Azure_4g.py > /home/admin/Logs/Logs718AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-036S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "VN-001S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/722Live/Live.py > /home/admin/Logs/Logs722History.txt &
	y=`echo $!`
	x="$x\n$y --VN-001S_Live"
	sleep 10
fi


if [ "$1" == "SerisLifetimeMaster" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/SerisLifetimeMaster.py > /home/admin/Logs/LogsSerisLifetimeMaster.txt &
	y=`echo $!`
	x="$x\n$y --SerisLifetimeMaster"
	sleep 20
fi

if [ "$1" == "LocusLifetimeMaster" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/LocusLifetimeMaster.py > /home/admin/Logs/LogsLocusLifetimeMaster.txt &
	y=`echo $!`
	x="$x\n$y --LocusLifetimeMaster"
	sleep 20
fi

if [ "$1" == "FlexiLifetimeMaster" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/FlexiLifetimeMaster.py > /home/admin/Logs/LogsFlexiLifetimeMaster.txt &
	y=`echo $!`
	x="$x\n$y --FlexiLifetimeMaster"
	sleep 20
fi

if [ "$1" == "Lifetime_Gen2" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/Lifetime_Gen2.py > /home/admin/Logs/LogsLifetimeGen_2.txt &
	y=`echo $!`
	x="$x\n$y --Lifetime_Gen2"
	sleep 10
fi


if [ "$1" == "Web_Interface" ] || [ $RUNALL == 1 ] 
then
	nohup python3  /home/admin/CODE/WebInterface/Interface_V2.py > /home/admin/Logs/LogsWebInterface.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --Web_Interface"
	sleep 10
fi

#Added as a Cronjob
#if [ "$1" == "Invoice_View" ] || [ $RUNALL == 1 ] 
#then
#	stdbuf -oL python /home/admin/CODE/Invoicing/View.py > /home/admin/Logs/LogsInvoiceHistory.txt &
#	y=`echo $!`
#	x="$x\n$y --Invoice_View"
#	sleep 10
#fi

if [ "$1" == "KH008_Curtailment_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Alarms/KH008_Curtailment_Alarm.py > /home/admin/Logs/LogsKH008CurtailmentAlarm.txt &
	y=`echo $!`
	x="$x\n$y --KH008_Curtailment_Alarm"
	sleep 10
fi

if [ "$1" == "Tesco_Server_Push" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/Tesco/tesco_server_push.py > /home/admin/Logs/LogsTescoServerPushHistory.txt &
	y=`echo $!`
	x="$x\n$y --Tesco_Server_Push"
	sleep 10
fi

if [ "$1" == "IN-721S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/721Digest/MailDigest721.R &
	y=`echo $!`
	x="$x\n$y --IN-721S_Mail"
	sleep 10
fi

if [ "$1" == "SG-724S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG724Digest/MailDigest724.R &
	y=`echo $!`
	x="$x\n$y --SG-724S_Mail"
	sleep 20 
fi

if [ "$1" == "IN-003W_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN003WDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-003W_Mail"
	sleep 10 
fi

if [ "$1" == "IN-005W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN005WDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsIN005WMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-005W_Mail"
    sleep 10
fi

if [ "$1" == "IN-013W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN013WDigest/Digest.py "Mail" "2020-09-30"> /home/admin/Logs/LogsIN013WMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-013W_Mail"
    sleep 10
fi

if [ "$1" == "IN-014L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Digest_Generic.py "Mail" "2020-06-10"> /home/admin/Logs/LogsIN014LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-014L_Mail"
	sleep 10 
fi

if [ "$1" == "IN-017A_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN017ADigest/Digest.py "Mail" "2020-09-10"> /home/admin/Logs/LogsIN017AMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-017A_Mail"
    sleep 10
fi

if [ "$1" == "IN-018L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN018LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-018L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-019L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN019LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-019L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-021L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN021LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-021L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-022L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN022LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-022L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-023L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN023LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-023L_Mail"
	sleep 10
fi

if [ "$1" == "IN-025L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN025LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-025L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-027L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN027LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-027L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-028L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN028LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-028L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-030L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN030LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-030L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-031L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN031LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-031L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-032W_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN032WDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsIN032WMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-032W_Mail"
	sleep 10 
fi


if [ "$1" == "IN-033L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN033LDigest/Digest.py "Mail" "2020-09-10"> /home/admin/Logs/LogsIN033LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-033L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-037L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN037LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-037L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-038L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN038LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-038L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-039L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN039LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-039L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-041L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN041LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-041L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-042L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN042LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-042L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-043L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN043LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-043L_Mail"
	sleep 20 
fi



if [ "$1" == "IN-045L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN045LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-045L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-047L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN047LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-047L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-048L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN048LDigest/Digest.py "Mail" "2020-09-10"> /home/admin/Logs/LogsIN048LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-048L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-049L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN049LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-049L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-051L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN051LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-051L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-052L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN052LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-052L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-053L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN053LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-053L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-054L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN054LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-054L_Mail"
	sleep 10
fi

if [ "$1" == "IN-055L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN055LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-055L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-056L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN056LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-056L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-057L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN057LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-057L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-059L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN059LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-059L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-060L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN060LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-060L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-061L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN061LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-061L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-062L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN062LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-062L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-063L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN063LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-063L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-064L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN064LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-064L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-065L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN065LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-065L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-066L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN066LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-066L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-067L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN067LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-067L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-068L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN068LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-068L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-068L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN068LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-068L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-069L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN069LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-069L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-069L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN069LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-069L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-071L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN071LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-071L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-072L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN072LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-072L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-073L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN073LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-073L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-074L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN074LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-074L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-075L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN075LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-075L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-080L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN080LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-080L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-076L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN076LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-076L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-077L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN077LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-077L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-078L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN078LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-078L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-079L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN079LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-079L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-081L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN081LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-081L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-082L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN082LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-082L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-083L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN083LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-083L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-085L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN085LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-085L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-086L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN086LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-086L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-086L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN086LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-086L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-087L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN087LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-087L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-088L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN088LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-088_Mail"
	sleep 20 
fi

if [ "$1" == "IN-088L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN088LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-088L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-089L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN089LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-087L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-089L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN089LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-089L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-090L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN090LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-087L_Mail"
	sleep 20 
fi

if [ "$1" == "MY-008L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY008LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-008L_Mail"
fi

if [ "$1" == "MY-009L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY009LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-009L_Mail"
fi

if [ "$1" == "SG-725S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG725Digest/MailDigest725.R &
	y=`echo $!`
	x="$x\n$y --SG-725S_Mail"
	sleep 20 
fi

if [ "$1" == "VN-002L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN002LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --VN-002L_Mail"
	sleep 20 
fi

if [ "$1" == "VN-003L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN003LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --VN-003L_Mail"
	sleep 20 
fi

if [ "$1" == "SG-726S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG726Digest/MailDigest726.R &
	y=`echo $!`
	x="$x\n$y --SG-726S_Mail"
	sleep 20 
fi


if [ "$1" == "SG-727S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG727Digest/MailDigest727.R &
	y=`echo $!`
	x="$x\n$y --SG-727S_Mail"
	sleep 20 
fi

if [ "$1" == "SG-005S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG005SDigest/MailDigest724.R &
	y=`echo $!`
	x="$x\n$y --SG-005S_Mail"
	sleep 10
fi

if [ "$1" == "VN-001S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN001SDigest/MailDigest722.R &
	y=`echo $!`
	x="$x\n$y --VN-001S_Mail"
	sleep 10
fi

if [ "$1" == "VN-722S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN722Digest/MailDigest722.R &
	y=`echo $!`
	x="$x\n$y --VN-722S_Mail"
	sleep 10
fi

if [ "$1" == "SG-999S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/Customer/MailDigest724.R &
	y=`echo $!`
	x="$x\n$y --SG-999S_Mail"
	sleep 10
fi

if [ "$1" == "MY-004S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY004SDigest/MailDigest720.R &
	y=`echo $!`
	x="$x\n$y --MY-004S_Mail"
	sleep 10
fi

if [ "$1" == "MY-720S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY720Digest/MailDigest720.R &
	y=`echo $!`
	x="$x\n$y --MY-720S_Mail"
	sleep 10
fi

if [ "$1" == "MY-006S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY006SDigest/MailDigest729.R &
	y=`echo $!`
	x="$x\n$y --MY-006S_Mail"
	sleep 10
fi

if [ "$1" == "KH-008S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH008SDigest/MailDigestKH008.R &
	y=`echo $!`
	x="$x\n$y --KH-008S_Mail"
	sleep 10
fi

if [ "$1" == "IN-008X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN008Digest/LalruCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --IN-008X_History"
	sleep 10
fi

if [ "$1" == "IN-010X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN010Digest/DelhiCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --IN-010X_History"
	sleep 10
fi

if [ "$1" == "SG-001X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG001Digest/SG001CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-001X_History"
	sleep 10
fi

if [ "$1" == "SG-002X_History" ] || [ $RUNALL == 1 ]
then
	Rscript /home/admin/CODE/SGDigest/SG002Digest/SG002CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-002X_History"
	sleep 10
fi

if [ "$1" == "SG-004X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG004XDigest/SG004CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-004X_History"
	sleep 10
fi

if [ "$1" == "SG-006X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG006XDigest/SG006CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-006X_History"
	sleep 10
fi


if [ "$1" == "SG-003EF_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Final/SG003EFCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-003EF_History"
	sleep 10
fi

if [ "$1" == "SG-003EP_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Prelim/SG003EPCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-003EP_History"
	sleep 10
fi

if [ "$1" == "SG-007EP_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Prelim/SG007EPCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-007EP_History"
	sleep 10
fi

if [ "$1" == "SG-007EF_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Final/SG007EFCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-007EF_History"
	sleep 10
fi

if [ "$1" == "SG-008EF_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Final/SG008EFCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-008EF_History"
	sleep 10
fi

if [ "$1" == "SG-008EP_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Prelim/SG008EPCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-008EP_History"
	sleep 10
fi

if [ "$1" == "KH-002X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH002Digest/KH002CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-002X_History"
	sleep 10
fi

if [ "$1" == "KH-003X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH003Digest/KH003CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-003X_History"
	sleep 10
fi

if [ "$1" == "KH-004X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH004Digest/KH004CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-004X_History"
	sleep 10
fi

if [ "$1" == "KH-005X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH005XDigest/KH005CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-005X_History"
	sleep 10
fi

if [ "$1" == "KH-006X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH006XDigest/KH006CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-006X_History"
	sleep 10
fi

if [ "$1" == "KH-007X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH007XDigest/KH007CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-007X_History"
	sleep 10
fi

if [ "$1" == "KH-008X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH008Digest/KH008CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-008X_History"
	sleep 10
fi



if [ "$1" == "TH-003X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH003Digest/TH003CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --TH-003X_History"
	sleep 10
fi

if [ "$1" == "MY-001X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY001Digest/MY001CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --MY-001X_History"
	sleep 10
fi

if [ "$1" == "MY-003X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY003Digest/MY003CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --MY-003X_History"
	sleep 10
fi

if [ "$1" == "VN-001X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN001XDigest/VN001CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --VN-001X_History"
	sleep 10
fi

if [ "$1" == "IN-012L_Mail" ] || [ $RUNALL == 1 ]
then
	Rscript /home/admin/CODE/IN012LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-012L_Mail"
	sleep 10
fi


if [ "$1" == "IN-070L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN070LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-070L_Mail"
	sleep 10
fi

if [ "$1" == "MY-002L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY002LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-002L_Mail"
	sleep 10
fi

if [ "$1" == "TH-002L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH002LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-002L_Mail"
	sleep 10
fi

if [ "$1" == "MY-002L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY002LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-002L_Mail"
	sleep 10
fi

if [ "$1" == "MY-011L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY011LDigest/Digest_temp.py "Mail" "2020-09-25"> /home/admin/Logs/LogsMY011LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-011L_Mail"
    sleep 10
fi

if [ "$1" == "IN-301L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN301LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-301L_Mail"
	sleep 10
fi


if [ "$1" == "IN-302L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN302LDigest/Digest.py "Mail" "2020-09-01"> /home/admin/Logs/LogsIN302LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-302L_Mail"
	sleep 10
fi

if [ "$1" == "IN-008X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN008Digest/MailDigestLalru.R &
	y=`echo $!`
	x="$x\n$y --IN-008X_Mail"
	sleep 10
fi

if [ "$1" == "IN-010X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN010Digest/MailDigestDelhi.R &
	y=`echo $!`
	x="$x\n$y --IN-010X_Mail"
	sleep 10
fi

if [ "$1" == "KH-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH001Digest/MailDigestKH001.R &
	y=`echo $!`
	x="$x\n$y --KH-001X_Mail"
	sleep 10
fi

if [ "$1" == "KH-002X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH002Digest/MailDigestKH002.R &
	y=`echo $!`
	x="$x\n$y --KH-002X_Mail"
	sleep 10
fi

if [ "$1" == "SG-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG001Digest/MailDigestSG001.R &
	y=`echo $!`
	x="$x\n$y --SG-001X_Mail"
	sleep 10
fi

if [ "$1" == "SG-002X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG002Digest/MailDigestSG002.R &
	y=`echo $!`
	x="$x\n$y --SG-002X_Mail"
	sleep 10
fi

if [ "$1" == "SG-003EP_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Prelim/MailDigestSG003EP.R &
	y=`echo $!`
	x="$x\n$y --SG-003EP_Mail"
	sleep 10
fi

if [ "$1" == "SG-003EF_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Final/MailDigestSG003EF.R &
	y=`echo $!`
	x="$x\n$y --SG-003EF_Mail"
	sleep 10
fi

if [ "$1" == "SG-007EP_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Prelim/MailDigestSG007EP.R &
	y=`echo $!`
	x="$x\n$y --SG-007EP_Mail"
	sleep 10
fi

if [ "$1" == "SG-007EF_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Final/MailDigestSG007EF.R &
	y=`echo $!`
	x="$x\n$y --SG-007EF_Mail"
	sleep 10
fi

if [ "$1" == "TH-001L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH001LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-001L_Mail"
	sleep 10
fi

if [ "$1" == "SG-008EP_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Prelim/MailDigestSG008EP.R &
	y=`echo $!`
	x="$x\n$y --SG-008EP_Mail"
	sleep 10
fi

if [ "$1" == "SG-008EF_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Final/MailDigestSG008EF.R &
	y=`echo $!`
	x="$x\n$y --SG-008EF_Mail"
	sleep 10
fi

if [ "$1" == "SG-004X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG004XDigest/MailDigestSG004.R &
	y=`echo $!`
	x="$x\n$y --SG-004X_Mail"
	sleep 10
fi

if [ "$1" == "SG-006X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG006XDigest/MailDigestSG006.R &
	y=`echo $!`
	x="$x\n$y --SG-006X_Mail"
	sleep 10
fi


if [ "$1" == "IN-018X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN018Digest/MailDigestIN018.R &
	y=`echo $!`
	x="$x\n$y --IN-018X_Mail"
	sleep 10
fi

if [ "$1" == "SG-006_Lifetime" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/SG006Lifetime.py > /home/admin/Logs/LogsSG006LifetimeHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-006_Lifetime"
	sleep 10
fi

if [ "$1" == "KH-003X_Mail" ] || [ $RUNALL == 1 ]
then
	Rscript /home/admin/CODE/KH003Digest/MailDigestKH003.R &
	y=`echo $!`
	x="$x\n$y --KH-003X_Mail"
	sleep 10
fi

if [ "$1" == "KH-004X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH004Digest/MailDigestKH004.R &
	y=`echo $!`
	x="$x\n$y --KH-004X_Mail"
	sleep 10
fi

if [ "$1" == "KH-005X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH005XDigest/MailDigestKH005.R &
	y=`echo $!`
	x="$x\n$y --KH-005X_Mail"
	sleep 10
fi

if [ "$1" == "KH-006X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH006XDigest/MailDigestKH006.R &
	y=`echo $!`
	x="$x\n$y --KH-006X_Mail"
	sleep 10
fi

if [ "$1" == "KH-007X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH007XDigest/MailDigestKH007.R &
	y=`echo $!`
	x="$x\n$y --KH-007X_Mail"
	sleep 10
fi

if [ "$1" == "KH-008X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH008Digest/MailDigestKH008.R &
	y=`echo $!`
	x="$x\n$y --KH-008X_Mail"
	sleep 10
fi



if [ "$1" == "TH-003X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH003Digest/MailDigestTH003.R &
	y=`echo $!`
	x="$x\n$y --TH-003X_Mail"
	sleep 10
fi

if [ "$1" == "MY-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY001Digest/MailDigestMY001.R &
	y=`echo $!`
	x="$x\n$y --MY-001X_Mail"
	sleep 10
fi

if [ "$1" == "MY-003X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY003Digest/MailDigestMY003.R &
	y=`echo $!`
	x="$x\n$y --MY-003X_Mail"
	sleep 10
fi


if [ "$1" == "TH-004L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH004LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-004L_Mail"
fi


if [ "$1" == "TH-005L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH005LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-005L_Mail"
fi

if [ "$1" == "TH-006L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH006LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-006L_Mail"
fi


if [ "$1" == "TH-007L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH007LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-007L_Mail"
fi

if [ "$1" == "TH-008L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH008LDigest/Digest.py "Mail" "2020-09-10"> /home/admin/Logs/LogsTH008LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-008L_Mail"
fi

if [ "$1" == "TH-010L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH010LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-010L_Mail"
fi

if [ "$1" == "TH-011L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH011LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-011L_Mail"
fi


if [ "$1" == "TH-012L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH012LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-012L_Mail"
fi


if [ "$1" == "TH-013L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH013LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-013L_Mail"
fi

if [ "$1" == "TH-014L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH014LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-014L_Mail"
fi

if [ "$1" == "TH-015L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH015LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-015L_Mail"
fi

if [ "$1" == "TH-016L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH016LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-016L_Mail"
fi

if [ "$1" == "TH-017L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH017LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-017L_Mail"
fi

if [ "$1" == "TH-018L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH018LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-018L_Mail"
fi

if [ "$1" == "TH-019L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH019LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-019L_Mail"
fi

if [ "$1" == "TH-020L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH020LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-020L_Mail"
fi

if [ "$1" == "TH-021L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH021LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-021L_Mail"
fi

if [ "$1" == "TH-022L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH022LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-022L_Mail"
fi

if [ "$1" == "TH-023L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH023LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-023L_Mail"
fi

if [ "$1" == "TH-024L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH024LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-024L_Mail"
fi

if [ "$1" == "TH-025L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH025LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-025L_Mail"
fi

if [ "$1" == "TH-026L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH026LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-026L_Mail"
fi

if [ "$1" == "TH-027L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH027LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-027L_Mail"
fi

if [ "$1" == "TH-028L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH028LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-028L_Mail"
fi

if [ "$1" == "TH-046L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH046LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-046L_Mail"
fi

if [ "$1" == "TH-047L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH047LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-047L_Mail"
fi

if [ "$1" == "TH-048L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH048LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-048L_Mail"
fi

if [ "$1" == "TH-049L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH049LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-049L_Mail"
fi

if [ "$1" == "MY-005L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY005LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-005L_Mail"
fi

if [ "$1" == "MY-007L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY007LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-007L_Mail"
fi

if [ "$1" == "MY-010L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY010LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-010L_Mail"
fi

if [ "$1" == "MY-401L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY401LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-401L_Mail"
fi


if [ "$1" == "MY-402L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY402LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-402L_Mail"
fi


if [ "$1" == "MY-403L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY403LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-403L_Mail"
fi


if [ "$1" == "MY-404L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY404LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-404L_Mail"
fi

if [ "$1" == "MY-405L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY405LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-405L_Mail"
fi

if [ "$1" == "MY-406L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY406LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-406L_Mail"
fi


if [ "$1" == "MY-408L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY408LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-408L_Mail"
fi

if [ "$1" == "MY-407L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY407LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-407L_Mail"
fi

if [ "$1" == "MY-409L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY409LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-409L_Mail"
fi

if [ "$1" == "MY-410L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY410LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-410L_Mail"
fi


if [ "$1" == "MY-411L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY411LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-411L_Mail"
fi

if [ "$1" == "MY-412L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY412LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-412L_Mail"
fi

if [ "$1" == "MY-413L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY413LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-413L_Mail"
fi

if [ "$1" == "MY-414L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY414LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-414L_Mail"
fi


if [ "$1" == "MY-417L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY417LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-417L_Mail"
fi


if [ "$1" == "MY-418L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY418LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-418L_Mail"
fi

if [ "$1" == "MY-419L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY419LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-419L_Mail"
fi


if [ "$1" == "MY-420L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY420LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-420L_Mail"
fi

if [ "$1" == "MY-421L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY421LDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsMY421LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-421L_Mail"
    sleep 10
fi

if [ "$1" == "MY-422L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY422LDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsMY422LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-422L_Mail"
    sleep 10
fi

if [ "$1" == "MY-423L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY423LDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsMY423LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-423L_Mail"
    sleep 10
fi

if [ "$1" == "MY-424L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY424LDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsMY424LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-424L_Mail"
    sleep 10
fi

if [ "$1" == "MY-425L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY425LDigest/Digest.py "Mail" "2020-09-20"> /home/admin/Logs/LogsMY425LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-425L_Mail"
    sleep 10
fi

if [ "$1" == "MY-426L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY426LDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsMY426LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-426L_Mail"
    sleep 10
fi

if [ "$1" == "MY-427L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY427LDigest/Digest.py "Mail" "2020-09-16"> /home/admin/Logs/LogsMY427LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-427L_Mail"
    sleep 10
fi

if [ "$1" == "MY-428L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY428LDigest/Digest.py "Mail" "2020-09-08"> /home/admin/Logs/LogsMY428LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-428L_Mail"
    sleep 10
fi

if [ "$1" == "MY-429L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY429LDigest/Digest.py "Mail" "2020-09-22"> /home/admin/Logs/LogsMY429LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-429L_Mail"
    sleep 10
fi

if [ "$1" == "MY-430L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY430LDigest/Digest.py "Mail" "2020-09-22"> /home/admin/Logs/LogsMY430LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-430L_Mail"
    sleep 10
fi

if [ "$1" == "MY-430L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY431LDigest/Digest.py "Mail" "2020-09-25"> /home/admin/Logs/LogsMY431LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-431L_Mail"
    sleep 10
fi

if [ "$1" == "MY-432L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY432LDigest/Digest.py "Mail" "2020-09-25"> /home/admin/Logs/LogsMY432LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-432L_Mail"
    sleep 10
fi

if [ "$1" == "MY-433L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY433LDigest/Digest.py "Mail" "2020-09-25"> /home/admin/Logs/LogsMY433LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-433L_Mail"
    sleep 10
fi

if [ "$1" == "VN-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN001XDigest/MailDigestVN001.R &
	y=`echo $!`
	x="$x\n$y --VN-001X_Mail"
	sleep 10
fi

if [ "$1" == "MasterMail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MasterMail/MasterMail.R &
	y=`echo $!`
	x="$x\n$y --MasterMail"
	sleep 10
fi

if [ "$1" == "SERIS3G_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SERIS_3G/SERIS_3G.R &
	y=`echo $!`
	x="$x\n$y --SERIS3G"
	sleep 10
fi

if [ "$1" == "MY-001X_Azure4G" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/MY001X/MY-001X_azure_4g.py > /home/admin/Logs/LogsMY001X_Azure_4G.txt &
	y=`echo $!`
	x="$x\n$y --MY-001X_Azure4G"
	sleep 10
fi

if [ "$1" == "MY-003X_Azure4G" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/MY003X/MY-003X_azure_4g.py > /home/admin/Logs/LogsMY003X_Azure_4G.txt &
	y=`echo $!`
	x="$x\n$y --MY-003X_Azure4G"
	sleep 10
fi

if [ "$1" == "SG-003E_AzureAll" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/SG003E/SG-003E_azure_all.py > /home/admin/Logs/LogsSG003E_Azure_All.txt &
	y=`echo $!`
	x="$x\n$y --SG-003E_AzureAll"
	sleep 10
fi

if [ "$1" == "SG-007E_AzureAll" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/SG007E/SG-007E_azure_all.py > /home/admin/Logs/LogsSG007E_Azure_All.txt &
	y=`echo $!`
	x="$x\n$y --SG-007E_AzureAll"
	sleep 10
fi

if [ "$1" == "GIS_MASTER_ALL" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/GIS_Master_All/gis_master_all.py > /home/admin/Logs/LogsGISMasterAll.txt &	
	y=`echo $!`
	x="$x\n$y --GIS_MASTER_ALL"
	sleep 10
fi

if [ "$1" == "GIS_SUM_AHME" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/GIS_SUM/gis_sum_ahmedabad.py > /home/admin/Logs/LogsGIS_sum_ahme.txt &
	y=`echo $!`
	x="$x\n$y --GIS_SUM_AHME"
	sleep 10
fi

if [ "$1" == "GIS_SUM_NEWD" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/GIS_SUM/gis_sum_newd.py > /home/admin/Logs/LogsGIS_sum_newd.txt &
	y=`echo $!`
	x="$x\n$y --GIS_SUM_NEWD"
	sleep 10
fi

if [ "$1" == "Station_All_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/Alarms/Station_All_Alarm.py > /home/admin/Logs/LogsStationAllAlarm.txt &
	y=`echo $!`
	x="$x\n$y --Station_All_Alarm"
	sleep 10
fi

if [ "$1" == "KH003_Merge_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Azure/KH003_Merge_Live.py > /home/admin/Logs/LogsKH003MergeLive.txt &
	y=`echo $!`
	x="$x\n$y --KH003_Merge_Live"
	sleep 10
fi

if [ "$1" == "Seris_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Alarms/Seris_Alarm.py > /home/admin/Logs/LogsSeris_Alarm.txt &
	y=`echo $!`
	x="$x\n$y --Seris_Alarm"
	sleep 10
fi

if [ "$1" == "Locus_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/Alarms/Locus_Alarm.py > /home/admin/Logs/LogsLocus_Alarm.txt &
	y=`echo $!`
	x="$x\n$y --Locus_Alarm"
	sleep 10
fi


if [ "$1" == "Bot_Status" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Alarms/Bot_Status.py > /home/admin/Logs/LogsBotStatus.txt &
	y=`echo $!`
	x="$x\n$y --Bot_Status"
	sleep 10
fi

fi # RunRONLY

if [ $RUNPYTHONONLY -eq 0 ] 
then
if [ "$1" == "FaultAlert" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/Misc/FaultAlert.R &
	y=`echo $!`
	x="$x\n$y --FaultAlert"
	sleep 10
fi

if [ "$1" == "DropboxCheck" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/Misc/dropbox.R &
	y=`echo $!`
	x="$x\n$y --DropboxCheck"
	sleep 10
fi

if [ "$1" == "MemoryProfile" ] || [ $RUNALL == 1 ] 
then
	sh /home/admin/Usage.sh &
	sleep 1
fi
fi #RUNPYTHONONLY

if [ $# == 0 ] 
then 
	echo -e "$x" > "/home/admin/psIds.txt"
else 
	y1="$(echo -e "${x}" | sed -e ':a;N;$!ba;s/\n/ /g')"
	y2="$(echo -e "${y1}" | sed -e 's/^[[:space:]]*//')"
	sed -i "/$1/c$y2" "/home/admin/psIds.txt"
	#echo -e "$x" >> "/home/admin/psIds.txt"
fi

