import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import smtplib
from email.mime.text import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders
import logging


tz = pytz.timezone('Asia/Calcutta')

path_read='/home/admin/Dropbox/Gen 1 Data/[IN-014L]'



def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

bot_type=sys.argv[1]
start=sys.argv[2]

stn=['IN-014L']
irr_stn=['IN-015S']
meters={'MFM_1_PV Meter 01':'Phase 1 Meter 1','MFM_2_PV Meter 02':'Phase 1 Meter 2','MFM_3_PV Meter phase 02':'Phase 2 Meter 1'}


path_write='/home/admin/Dropbox/Second Gen/['+stn[0]+']'
path_write_3g='/home/admin/Dropbox/Third Gen/['+stn[0]+']'
path_write_4g='/home/admin/Dropbox/Fourth_Gen/['+stn[0]+']/['+stn[0]+']-lifetime.txt'
startpath="/home/admin/Start/MasterMail/"


chkdir(path_write)
chkdir(path_write_3g)
chkdir(path_write_4g[:-22])

mfm_capacities={'IN-014L':[378,226.8,453.6]}
no_meters=[3]
cod_date=['2016-05-27']
components=[] #Ordering Components so that WMS is first.Done since PR requires GHI to be updated first.


date=datetime.datetime.now(tz).strftime('%Y-%m-%d') #remove later

for i in sorted(os.listdir(path_read+'/'+date[0:4]+'/'+date[0:7])):
    if 'WMS' in i:
        components.insert(0,i)
    elif(('Meter' in i) or ('PV' in i) and ('Grid' not in i)):
        components.append(i)
print(components)

def create_template(Type,date,eac1='NA',eac2='NA',yld1='NA',yld2='NA',pr1='NA',pr2='NA',ghi='NA',da='NA',lastread='NA'):
    if(Type=='INV'):
        df_template = pd.DataFrame({'Date':[date],'DA':[da],'Eac':[eac2],'Yld':[yld2]},columns =['Date','DA','Eac','Yld'])
    elif(Type=='WMS'):
        df_template = pd.DataFrame({'Date':[date],'DA':[da],'GHI':[ghi]},columns =['Date','DA','GHI'])
    elif(Type=='MFM'):
        df_template = pd.DataFrame({'Date':[date],'DA':[da],'Eac1':[eac1],'Eac2':[eac2],'Yld1':[yld1],'Yld2':[yld2],'PR1':[pr1],'PR2':[pr2],'LastRead':[lastread]},columns =['Date','DA','Eac1','Eac2','Yld1','Yld2','PR1','PR2','LastRead'])
    return df_template

def site_information(date,site_name,location,stn,capacity,nometers,cod_date):
  name=  "Site Name: "+site_name+"\n\n"
  loc = "Location: "+location+"\n\n"
  code = "O&M Code: "+stn+"\n\n"
  size = "System Size [kWp]:"+str(capacity)+'\n\nSystem Size Facing North: 529.2 kWp (50.0% of total)\n\nSystem Size Facing South: 529.2 kWp (50.0% of total)\n\n'
  no_meters = "Number of Energy Meters: "+str(nometers)+"\n\n"
  brands= "Module Brand / Model / Nos: Trina / 315 Wp / 3360\n\nInverter Brand / Model / Nos: SMA / 60kW / 14 \n\n"
  #cod = "Site COD: "+str(cod_date)+"\n\nSystem age [days]:"+str(date-datetime.datetime.strptime(cod_date,"Y%-%m-%d"))+"\n\nSystem age [years]:"+str((date-datetime.datetime.strptime(cod_date,"Y%-%m-%d")/365))+"\n\n"
  info=name+loc+code+size+no_meters+brands
  return info

def get_irr(date,stn):
    irr_data={'GHI':'NA','DA':0}
    if(os.path.exists('/home/admin/Dropbox/Second Gen/[IN-015S]/'+date[0:4]+'/'+date[0:7]+'/[IN-015S] '+date+'.txt')):
        df=pd.read_csv('/home/admin/Dropbox/Second Gen/[IN-015S]/'+date[0:4]+'/'+date[0:7]+'/[IN-015S] '+date+'.txt',sep='\t')
        irr_data['GHI']=df['Gsi'][0]
        irr_data['DA']=df['DA'][0]
    return irr_data

def send_mail(date,stn,info,rec,attachment_path_list=None):
    s = smtplib.SMTP("smtp.office365.com")
    s.starttls()
    s.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    #s.set_debuglevel(1)
    msg = MIMEMultipart()
    sender = 'operations@cleantechsolar.com'
    recipients = ['rupesh.baker@cleantechsolar.com','jeeva.govindarasu@cleantechsolar.com','rohit.jaswal@cleantechsolar.com','sai.pranav@cleantechsolar.com','anusha.agarwal@cleantechsolar.com','oms@avisolar.com']
    msg['Subject'] = "Station "+stn+" Digest "+str(date)
    if sender is not None:
        msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
            try:
                file_name=each_file_path.split("/")[-1]
                part = MIMEBase('application', "octet-stream")
                part.set_payload(open(each_file_path, "rb").read())

                Encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                msg.attach(part)
            except:
                print ("could not attache file")
    msg.attach(MIMEText(info,'plain/text'))
    s.sendmail(sender, recipients, msg.as_string())

def process(date,stn_name,stn_index):
    total_eac=0#Store induvidual for total calculation
    total_yld=0#Store induvidual for total calculation
    total_pr=0#Store induvidual for total calculation
    meter_ylds=[]
    file_path=[]
    site_info=site_information(date,'DTVS Oragadam','Chennai, India','IN-014',sum(mfm_capacities[stn_name]),no_meters[stn_index],cod_date[stn_index])
    digest_body=site_info
    digest_body=digest_body+'--------------------------\nFull Site\n--------------------------\n\n'
    for index,i in enumerate(components):
        if(index==0):
            if(irr_stn[stn_index]!='NA'): #WMS NOT THERE
                irr_data=get_irr(date,irr_stn[stn_index])
                df_2g=create_template('WMS',date)
                df_2g['Date']=date
                df_2g['DA']=irr_data['DA']
                df_2g['GHI']=irr_data['GHI']
                GHI=df_2g['GHI'][0]
                digest_body=digest_body+'\n\n--------------------------\nWMS\n--------------------------\n\nDA [%]: '+str(df_2g['DA'][0])+'\n\nGHI [kWh/m^2] (From '+irr_stn[stn_index]+'): '+str(GHI)
                df_merge=df_2g.copy()
                cols_4g=['Date','WMS.DA','WMS.GHI']
            else:
                cols_4g=['Date']
        chkdir(path_write+'/'+date[0:4]+'/'+date[0:7]+'/'+i)
        if('INV' in i):
            df_2g=create_template('INV',date)
            if(os.path.exists(path_read+'/'+date[0:4]+'/'+date[0:7]+'/'+i+'/'+'['+stn_name+']-'+i+'-'+date+'.txt')):
                df=pd.read_csv(path_read+'/'+date[0:4]+'/'+date[0:7]+'/'+i+'/'+'['+stn_name+']-'+i+'-'+date+'.txt',sep='\t')
        elif('WMS' in i):
            df_2g=create_template('WMS',date)
            GHI=0
            if(os.path.exists(path_read+'/'+date[0:4]+'/'+date[0:7]+'/'+i+'/'+'['+stn_name+']-'+i+'-'+date+'.txt')):
                df=pd.read_csv(path_read+'/'+date[0:4]+'/'+date[0:7]+'/'+i+'/'+'['+stn_name+']-'+i+'-'+date+'.txt',sep='\t')
        elif('Meter' in i):
            df_2g=create_template('MFM',date)
            if(os.path.exists(path_write_3g+'/'+i+'/'+date[0:7]+'.txt')):
                df_3g=pd.read_csv(path_write_3g+'/'+i+'/'+date[0:7]+'.txt',sep='\t')
            if(os.path.exists(path_write_4g)):
                df_4g=pd.read_csv(path_write_4g,sep='\t')
            if(os.path.exists(path_read+'/'+date[0:4]+'/'+date[0:7]+'/'+i+'/'+'['+stn_name+']-'+i[0:3]+i[4]+'-'+date+'.txt')):
                df=pd.read_csv(path_read+'/'+date[0:4]+'/'+date[0:7]+'/'+i+'/'+'['+stn_name+']-'+i[0:3]+i[4]+'-'+date+'.txt',sep='\t')
                da=len(df.loc[:,'W_avg'].dropna())/2.88
                df_2g['DA']=round(da,1)
                df_2g['Eac1']=round(df['W_avg'].sum()/12000,2)
                df_2g['Yld1']=round(df['W_avg'].sum()/(mfm_capacities[stn_name][index]*12000),2)
                if(('WhExp_sum' not in df.columns) or (df['WhExp_sum'].dropna().empty)):
                    
                    df_2g['Yld2']='NA'
                    df_2g['Eac2']='NA'
                    df_2g['LastRead']='NA'
                else:
                    df_2g['Eac2']=round((df['WhExp_sum'].tail(1).values[0]-df['WhExp_sum'].head(1).values[0])/1000,2)
                    df_2g['Yld2']=round((df['WhExp_sum'].tail(1).values[0]-df['WhExp_sum'].head(1).values[0])/(mfm_capacities[stn_name][index]*1000),2)
                    df_2g['LastRead']=round(df['WhExp_sum'].dropna().tail(1).values[0]/1000,2)
                    print(df['WhExp_sum'].head())
                if(GHI==0 or GHI=='NA' or df_2g['Yld1'][0]=='NA'):
                    pass
                else:
                    df_2g['PR1']=round((df_2g['Yld1'][0]*100)/GHI,1)
                if(GHI==0 or GHI=='NA' or df_2g['Yld2'][0]=='NA'):
                    pass
                else:
                    df_2g['PR2']=round((df_2g['Yld2'][0]*100)/GHI,1)
                if(df_2g['Eac2'][0]=='NA'):
                    pass
                else:
                    total_eac=total_eac+df_2g['Eac2'][0]
                    meter_ylds.append(df_2g['Yld2'][0])
                #Stdev/COV Yields:
                if(df_2g['PR2'][0]=='NA'):
                    pass
                else:
                    total_pr=total_pr+df_2g['PR2'][0]*mfm_capacities[stn_name][index]
            digest_body=digest_body+'\n\n--------------------------\n'+meters[i.strip()]+'\n--------------------------\n\nDA [%]: '+str(df_2g['DA'][0])+'\n\nSize [kWp]: '+str((mfm_capacities[stn_name][index]))+'\n\nEAC method-1 (Pac) [kWh]: '+str(df_2g['Eac1'][0])+'\n\nYield method-1 [kWh/kWp]: '+str(df_2g['Yld1'][0])+'\n\nYield method-2 [kWh/kWp] : '+str(df_2g['Yld2'][0])+'\n\nPR-1 (GHI) [%]: '+str(df_2g['PR1'][0])+'\n\nPR-2 (GHI) [%]: '+str(df_2g['PR2'][0])+'\n\nLast recorded value [kWh]: '+str(df_2g['LastRead'][0])
        df_2g.to_csv(path_write+'/'+date[0:4]+'/'+date[0:7]+'/'+i+'/'+'['+stn_name+']-'+i+'-'+date+'.txt',sep='\t',index=False)
        file_path.append(path_write+'/'+date[0:4]+'/'+date[0:7]+'/'+i+'/'+'['+stn_name+']-'+i+'-'+date+'.txt')
        chkdir(path_write_3g+'/'+i)
        if(os.path.exists(path_write_3g+'/'+i+'/'+date[0:7]+'.txt')):
            if((df_3g['Date']==date).any()):#Third Gen
                vals=df_2g.values.tolist()
                df_3g.loc[df_3g['Date']==date,df_3g.columns.tolist()]=vals[0]
                df_3g.to_csv(path_write_3g+'/'+i+'/'+date[0:7]+'.txt',sep='\t',mode='w',index=False,header=True)
            else:
                df_2g.to_csv(path_write_3g+'/'+i+'/'+date[0:7]+'.txt',sep='\t',mode='a',index=False,header=False)
        else:
            df_2g.to_csv(path_write_3g+'/'+i+'/'+date[0:7]+'.txt',sep='\t',mode='a',index=False,header=True) #First time
        if(index==0 and irr_stn[stn_index]=='NA'):#WMS THERE
            df_merge=df_2g.copy()
        else:
            df_merge=df_merge.merge(df_2g,on='Date')
        a=[components[index]+'.'+n for n in df_2g.columns.tolist()[1:]]
        cols_4g=cols_4g+(a) 
    df_merge.columns=cols_4g
    index=digest_body.find('\n\n--------------------------\nWMS')
    full_site='System Full Generation [kWh]: '+str(total_eac)+'\n\nSystem Full Yield [kWh/kWp]: '+str(round(total_eac/sum(mfm_capacities[stn_name]),2))+'\n\nSystem Full PR [%]: '+str(round(total_pr/sum(mfm_capacities[stn_name]),1))
    digest_body=digest_body[:index] +full_site+ digest_body[index:]
    print(digest_body)
    if not os.path.exists(path_write_4g):
        df_merge.to_csv(path_write_4g,sep='\t',mode='a',index=False,header=True)
    elif((df_4g['Date']==date).any()):#Fourth Gen
        vals=df_merge.values.tolist()
        df_4g.loc[df_4g['Date']==date,df_4g.columns.tolist()]=vals[0]
        df_4g.to_csv(path_write_4g,sep='\t',mode='w',index=False,header=True)
    else:
        df_merge.to_csv(path_write_4g,sep='\t',mode='a',index=False,header=False)
    print('4G Done')
    return [digest_body,file_path]



if(os.path.exists(startpath+stn[0]+"_Mail.txt")):
    print('Exists')
else:
    try:
        shutil.rmtree(path_write,ignore_errors=True)
        shutil.rmtree(path_write_3g,ignore_errors=True)
        os.remove(path_write_4g)
    except Exception as e:
        print(e)
    with open(startpath+stn[0]+"_Mail.txt", "w") as file:
        timenow=(datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')  
        file.write(timenow+"\n"+start)   

with open(startpath+stn[0]+"_Mail.txt") as f:
    startdate = f.readlines()[1].strip()
print(startdate)
startdate=datetime.datetime.strptime(startdate,"%Y-%m-%d")

#Historical
print('Historical Started!')
startdate=startdate+datetime.timedelta(days=1) #Add one cause startdate represent last day that mail was sent successfully
while((datetime.datetime.now(tz).date()-startdate.date()).days>0):
    print('Historical Processing!')
    processed_data=process(str(startdate)[0:10],stn[0],0)
    if(bot_type=='Mail'):
        send_mail(str(startdate)[0:10],'[IN-014L]',processed_data[0],'asd',processed_data[1])
    startdate=startdate+datetime.timedelta(days=1)
startdate=startdate+datetime.timedelta(days=-1) #Last day for which mail was sent
with open(startpath+stn[0]+"_Mail.txt", "w") as file:
    timenow=(datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S') 
    file.write(timenow+"\n"+(startdate).strftime('%Y-%m-%d'))  
print('Historical Done!')

while(1):
    try:
        date=(datetime.datetime.now(tz)).strftime('%Y-%m-%d')
        timenow=(datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
        for stn_index,stn_name in enumerate(stn):
            with open(startpath+stn_name+"_Bot.txt", "w") as file:
                file.write(timenow)   
            processed_data=process(date,stn_name,stn_index)
            print('Done Processing')
            if(datetime.datetime.now(tz).hour==2 and (datetime.datetime.now(tz).date()-startdate.date()).days>1):
                print('Sending')
                date_yesterday=(datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
                processed_data=process(date_yesterday,stn_name,stn_index)
                send_mail(date_yesterday,'[IN-014L]',processed_data[0],'asd',processed_data[1])
                with open(startpath+stn_name+"_Mail.txt", "w") as file:
                    file.write(timenow+"\n"+date_yesterday)   
                startdate=datetime.datetime.strptime(date_yesterday,'%Y-%m-%d')
        print('Sleeping')
    except:
        logging.exception('msg')
    time.sleep(300)



