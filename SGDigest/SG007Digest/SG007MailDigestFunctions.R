rm(list = ls())
TIMESTAMPSALARM = NULL
ltcutoff = .001
CONSTYIELD=167.7
path4G = "/home/admin/Dropbox/Fourth_Gen/[SG-007X]/[SG-007X]-lifetime.txt"
RESET4G = 1
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

fetchGSIData = function(date)
{
	pathMain = '/home/admin/Dropbox/Second Gen/[SG-003S]'
	pathMain2 = '/home/admin/Dropbox/Second Gen/[SG-001X]' 
	yr = substr(date,1,4)
	mon = substr(date,1,7)
	txtFileName = paste('[SG-003S] ',date,".txt",sep="")
	txtFileName2 = paste('[SG-001X] ',date,".txt",sep="")
	pathyr = paste(pathMain,yr,sep="/")
	gsiVal = NA
	if(file.exists(pathyr))
	{
		pathmon = paste(pathyr,mon,sep="/")
		if(file.exists(pathmon))
		{
			pathfile = paste(pathmon,txtFileName,sep="/")
			if(file.exists(pathfile))
			{
				dataread = read.table(pathfile,sep="\t",header = T)
				gsiVal = as.numeric(dataread[,3])
			}
		}
	}
	pathyr2 = paste(pathMain2,yr,sep="/")
	gsiVal2 = NA
	if(file.exists(pathyr2))
	{
		pathmon2 = paste(pathyr2,mon,sep="/")
		if(file.exists(pathmon2))
		{
			pathfile2 = paste(pathmon2,txtFileName2,sep="/")
			if(file.exists(pathfile2))
			{
				dataread = read.table(pathfile2,sep="\t",header = T)
				gsiVal2 = as.numeric(dataread[,8])
			}
		}
	}
	return(c(gsiVal,gsiVal2))
}

secondGenData = function(filepath,writefilepath)
{
	TIMESTAMPSALARM <<- NULL
  
	dataread = try(read.table(filepath,header = T,sep = "\t",stringsAsFactors=F),silent=F)
	dataread2 = dataread
	lastread = lasttime = Eac2 = "NA"
	if(class(dataread)=="try-error" || ncol(dataread) < 9)
	{
		return(NA)
	}
	idxpac = 15
	idxen = 39
	if(ncol(dataread) > 50)
	{
		idxpac = 47
		idxen = 25
	}
	dateac  = as.character(dataread[1,1])
	if((as.numeric(substr(dateac,1,4)) > 2019) || 
	((as.numeric(substr(dateac,1,4)) == 2019) && (as.numeric(substr(dateac,6,7) > 3))))
	{
		idxpac = 36
		idxen = 2
	}
	dataread = dataread[complete.cases(dataread[,idxen]),]
	if(nrow(dataread) > 0)
	{
	lastread = as.character(as.numeric(dataread[nrow(dataread),idxen])/1000)
	lasttime = as.character(dataread[nrow(dataread),1])
	Eac2 = format(round((as.numeric(dataread[nrow(dataread),idxen]) - as.numeric(dataread[1,idxen]))/1000,1),nsmall=1)
	}
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) < 0,]
	Eac1 = 0
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(round(sum(as.numeric(dataread[,idxpac]))/-60000,1),nsmall=1)
	}
	dataread = dataread2[complete.cases(dataread2[,9]),]
	dataread = dataread[as.numeric(dataread[,9]) < 0,]
	
	dataread = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 540,]
  tdx = tdx[tdx > 540]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
  missingfactor = 600 - nrow(dataread2)
  dataread2 = dataread2[abs(as.numeric(dataread2[,idxpac])) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
		TIMESTAMPSALARM <<- as.character(dataread2[,1])
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/1.2,1),nsmall=1)
	date = substr(as.character(dataread[1,1]),1,10)
	gsiVal = fetchGSIData(date)
	gsiVal2 = gsiVal[2]
	gsiVal = gsiVal[1]
	dspyield1 = round((as.numeric(Eac1)/CONSTYIELD),2)
	dspyield2 = round((as.numeric(Eac2)/CONSTYIELD),2)
	PR1 = round((dspyield1*100/as.numeric(gsiVal)),1)
	PR2 = round((dspyield2*100/as.numeric(gsiVal)),1)
	PR3 = round((dspyield1*100/as.numeric(gsiVal2)),1)
	PR4 = round((dspyield2*100/as.numeric(gsiVal2)),1)
  df = data.frame(Date = date, Eac1 = as.numeric(Eac1),Eac2 = as.numeric(Eac2),
             #LoadDelivered = as.numeric(Eac22),EnergyRecv=as.numeric(Eac3),
						 DA = DA,Downtime = totrowsmissing,
						LastTime = lasttime, LastReadDel = lastread,
						#LastReadRecv=lastread2,GSI=gsiVal,
						DailySpecYield1=dspyield1,
						DailySpecYield2=dspyield2,
						PR1SG003=PR1,
						PR2SG003=PR2,
						GHISG003=gsiVal,
						PR1SG001=PR3,
						PR2SG001=PR4,
						GHISG001=gsiVal2,
						stringsAsFactors = F)
	
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1,writefilepath,tmPush)
{
  dataread1 = try(read.table(filepathm1,header = T,sep = "\t",stringsAsFactors=F),silent=T) #its correct dont change
	if(class(dataread1) == "try-error")
		return()
	dateac = as.character(dataread1[1,1])
	if(nchar(dateac) < 10)
	{
		print(paste("Error in date read.. date read was",dateac,"and its incorrect"))
		return()
	}
  {
    if(!file.exists(writefilepath))
    {
      write.table(dataread1,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
    else
    {
  		dataread2 = read.table(writefilepath,header = T,sep = "\t",stringsAsFactors=F)
			dateall = as.character(dataread2[,1])
			idxmtch = match(dateac,dateall)
			{
			if(!is.finite(idxmtch))
      	write.table(dataread1,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
			else
			{
				dataread2[idxmtch,] = dataread1[1,]
      	write.table(dataread2,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
			}
			}
    }
	}
	fourthGenData(filepathm1,tmPush)
}

fourthGenData = function(path,tmPush)
{
	data = read.table(path,header=T,sep="\t",stringsAsFactors=F)
	dateac = as.character(data[,1])
	colnamesuse = c(colnames(data),"TimeUpdated","StnID")
	if(is.na(tmPush))
		tmPush = as.character(format(Sys.time(),tz = "Singapore"))
	data[1,(ncol(data)+1)] = tmPush
	data[1,(ncol(data)+1)] = "SG-007X"
	colnames(data) = colnamesuse
	{
		if(RESET4G)
		{
			write.table(data,file=path4G,sep="\t",row.names=F,col.names=T,append=F)
			RESET4G <<- 0
		}
		else
		{
  		dataread2 = read.table(path4G,header = T,sep = "\t",stringsAsFactors=F)
			dateall = as.character(dataread2[,1])
			idxmtch = match(dateac,dateall)
			print(paste(idxmtch,"is idxmtch"))
			{
			if(!is.finite(idxmtch))
      	write.table(data,file = path4G,row.names = F,col.names = F,sep = "\t",append = T)
			else
			{
				dataread2[idxmtch,] = data[1,]
      	write.table(dataread2,file = path4G,row.names = F,col.names = T,sep = "\t",append = F)
			}
			}
		}
	}
}
