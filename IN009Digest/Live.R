rm(list = ls())

#source Functions.R for fetchrawdata and cleansedata function calls
source('/home/admin/CODE/IN009Digest/Functions.R')
require('mailR')

#file to store the error logs.
errHandle = file('/home/admin/Logs/LogsIN009Gen1.txt',open='w',encoding='UTF-8')

#direct all warnings, messages and errors to the log file
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

NODATACOUNTER = 0  # Variable to handle TORP SERVER fault
DATASENTFORTHEDAY = 0 # Variable to check if TORP SERVER fault mail sent for day

#convert time in format HH:MM to a numerical value between 1-1140
timetomins = function(x)
{
	splitup = unlist(strsplit(x,":"))
	hr = as.numeric(splitup[1])
	min = as.numeric(splitup[2])
	return(((hr * 60)+min+1))
}


#convert time in format 'DD-MM-YYYY HH:MM' to a numerical value between 1-1140
timetomins2 = function(x)
{
	lists = unlist(strsplit(x,"\\ "))
	seq1 = seq(from = 2, to = length(lists),by=2)
	lists = lists[seq1]
	lists = unlist(strsplit(lists,":"))
	seq1 = seq(from = 1, to = length(lists),by = 2)
	seq2 = seq(from = 2, to = length(lists),by = 2)
	hr = as.numeric(lists[seq1])
	min = as.numeric(lists[seq2])
	return((hr * 60)+min+1)
}

################################################################################
#function to send a mail along with the TWILIO message alert. 
#The firetwilio function checks if a warning message has to be sent, if yes this
#function is called from the firetwilio function called (defined later in this
#file), which sends a mail to the recipients.
#Function takes inputs message and subject of the mail and returns once mail is
#sent successfully.
###############################################################################
sendEmailToo = function(message,subj)
{
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = c('andre.nobre@cleantechsolar.com','rupesh.baker@cleantechsolar.com', 'jeeva.govindarasu@cleantechsolar.com', 'rohit.jaswal@cleantechsolar.com','abhishek.puri@cleantechsolar.com')
pwd = 'CTS&*(789'
  send.mail(from = sender,
            to = recipients,
            subject = paste(subj),
            body = message,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            debug = F)
}

#name of the station
stationno = "[IN-009T]"

#The last day which was read from the TORP server is stored onto a file.
#pathlastday read stores this path.
pathlastday = "/home/admin/Start/IN009.txt"

#Path to store the raw-files
pathraw = paste("/home/admin/Data/TORP Data",stationno,sep="/")

#Path for the gen-1 data
pathgen1 = paste("/home/admin/Dropbox/Gen 1 Data",stationno,sep="/")

monthsno = c("01","02","03","04","05","06","07","08","09","10","11","12")
daysmonthsno = c(31,28,31,30,31,30,31,31,30,31,30,31)

#Flags used by the twilio function to check if a warning has to be sent
#each flag is explained in great detail in the firetwilio function call
doneforthedayfiringpac = doneforthedayfiringpr = prless50 = prless60 = 0
lastfiredtspac = lastfiredtspr25 = lastfiredtspr50 = lastfiredtspr60  = " "

lastsuccesspacts = " "

#path to store last time-stamp of day for which warning mails and text was sent
pathtstwilioread = "/home/admin/TwilioStart/IN-009.txt"


################################################################################
#Function to send a warning that the FTP server for TORP is down.
#This is triggered if the 'cleansedata' function call returns a null data-frame
#after 10 consecutive calls. The NODATACOUNTER is incremented by 1 every-time 
#the cleansedata call returns 0 rows. When this value becomes 10 the mail is fired
#This mail is fired at max once a day and sets the
#DATASENTFORTHEDAY flag which is only reset when a new day is encountered
################################################################################

SENDWARNINGMAIL = function()
{
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = c('operations@cleantechsolar.com')
  send.mail(from = sender,
            to = recipients,
            subject = "IN-009T TORP DATA SERVER DOWN",
            body = " ",
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            debug = F)
DATASENTFORTHEDAY <<-1
}

################################################################################
#This function sends a mail if the TORP server is back running once it was 
#flagged down. The mail is sent only when the SENDWARNINGMAIL function call was 
#called on the same day and when the TORP server is pumping back data
###############################################################################
SENDOKMAIL = function()
{
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = c('operations@cleantechsolar.com')
  send.mail(from = sender,
            to = recipients,
            subject = "IN-009T TORP DATA SERVER BACK-UP",
            body = " ",
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            debug = F)
	DATASENTFORTHEDAY <<-0
}

################################################################################
#Check if the file which stores last time a TWILIO message was sent is present
#if it is, read its contents and set lastsuccesspacts flag to the content of the
#file
################################################################################
if(file.exists(pathtstwilioread))
{
	tmp = readLines(pathtstwilioread)
	if(length(tmp) && tmp!= " ")
	{
		time = Sys.time()
		time = format(time, tz="Asia/Calcutta",usetz=TRUE)
		if(substr(time,1,10) == substr(tmp,1,10))
		{
		lastsuccesspacts = tmp
		}
	}
}

################################################################################
#Functions to handle firing of warning messages in-case of PR error or Pac 
#reading error.
#
#The function takes as input, path to the Gen-1 data which is obtained using the 
#fetchrawdata and cleansedata function calls.
#
#The function checks for the following errors:
#1. PR Error:
#		a) Is last time-stamp between 8am - 5pm, if not return
#		b) Check if doneforthedayfiringpr flag is not set, if it is set(i.e equal to 1)
#			return. (to know more about the doneforthedayfiringpr function refer to the
#			manageflags function).
#		c) Once a) & b) are satisfied calculate Live PR and perform following checks
#			(i) If live-PR is > 60 but less than 100, reset prless60 and prles50 variable to 0
#			(ii) If live-PR is < 25 or live-PR > 100 but less than 500, 
#				fire a warning message immediately. Once this is done, 
#				set the doneforthedayfiringpr flag & update the variable lastfiredtspr25
#				to the current time. Also reset prless50 and prless60 variables to 0.
#			(iii) If live-PR is > 25 but live-PR is < 50, increment the prless50 
#					and prless60 variable by 1.
#					If the prless50 variable is greater than 2 fire the warning message and 
#					set doneforthedayfiringpr flag & update the variable lastfiredtspr50 to
#					the current time. Reset the prless50 and prless60 variables to 0.
#			(iv) If live-PR is > 50 but live-PR is < 60, increment the prless60
#					variable by 1.
#					If the prless60 variable is greater than 4 fire the warning message and
#					set doneforthedayfiringpr flag & update the variable lastfiredtspr60 to
#					current time. Reset the prless60 prless50 variables to 0.
#
#2. Pac-Error:
#		a)Is the last time-stamp between 8am-5pm, if not return
#		b) Check if doneforthedayfiringpac flag is not set, if its set(i.e equal to 1)
#			return. 
#		c)If a) and b) are satisfied extract the portion of the Gen-1 data from 
#		lastsuccesspacts variable to the end of the data-file. For example:
#		
#		If lastsuccesspacts is 2016-12-12 10:28:00 and the Gen-1 data is as below:
#		
#		Tm									Pac	Eac	GHI		Tmod
#		2016-12-12 10:23:00	15	994	1024	38.2
#		2016-12-12 10:28:00	16	997	1022	38.0
#		2016-12-12 10:33:00	12	991	1023	38.4
#		2016-12-12 10:38:00	11	999	1021	38.0
#	
#		Then extract sub data fro 2016-12-12 10:33:00 till the end of file.
#
#		Once the data has been extracted update the lastsuccesspacts variable to the
#		last timestamp of the Gen-1 data (In the above example lastsuccesspacts will
#		be updated to 2016-12-12 10:38:00). Check if the last recorded Pac value is less
#		than the threshold (0.5W).
#		If yes then update the lastfiredpacts variable to the current time-stamp and
#		set the doneforthedayfiringpac flag.
#
#Based on the above errors a warning message is fired which is done by callig a
#python script. We pass as arguments to this script the body of the message, which
#contains the time-stamp along with the corresponding Pac reading or the live-PR
#based on who fires the warning message. Similarly the sendmailtoo function is also
#called which fires a mail containg the warning message.
################################################################################

firetwilio = function(day)
{
	
	#if both doneforthedayfiringpac and doneforthedayfiringpr flags are set return
	#as no warning messages are to be sent
	if(doneforthedayfiringpac && doneforthedayfiringpr)
	{
		print(paste('Done for the day firing both twilio',day))
		return()
	}

	dataread = read.table(day,header = T,sep="\t",stringsAsFactors = F)
	datareadac = dataread
	
	#check for data between 9am - 4pm
	tmmins = timetomins2(as.character(dataread[,1]))
	dataread2 = dataread[tmmins>540,]
	tmmins = tmmins[tmmins>540]
	dataread = dataread2[tmmins < 960,]

	#if no values are between this time-window return
	if(nrow(dataread)<1)
	{
		print(paste('Not yet time...Last time:',as.character(datareadac[nrow(datareadac),1])))
		return()
	}
	fulldataread = dataread
	
	#extract subdata from index of lastsuccesspacts to the end of the file
	if(lastsuccesspacts != " ")
	{
		print(paste('to match',lastsuccesspacts,'with',as.character(dataread[nrow(dataread),1])))
		idxtsmtch = match(lastsuccesspacts,as.character(dataread[,1]))
		print(paste('match val of idxts is',idxtsmtch))
		print(paste('nrow dataread',nrow(dataread)))
		
		#if the match is the last row, then no new data exists so return the call
		if((idxtsmtch+1) > nrow(dataread))
		{
			print('No new data recorded.... returning twilio call')
			return()
		}
		
		#Extract the sub-data
		dataread = dataread[(idxtsmtch+1):nrow(dataread),]
	}

	#set last successpacts to the last recorded value and write this to a file
	lastsuccesspacts <<- as.character(dataread[nrow(dataread),1])
	print(paste('Last timestamp recordered sucess',lastsuccesspacts))
	write(lastsuccesspacts,file=pathtstwilioread,append = F)

	pac = pacac = as.numeric(dataread[,2])
	pac = pac[complete.cases(pac)]
	datareadtrue = dataread[complete.cases(pacac),]
	
	#pacneg stores values of pac in the data which are not finite like NA on NaN	
	pacneg = pacac[!is.finite(pacac)] 

	#Perform all Pac checking operations only if the doneforthedayfiringpac flag isn't set
	if(!doneforthedayfiringpac)
	{
	print(paste('length pacneg',length(pacneg)))
	{

		#if length of pacneg is > 0 firewarning and set all the corresponding flags
		if(length(pacneg) > 0)
		{
			idx = match(pacneg[1],pacac)
			print(paste(idx,'idx val in pacneg'))

			#body of the text message
			message = paste("Station IN-009T Pac meter error, Pac reading:",as.character(pacneg[1]),"Timestamp:",as.character(dataread[idx,1]))
			command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',message,'"',' "IN-009T"',sep = "")
			
			#System command call to call the python script to fire the text-message
			system(command)

			#send an email too with the message and subject passed as parameters
			sendEmailToo(message,"IN-009T Pac Meter Error");

			#set the flag
			doneforthedayfiringpac <<-1
			print(paste('Pac NA error, message fired, time',as.character(dataread[idx,1])))
			
			#update lastfiredtspac
			lastfiredtspac <<- Sys.time()
		}
		else
		{
			dataread = dataread[complete.cases(pacac),]
			
			#check for data greater than the threshold
			pac2 = pac[pac < 0.5]
			print(paste('pac2 length',length(pac2)))
			if(length(pac2) > 0)
			{
			
			#if there are values less than the threshold then fire-warning mail
				idx = match(pac2[1],pac)
			print(paste(idx,'idx val in pac2'))
			
			#body of the text message
				message = paste("Station IN-009T Pac meter error, Pac reading:",as.character(pac2[1]),"Timestamp:",as.character(dataread[idx,1]))
				command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',message,'"',' "IN-009T"',sep = "")
				
			#System command call to call the python script to fire the text-message
				system(command)
				
			#send an email too with the message and subject passed as parameters
				sendEmailToo(message,"IN-009T Pac Meter error");
			
			#set the flag	
				doneforthedayfiringpac <<-1
				print(paste('Pac less than threshold error, message fired, time',as.character(dataread[idx,1])))
				
			#update lastfiredtspac	
				lastfiredtspac <<- Sys.time()
			}
		}
	}	
	}

	if(!doneforthedayfiringpac)
		print(paste('Twilio passed for pac values',as.character(dataread[nrow(dataread),1])))
	
	if(doneforthedayfiringpr)
	{
		return()
	}
	
	dataread = fulldataread
	datareadtrue = dataread
	
	#calculate Live-PR
	pr = (sum(as.numeric(dataread[complete.cases(dataread[,2]),2]))*100)/((sum(as.numeric(dataread[complete.cases(dataread[,4]),4]))/60000)*500.24)
  if(is.finite(pr))
	{
	#check for PR < 25 and PR > 100 but less than 500
	if(pr < 25 || ((pr > 100) && pr < 500))
	{
				message = paste("Station IN-009T PR err, PR:",as.character(pr),"Last Timestamp:",as.character(datareadtrue[nrow(datareadtrue),1]))
				command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',message,'"',' "IN-009T"',sep = "")
				system(command)
				sendEmailToo(message,"IN-009T PR Error");
				
				#set doneforthedayfiring flag
				doneforthedayfiringpr <<-1
				print(paste('PR less than 25 error, message fired, time',as.character(dataread[nrow(dataread),1])))
				
				#update the lastfiredtspr time to current time
				lastfiredtspr25 <<- Sys.time()
				return()
	}
  
	if(pr < 50)
	{
		print(paste('Pr less than 50, time',as.character(dataread[nrow(dataread),1])))
		
		#update the prless50 counter
		prless50 <<- prless50 + 1
		
		#check if counter is > 2
		if(prless50 > 2)
		{

		#if yes fire warning message
				message = paste("Station IN-009T PR err, PR less than 50 for 3 continuous readings, PR for day so far:",as.character(pr),"Last Timestamp:",as.character(datareadtrue[nrow(datareadtrue),1]))
				command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',message,'"',' "IN-009T"',sep = "")
				system(command)
				sendEmailToo(message,"IN-009T PR Error");
				
				#update flag
				doneforthedayfiringpr <<-1
				print(paste('PR less than 50 error, message fired, time',as.character(dataread[nrow(dataread),1])))
				
				#update last recorded time
				lastfiredtspr50 <<- Sys.time()
				return()
		}
	}

	if(pr < 60)
	{
		print(paste('Pr less than 60, time',as.character(dataread[nrow(dataread),1])))
		if(pr > 50)
		{
			#since pr > 50 reset the prless50 counter
			prless50 <<- 0
			print(paste('Pr greater than 50 so reset pr50 counter, time',as.character(dataread[nrow(dataread),1])))
		}

		#increment the prless60 counter
		prless60 <<- prless60 + 1

		if(prless60 > 4)
		{
		#if yes fire warning message
				message = paste("Station IN-009T PR err, PR less than 60 for 5 continuous readings, PR for day so far:",as.character(pr),"Last Timestamp:",as.character(datareadtrue[nrow(datareadtrue),1]))
				command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',message,'"',' "IN-009T"',sep = "")
				system(command)
				sendEmailToo(message,"IN-009T PR Error");
				
				#update flag
				doneforthedayfiringpr <<-1
				print(paste('Pr less than 60 error, message fired time',as.character(dataread[nrow(dataread),1])))
				
				#update last recorded time-stamp
				lastfiredtspr60 <<- Sys.time()
		}
		return()
	}
  }
	#if none of the condition are met reset the psless50 and prless60 values as the PR > 60
	prless50 <<- 0
	prless60 <<- 0
	if(!doneforthedayfiringpac)
	print(paste('All test passed no problem, time',as.character(dataread[nrow(dataread),1])))
}


#this variable stores the last recorded time and date for which data was successfully
#fetched for TORP. This variable's contents is updated after every successful 
#data fetch. Its contents are also written to the pathlastday file.

datepreviousouter = " "

################################################################################
#Function to manage backlog if any. This function checks for backlog present, i.e
#if the system was down for sometime, then the function fetches data from the
#last recorded data the system was up to the current date. The pathlastday variable
#stores the file-name which contains the last recorded date & timefor which the 
#system was working and successfully recorded the data. This function compares the
#current date and time with that of what's stored in the file, and asesses if 
#there is any backlog dates for which data has to be fetched.
#For example if the current date is 12-12-2016 and the contents of the 
#'pathlastday' file are 29-11-2016, then data from 29-11-2016 to 12-12-2016 is
#fetched using this function call.
################################################################################
managebacklog = function()
{
	today = as.character(Sys.Date())
	nowdate = unlist(strsplit(as.character(today),"-"))

	#Get year month and date for current day
	yrnow = as.numeric(nowdate[1])
	mnnow = as.numeric(nowdate[2])
	dynow = as.numeric(nowdate[3])


	print(paste('Now',yrnow,mnnow,dynow))

	#Read content of the pathlastday file to get last recorded date
	lastdate = readLines(pathlastday)


	newdate = unlist(strsplit(as.character(lastdate),"-"))
	
	#Get year month and date for last-recorded date
	yrlast = as.numeric(newdate[1])
	mnlast = as.numeric(newdate[2])
	dylast = as.numeric(newdate[3])


	print(paste('Last',yrlast,mnlast,dylast))
	
	#Conditions for no backlog
	{
		if((yrnow==yrlast) && (mnnow==mnlast) && ((dynow -dylast) <= 1))
		{
			print('No Backlogs')
			return()
		}
		else if((yrnow==yrlast) && ((mnnow-1)==mnlast) && (dynow==1) && (dylast == daysmonthsno[mnlast]))
		{
				print('No Backlogs')
				return()
		}
	}

	print('backlog exist')
	print(paste('last day:',lastdate,'today:',today))
	
	#Fetch backlog data by traversing through all the days in-between last recorded
	#date and current date

	for(x in yrlast : yrnow)
	{
		pathyrraw = paste(pathraw,x,sep="/")
		if(!file.exists(pathyrraw))
			dir.create(pathyrraw)
		pathyrgen1 = paste(pathgen1,x,sep="/")
		if(!file.exists(pathyrgen1))
			dir.create(pathyrgen1)
		for(y in mnlast : mnnow)
		{
			mnlast2 = paste(x,"-",y,sep="")
			if(y < 10)
			{
				mnlast2 = paste(x,"-0",y,sep="")
			}
			pathmonraw = paste(pathyrraw,mnlast2,sep="/")
			print(pathmonraw)
			if(!file.exists(pathmonraw))
				dir.create(pathmonraw)
			pathmongen1 = paste(pathyrgen1,mnlast2,sep="/")
			if(!file.exists(pathmongen1))
				dir.create(pathmongen1)
			start = end = 1
			if(y == mnlast)
			{
				start = dylast + 1
			}
			{
				if(y == mnnow)
				{
					end = dynow-1
				}
				else
				{
					end = daysmonthsno[y]
				}
			}
			for(z in start : end)
			{
				day = z
				mon = y
				yr = x
				if(day < 10)
				{
					day = paste("0",day,sep="")
				}
				if(mon < 10)
				{
					mon = paste("0",mon,sep="")
				}

				#parameter passed to fetchraw data must be in DD/MM/YYYY format
				day1 = paste(day,mon,yr,sep="/")

				#while naming files the format is YYYY-MM-DD 
				dayac = paste(yr,mon,day,sep="-")
				
				#pathdayraw stores the path to store the raw data from the TORP system
				#this content is fetched from the fetchrawdata function
				pathdayraw = paste(pathmonraw,"/",stationno," ",dayac,".txt",sep="")
				
				#pathdaygen1 stores the path to store the gen-1 data which is obtained 
				#from the cleansedata function
				pathdaygen1 = paste(pathmongen1,"/",stationno," ",dayac,".txt",sep="")
				df = fetchrawdata(day1,day1)
				write.table(df,file = pathdayraw,row.names = F,col.names = T,sep = "\t",append = F)

				df2 = cleansedata(pathdayraw)
				write.table(df2,file = pathdaygen1,row.names = F,col.names = T,sep = "\t",append = F)
				write(dayac,file=pathlastday,append = F)
				print(paste(dayac,"done"))
			}
		}
	}

	#update this variable to reflect last recroded date
	datepreviousouter <<- dayac
}


################################################################################
#Function to update the flags used the firetwilio function
#The flags doneforthedayfiringpac and doneforthedayfiringpr are reset to 0if the
#time since they were last set to 1 is greater than 1 hr. The lastfiredtspac and
#lastfiredtspr stores the time-stamp when the last warning messages were sent and
#thus is used to compare if an hour has elapsed since the flags were set.
#While reseting the doneforthedayfiringpac flag the flags prless50 prless60 are 
#also reset to 0. 
################################################################################
manageflags = function()
{
	tmnow = Sys.time()
	if(doneforthedayfiringpac)
	{
		print('Checking if pac flag has to be reset')
		print(paste('time pac flag set',as.character(lastfiredtspac),'Time now',as.character(tmnow)))
		{
			if(abs(as.numeric(difftime(tmnow,lastfiredtspac,units = "mins")))> 60)
			{
				print('Time is more than 1 hr so resetting flag pac')
				doneforthedayfiringpac <<-0
			}
			else
				print('Time not yet elspased, so not resetting flag')
		}
	}
	if(doneforthedayfiringpr)
	{
		print('Checking if pr flag has to be reset')
		{
			if(!(prless50 > 2 || prless60 > 4))
			{
				print('Flag set due to PR < 25, so checking if it has to be reset')
				print(paste('time pr flag set',as.character(lastfiredtspr25),'Time now',as.character(tmnow)))
				{
					if(abs(as.numeric(difftime(tmnow,lastfiredtspr25,units="mins")))> 60)
					{
						print('Time is more than 1 hr so resetting flag pr')
						doneforthedayfiringpr <<-0
						prless50 <<- 0
						prless60 <<- 0 
					}
					else
						print('Time not yet elapsed, so not resetting flag')
				}
			}
			else if(prless50 > 2)
			{
				print('Flag set due to PR < 50, so checking if it has to be reset')
				print(paste('time pr flag set',as.character(lastfiredtspr50),'Time now',as.character(tmnow)))
				{
					if(abs(as.numeric(difftime(tmnow,lastfiredtspr50,units="mins")))> 60)
					{
						print('Time is more than 1 hr so resetting flag pr')
						doneforthedayfiringpr <<-0
						prless50 <<- 0
						prless60 <<- 0 
					}
					else
						print('Time not yet elapsed, so not resetting flag')
				}
			}
			else if(prless60 > 4)
			{
				print('Flag set due to PR < 60, so checking if it has to be reset')
				print(paste('time pr flag set',as.character(lastfiredtspr60),'Time now',as.character(tmnow)))
				{
					if(abs(as.numeric(difftime(tmnow,lastfiredtspr60,units="mins")))> 60)
					{
						print('Time is more than 1 hr so resetting flag pr')
						doneforthedayfiringpr <<-0
						prless50 <<- 0
						prless60 <<- 0 
					}
					else
						print('Time not yet elapsed, so not resetting flag')
				}
			}
		}
	}
	print(paste('Manage flag operations done, donefiring pac is ',doneforthedayfiringpac,'donefiring pr is',doneforthedayfiringpr))
}


#This is where the code begins to execute all other calls are just functions
dayac = datepreviousouter

#Log-in
#req = httr::POST("http://52.70.243.223/torp/ServiceRouter/login?loginid=operations@cleantechsolar.com&pwd=torp1227")

#Check if there are any backlogs, if yes fetch backlog.
managebacklog()


#variable to check the need for re-login. After every call to fetchrawdata this
#variable is incremented by 1. When the value reaches 12, the log-in function is
#called again, to prevent session expiry to access TORP data. This value is reset
#to 0 every time the login calls happens. 12 chosen as 12*5=60 mins, hence we login
#once every hour

ctrbackoff = 0
dayprevac = dayac

#log-in again, before proceeding to fetching live data
#req = httr::POST("http://52.70.243.223/torp/ServiceRouter/login?loginid=operations@cleantechsolar.com&pwd=torp1227")

#Infinte loop to fetch TORP data every 5 mins
while(1)
{

#Get system time and convert it to India time (System is based in the US, 
#hence convert it to IST). If the time is before 02:00 hrs enter doze mode and 
#sleep for an hour.
	time = Sys.time()
	time = format(time, tz="Asia/Calcutta",usetz=TRUE)
	time = as.character(time)
	time = unlist(strsplit(time,"\\ "))
	date = time[1]
	time = time[2]
	timeac = timetomins(time)
	if(timeac < 120)
	{
		print('In doze mode')
		ctrbackoff = 12
		Sys.sleep(3600)
#log-in after an hours of sleep
#req = httr::POST("http://52.70.243.223/torp/ServiceRouter/login?loginid=operations@cleantechsolar.com&pwd=torp1227")

#go back to the beginning of the loop
		next
	}

	#ctrbackoff reached 12, hence relogin.
	if(ctrbackoff > 11)
	{
		print('Hit backoff making connection request...')
		#req = httr::POST("http://52.70.243.223/torp/ServiceRouter/login?loginid=operations@cleantechsolar.com&pwd=torp1227")
		ctrbackoff = 0
		print('request done')
	}

	rfmt = unlist(strsplit(date,"-"))
	yr = rfmt[1]
	mnth = rfmt[2]
	day = rfmt[3]
	
	#reformating the date to match previously defined convention in managebacklog
	day1 = paste(rfmt[3],rfmt[2],rfmt[1],sep="/")

	pathyrraw = paste(pathraw,yr,sep="/")
	if(!file.exists(pathyrraw))
		dir.create(pathyrraw)
	mnth2 = paste(yr,mnth,sep="-")
	pathmonthraw = paste(pathyrraw,mnth2,sep="/")
	if(!file.exists(pathmonthraw))
		dir.create(pathmonthraw)
	pathwriteraw = paste(pathmonthraw,"/",stationno," ",date,".txt",sep="")
	pathyrgen1 = paste(pathgen1,yr,sep="/")
	if(!file.exists(pathyrgen1))
		dir.create(pathyrgen1)
	pathmonthgen1 = paste(pathyrgen1,mnth2,sep="/")
	if(!file.exists(pathmonthgen1))
		dir.create(pathmonthgen1)
	pathwritegen1 = paste(pathmonthgen1,"/",stationno," ",date,".txt",sep="")
	print('Making fetch call')
	df = fetchrawdata(day1,day1)
	print(paste('Fetched raw data for',day1))
	
	#write raw data fetched from TORP
	write.table(df,file = pathwriteraw,row.names = F,col.names = T,sep = "\t",append = F)
	df2 = cleansedata(pathwriteraw)
	
	#NODATACOUNTER is a variable that gets incremented if the data fetched from TORP
	#has 0 rows. If this value reaches 10, a warning mail is sent by calling the 
	#SENDWARNINGMAIL function
	{
	if(nrow(df2) < 1)
		NODATACOUNTER = NODATACOUNTER + 1
	else{
		NODATACOUNTER = 0
		if(DATASENTFORTHEDAY)
		{
			SENDOKMAIL()
		}
	}
	}

	if(NODATACOUNTER > 10 && DATASENTFORTHEDAY == 0)
	{
		SENDWARNINGMAIL()
		NODATACOUNTER = 0
	}
	if(nrow(df2) < 1 || is.na(df2[nrow(df2),1]))
	{
		print('No new rows... sleeping for an hour')
		Sys.sleep(3600)
		next
	}
	lsttime = as.character(df2[nrow(df2),1])
	
	#Write the gen-1 data fetched from cleansedata function
	write.table(df2,file = pathwritegen1,row.names = F,col.names = T,sep = "\t",append = F)
	write(date,file=pathlastday,append = F)
	print(paste('Iteration done last timestamp',lsttime))

	#call manage flags function
	manageflags()

	#call firetwilio for the last updated gen-1 file
	firetwilio(pathwritegen1)

	#sleep for 5 mins
	Sys.sleep(300)

	#increment this variable to ensure relogin
	ctrbackoff = ctrbackoff + 1

	if(dayprevac == " ")
	{
		print('Yes dateprevac not initialized so doing it')
		dayprevac = date
	}

# if this condition is true, it implies the date is still the same, so go to 
#beginning of the loop
	if(date==dayprevac)
	{
		next
	}

	#new day encoutered so reset and update all the variable
	print('Moving on to the next day....')
	dayprevac = date
	doneforthedayfiringpr <<- 0
	doneforthedayfiringpac <<- 0
	prless50 <<- 0
	prless60 <<- 0
	lastsuccesspacts <<- " "
	NODATACOUNTER = 0
	DATASENTFORTHEDAY=0
	write(lastsuccesspacts,file=pathtstwilioread,append = F)
}
